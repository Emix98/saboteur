/*************
/*************
                Requêtes simple de vérification
                                                **************\
                                                **************\



/* Vérifier si un USER existe en fonction de son ID ou PSEUDO */

SELECT COUNT(*) FROM User U
INNER JOIN Player P
 ON U.id_player=P.id_player
WHERE P.player_name = 'truc';


/* Récupérer son PSEUDO et/ou son MDP en fonction de son ID(ou PSEUDO) */

CREATE VIEW Test AS
SELECT Login from User where id_user = 1;


/* Vérifier si une GAME existe en fonction de son ID */

SELECT COUNT(*) FROM Game
WHERE id_Game = '4';


/*************
/*************
                Requêtes utile pour les stats
                                                **************\
                                                **************\



/* Sortir les stats d'un USER */

select * from Statistic where id_user = 1;

SELECT num_game, num_win, num_gold, num_round_saboteur FROM Statistic S
INNER JOIN User U
 ON S.id_user=U.id_user
WHERE U.login='truc';


/* Classement en terme de victoires */

SELECT login, num_win AS "Nombre de victoires" FROM Statistic S
INNER JOIN User U
 ON S.id_user=U.id_user
ORDER BY num_win DESC;


/* Classement en terme d'or récoltés */

SELECT login, num_gold AS "Nombre d'or récoltés" FROM Statistic S
INNER JOIN User U
 ON S.id_user=U.id_user
ORDER BY num_gold DESC;


/* Classement en terme de ratio  */

SELECT login, num_win/num_game AS "Ratio" FROM Statistic S
INNER JOIN User U
 ON S.id_user=U.id_user
 ORDER BY (num_win/num_game) DESC;


/* Récupérer les informations d'une carte (le type et ensuite pour chaque type les infos)*/

select card_name as nomDeLaCarte,
type_name as typeDeLaCarte
from Card ca, Type t where ca.id_type = t.id_type;


/*************
/*************
                Requêtes pour récupérer les données
                                                **************\
                                                **************\



/* Numéro du round */

SELECT actual_round
FROM Game G
WHERE id_Game = 1;


/* Cartes d'un joueur */

SELECT C.card_name AS "Carte du joueur",
PP.quantite_card_player AS "quantité"
 FROM Player P
INNER JOIN player_card PP
 ON P.id_player=PP.id_player
 INNER JOIN Card C
 ON PP.id_card=C.id_card
 WHERE P.id_player=1;


/* Pioche */

SELECT C.card_name AS "Carte",
CC.quantite_card_game AS "quantité"
 FROM Game G
INNER JOIN card_game CC
 ON G.id_game=CC.id_game
 INNER JOIN Card C
 ON CC.id_card=C.id_card
 WHERE G.id_game=1;


 /* Lister les JOUEURS d'une game */

 SELECT player_name AS "Liste des joueurs" FROM Player P
 INNER JOIN Team T
  ON P.id_team=T.id_team
 WHERE T.id_game=1;


/* Carte sur plateau */

SELECT C.x_position AS "Position x",
C.y_position AS "Position y",
Ca.card_name AS "Nom de la carte"
FROM Game G
INNER JOIN has H
ON G.id_game=H.id_game
INNER JOIN Cell C
ON H.id_cell=C.id_cell
INNER JOIN Path P
ON C.id_path=P.id_path
INNER JOIN Card Ca
ON P.id_path=Ca.id_path
WHERE G.id_game=1;


/* Handicap du joueur */

SELECT P.player_name AS "Nom du joueur",
H.pickaxe AS "H1",
H.mine_cart AS "H2",
H.lantern AS "H3"
FROM Player P
INNER JOIN player_handicap PP
ON P.id_player=PP.id_player
INNER JOIN Handicap H
ON PP.id_handicap=H.id_handicap
WHERE P.id_player=1;


/* Taille de la grille */

SELECT G.grid_width AS "Largeur",
G.grid_height AS "Hauteur"
FROM Game G
WHERE id_Game=2;


/* Requête pour savoir à qui le tour */

SELECT token_position
FROM Game G
WHERE id_Game=1;


/* Numéro d'or d'un joueur */

SELECT num_gold
FROM Player
WHERE id_player=1;


/* Nombre de rounds d'une game */

SELECT max_round
FROM Game
WHERE id_game=1;


/* Requête pour récupérer toutes les données suivante
  - Numéro du Round
  - Grille
  - token_position
  - nombre de round */


  SELECT G.grid_width AS "Largeur",
  G.grid_height AS "Hauteur",
  G.actual_round AS "Numéro du round",
  G.max_round AS "Nombre de rounds"
  FROM Game G
  WHERE id_Game=1;


  /* Requête pour récupérer toutes les données suivante
    - Liste des joueurs
    - Leur nombre d'or
    */

SELECT P.player_name AS "Liste des joueurs",
P.num_gold AS "Nombre d'or"
FROM Player P
INNER JOIN Team T
 ON P.id_team=T.id_team
  WHERE T.id_game=1;


/* Requête pour récupérer toutes les données suivante
  - Liste des joueurs
  - Leur nombre d'or
  - Leur(s) handicap(s)
  - Leurs cartes
 */

 SELECT P.player_name AS "Liste des joueurs",
 P.num_gold AS "Nombre d'or" ,
 C.card_name AS "Carte du joueur",
 PP.quantite_card_player AS "quantité",
 H.pickaxe AS "H1",
 H.mine_cart AS "H2",
 H.lantern AS "H3"
 FROM Player P
 INNER JOIN Team T
  ON P.id_team=T.id_team
  INNER JOIN player_card PP
   ON P.id_player=PP.id_player
   INNER JOIN Card C
   ON PP.id_card=C.id_card
   INNER JOIN player_handicap PH
   ON P.id_player=PP.id_player
   INNER JOIN Handicap H
   ON PH.id_handicap=H.id_handicap
   WHERE T.id_game=1;


/* Requête pour récupérer toutes les données suivante
  - Numéro du round x
  - Grille x
  - Liste des JOUEURS x
  - Cartes par JOUEURS x
  - num_gold x
  - token_position x
  - leurs handicaps
*/


SELECT G.actual_round AS "Numéro du round",
G.grid_width AS "Largeur de la grille",
G.grid_height AS "Hauteur de la grille",
G.token_position AS "Tour",
P.player_name AS "Joueur dans la Game",
P.num_gold AS "Nombre d'or",
C.card_name AS "Carte du joueur",
PP.quantite_card_player AS "Quantité carte du joueur",
H.pickaxe AS "H1",
H.mine_cart AS "H2",
H.lantern AS "H3"
 FROM Game G
 INNER JOIN Team T
 ON G.id_game=T.id_game
 INNER JOIN Player P
 ON T.id_team=P.id_team
 INNER JOIN player_card PP
 ON P.id_player=PP.id_player
 INNER JOIN Card C
 ON PP.id_card=C.id_card
 INNER JOIN player_handicap PH
 ON P.id_player=PP.id_player
 INNER JOIN Handicap H
 ON PH.id_handicap=H.id_handicap
 WHERE T.id_Game=1;
