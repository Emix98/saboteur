#------------------------------------------------------------
#        Script insertion MySQL.
#------------------------------------------------------------


/* TRUE = 1*/

#------------------------------------------------------------
# Table: Type
#------------------------------------------------------------

INSERT INTO Type VALUES (1, 'Path');
INSERT INTO Type VALUES (2, 'Action_On_Player');
INSERT INTO Type VALUES (3, 'Action_On_Grid');



#------------------------------------------------------------
# Table: Action_On_Grid
#------------------------------------------------------------


INSERT INTO Action_On_Grid VALUES (1, 1, 0, 0);
INSERT INTO Action_On_Grid VALUES (2, 0, 1, 0);
INSERT INTO Action_On_Grid VALUES (3, 0, 0, 1);


#------------------------------------------------------------
# Table: Handicap
#------------------------------------------------------------


/* 1 pickaxe 2 mine 3 lantern */

INSERT INTO Handicap VALUES (1, 1, 0, 0);
INSERT INTO Handicap VALUES (2, 0, 1, 0);
INSERT INTO Handicap VALUES (3, 0, 0, 1);
INSERT INTO Handicap VALUES (4, 1, 1, 0);
INSERT INTO Handicap VALUES (5, 0, 1, 1);
INSERT INTO Handicap VALUES (6, 1, 0, 1);


#------------------------------------------------------------
# Table: Action_On_Player
#------------------------------------------------------------


INSERT INTO Action_On_Player VALUES(1, 0, 1);
INSERT INTO Action_On_Player VALUES(2, 0, 2);
INSERT INTO Action_On_Player VALUES(3, 0, 3);
INSERT INTO Action_On_Player VALUES(4, 1, 1);
INSERT INTO Action_On_Player VALUES(5, 1, 2);
INSERT INTO Action_On_Player VALUES(6, 1, 3);
INSERT INTO Action_On_Player VALUES(7, 0, 4);
INSERT INTO Action_On_Player VALUES(8, 0, 5);
INSERT INTO Action_On_Player VALUES(9, 0, 6);


#------------------------------------------------------------
# Table: Path
#------------------------------------------------------------


/*b, nsew*/

INSERT INTO Path VALUES (1, 0, 1, 1, 1, 1);
INSERT INTO Path VALUES (2, 0, 1, 0, 1, 1);
INSERT INTO Path VALUES (3, 0, 1, 1, 1, 0);
INSERT INTO Path VALUES (4, 1, 0, 0, 0, 1);
INSERT INTO Path VALUES (5, 0, 0, 0, 1, 1);
INSERT INTO Path VALUES (6, 0, 0, 1, 1, 0);
INSERT INTO Path VALUES (7, 1, 1, 0, 1, 1);
INSERT INTO Path VALUES (8, 0, 1, 1, 0, 0);

INSERT INTO Path VALUES (9, 1, 1, 0, 1, 0);
INSERT INTO Path VALUES (10, 1, 1, 1, 1, 1);
INSERT INTO Path VALUES (11, 1, 1, 0, 0, 0);
INSERT INTO Path VALUES (12, 1, 0, 1, 1, 0);
INSERT INTO Path VALUES (13, 1, 0, 1, 0, 1);

INSERT INTO Path VALUES (14, 1, 1, 1, 0, 1);
INSERT INTO Path VALUES (15, 1, 1, 0, 1, 1);
INSERT INTO Path VALUES (16, 1, 0, 0, 1, 1);
INSERT INTO Path VALUES (17, 1, 1, 1, 0, 0);




#------------------------------------------------------------
# Table: Card
#------------------------------------------------------------

/* 2 AOP 3 AOG */

INSERT INTO Card VALUES(1, 'PathNSEW', 1, NULL, NULL, 1, 5);
INSERT INTO Card VALUES(2, 'PathNEW', 1, NULL, NULL, 2, 5);
INSERT INTO Card VALUES(3, 'PathNSE', 1, NULL, NULL, 3, 5);
INSERT INTO Card VALUES(4, 'NoPathW', 1, NULL, NULL, 4, 1);
INSERT INTO Card VALUES(5, 'PathEW', 1, NULL, NULL, 5, 3);
INSERT INTO Card VALUES(6, 'PathSE', 1, NULL, NULL, 6, 7);
INSERT INTO Card VALUES(8, 'PathNS', 1, NULL, NULL, 8, 4);
INSERT INTO Card VALUES(9, 'NoHandicap1', 2, NULL, 1, NULL, 2);
INSERT INTO Card VALUES(10, 'NoHandicap2', 2, NULL, 2, NULL, 2);
INSERT INTO Card VALUES(11, 'NoHandicap3', 2, NULL, 3, NULL, 2);
INSERT INTO Card VALUES(12, 'Handicap1', 2, NULL, 4, NULL, 3);
INSERT INTO Card VALUES(13, 'Handicap2', 2, NULL, 5, NULL, 3);
INSERT INTO Card VALUES(14, 'Handicap3', 2, NULL, 6, NULL, 3);
INSERT INTO Card VALUES(15, 'NoHandicap4', 2, NULL, 7, NULL, 2);
INSERT INTO Card VALUES(16, 'NoHandicap5', 2, NULL, 8, NULL, 2);
INSERT INTO Card VALUES(17, 'NoHandicap6', 2, NULL, 9, NULL, 2);
INSERT INTO Card VALUES(18, 'Reveal', 3, 1, NULL, NULL, 5);
INSERT INTO Card VALUES(19, 'Remove', 3, 2, NULL, NULL, 3);
INSERT INTO Card VALUES(20, 'Swap', 3, 3, NULL, NULL, 0);

INSERT INTO Card VALUES(21, 'NoPathNE', 1, NULL, NULL, 9, 1);
INSERT INTO Card VALUES(22, 'NoPathNSEW', 1, NULL, NULL, 10, 1);
INSERT INTO Card VALUES(23, 'NoPathN', 1, NULL, NULL, 11, 1);
INSERT INTO Card VALUES(24, 'NoPathSE', 1, NULL, NULL, 12, 1);
INSERT INTO Card VALUES(25, 'PathWS', 1, NULL, NULL, 13, 2);

INSERT INTO Card VALUES(26, 'NoPathNWS', 1, NULL, NULL, 14, 1);
INSERT INTO Card VALUES(27, 'NoPathNEW', 1, NULL, NULL, 15, 1);
INSERT INTO Card VALUES(28, 'NoPathEW', 1, NULL, NULL, 16, 1);
INSERT INTO Card VALUES(29, 'NoPathNS', 1, NULL, NULL, 17, 1);










#------------------------------------------------------------
# Table: Cell
#------------------------------------------------------------


INSERT INTO Cell VALUES (1, 0, 1, 0, 0, 1, "E", 1 );
INSERT INTO Cell VALUES (2, 1, 1, 0, 0, 1, "E", 1 );
INSERT INTO Cell VALUES (3, 2, 1, 0, 0, 1, "E", 1 );
INSERT INTO Cell VALUES (4, 5, 4, 0, 0, 1, "E", 1 );
INSERT INTO Cell VALUES (5, 6, 6, 0, 0, 1, "E", 1 );
INSERT INTO Cell VALUES (6, 2, 2, 0, 0, 1, "E", 1 );
INSERT INTO Cell VALUES (7, 2, 3, 0, 0, 1, "E", 1 );


#------------------------------------------------------------
# Table: User
#------------------------------------------------------------


INSERT INTO User VALUES (1, 'HamzaKena', 'abcd', 'Hamza', NULL, NULL, NULL);
INSERT INTO User VALUES (2, 'Maxim', 'efgh', 'Maxime', NULL, NULL, NULL);
INSERT INTO User VALUES (3, 'JacquesChiraque', 'jeifz', 'Jacques', 'Chiraque', NULL, NULL);
INSERT INTO User VALUES (4, 'StephaneBorgne', 'fezfefe', 'Stephane', 'Bern', NULL, NULL);


#------------------------------------------------------------
# Table: Statistic
#------------------------------------------------------------


INSERT INTO Statistic VALUES (1, 50, 37, 88, 102, 1);
INSERT INTO Statistic VALUES (2, 12, 5, 23, 15, 2);
INSERT INTO Statistic VALUES (3, 45, 14, 46, 76, 3);
INSERT INTO Statistic VALUES (4, 104, 67, 156, 147, 4);


#------------------------------------------------------------
# Table: Game
#------------------------------------------------------------


INSERT INTO Game VALUES (1, 3, 3, 1, 50, 60);
INSERT INTO Game VALUES (2, 3, 3, 1, 50, 60);


#------------------------------------------------------------
# Table: Team
#------------------------------------------------------------


INSERT INTO Team VALUES (1, 'TeamOne', 1);
INSERT INTO Team VALUES (2, 'Teamzb', 2);


#------------------------------------------------------------
# Table: Player
#------------------------------------------------------------


/* 1= saboteur */

INSERT INTO Player VALUES (1, 'SauceGod', 0, 1, 1, 1, 1);
INSERT INTO Player VALUES (2, 'Max_imum', 0, 2, 0, 2, 1);
INSERT INTO Player VALUES (3, 'Jacky', 0, 3, 0, 3, 2);
INSERT INTO Player VALUES (4, 'Stef', 0, 4, 0, 4, 2);



#------------------------------------------------------------
# Table: card_game
#------------------------------------------------------------

INSERT INTO card_game VALUES (1, 1, 1);
INSERT INTO card_game VALUES (2, 1, 1);
INSERT INTO card_game VALUES (3, 1, 3);
INSERT INTO card_game VALUES (4, 1, 1);
INSERT INTO card_game VALUES (5, 1, 2);
INSERT INTO card_game VALUES (6, 1, 2);
INSERT INTO card_game VALUES (10, 1, 3);
INSERT INTO card_game VALUES (11, 1, 2);
INSERT INTO card_game VALUES (12, 1, 1);
INSERT INTO card_game VALUES (13, 1, 3);
INSERT INTO card_game VALUES (15, 1, 1);
INSERT INTO card_game VALUES (16, 1, 2);
INSERT INTO card_game VALUES (17, 1, 1);
INSERT INTO card_game VALUES (18, 1, 3);
INSERT INTO card_game VALUES (19, 1, 1);

INSERT INTO card_game VALUES (1, 2, 2);
INSERT INTO card_game VALUES (2, 2, 1);
INSERT INTO card_game VALUES (3, 2, 1);
INSERT INTO card_game VALUES (4, 2, 1);
INSERT INTO card_game VALUES (5, 2, 2);
INSERT INTO card_game VALUES (6, 2, 2);
INSERT INTO card_game VALUES (8, 2, 3);
INSERT INTO card_game VALUES (9, 2, 2);
INSERT INTO card_game VALUES (12, 2, 1);
INSERT INTO card_game VALUES (13, 2, 2);
INSERT INTO card_game VALUES (15, 2, 1);
INSERT INTO card_game VALUES (16, 2, 2);
INSERT INTO card_game VALUES (17, 2, 1);
INSERT INTO card_game VALUES (18, 2, 2);
INSERT INTO card_game VALUES (19, 2, 2);


#------------------------------------------------------------
# Table: player_card
#------------------------------------------------------------


INSERT INTO player_card VALUES (1, 1, 1);
INSERT INTO player_card VALUES (3, 1, 1);
INSERT INTO player_card VALUES (12, 1, 2);
INSERT INTO player_card VALUES (15, 1, 1);

INSERT INTO player_card VALUES (2, 2, 1);
INSERT INTO player_card VALUES (4, 2, 1);
INSERT INTO player_card VALUES (5, 2, 1);
INSERT INTO player_card VALUES (14, 2, 1);
INSERT INTO player_card VALUES (16, 2, 1);

INSERT INTO player_card VALUES (6, 3, 1);
INSERT INTO player_card VALUES (1, 3, 1);
INSERT INTO player_card VALUES (8, 3, 1);
INSERT INTO player_card VALUES (11, 3, 1);
INSERT INTO player_card VALUES (17, 3, 1);

INSERT INTO player_card VALUES (2, 3, 1);
INSERT INTO player_card VALUES (4, 3, 1);
INSERT INTO player_card VALUES (19, 3, 1);
INSERT INTO player_card VALUES (20, 3, 1);


#------------------------------------------------------------
# Table: player_handicap
#------------------------------------------------------------


INSERT INTO player_handicap VALUES (1, 1);


#------------------------------------------------------------
# Table: has
#------------------------------------------------------------


INSERT INTO has VALUES (1, 1);
