#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Type
#------------------------------------------------------------

CREATE TABLE Type(
        id_type   Int NOT NULL ,
        type_name Varchar (32) NOT NULL
	,CONSTRAINT Type_PK PRIMARY KEY (id_type)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Action_On_Grid
#------------------------------------------------------------

CREATE TABLE Action_On_Grid(
        id_action_on_grid Int NOT NULL ,
        reveal            TinyINT NOT NULL ,
        remove            TinyINT NOT NULL ,
        swap              TinyINT NOT NULL
	,CONSTRAINT Action_On_Grid_PK PRIMARY KEY (id_action_on_grid)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Handicap
#------------------------------------------------------------

CREATE TABLE Handicap(
        id_handicap Int NOT NULL ,
        pickaxe     TinyINT NOT NULL ,
        mine_cart   TinyINT NOT NULL ,
        lantern     TinyINT NOT NULL
	,CONSTRAINT Handicap_PK PRIMARY KEY (id_handicap)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Action_On_Player
#------------------------------------------------------------

CREATE TABLE Action_On_Player(
        id_action_on_player Int NOT NULL ,
        positive            TinyINT NOT NULL ,
        id_handicap         Int NOT NULL
	,CONSTRAINT Action_On_Player_PK PRIMARY KEY (id_action_on_player)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Path
#------------------------------------------------------------

CREATE TABLE Path(
        id_path Int NOT NULL ,
        blocked TinyINT NOT NULL ,
        north   TinyINT NOT NULL ,
        south   TinyINT NOT NULL ,
        east    TinyINT NOT NULL ,
        west    TinyINT NOT NULL
	,CONSTRAINT Path_PK PRIMARY KEY (id_path)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Card
#------------------------------------------------------------

CREATE TABLE Card(
        id_card             Int NOT NULL ,
        card_name           Varchar (32) NOT NULL ,
        id_type             Int NOT NULL ,
        id_action_on_grid   Int ,
        id_action_on_player Int ,
        id_path             Int ,
        quantity            Int NOT NULL
	,CONSTRAINT Card_PK PRIMARY KEY (id_card)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Cell
#------------------------------------------------------------

CREATE TABLE Cell(
        id_cell     Int NOT NULL ,
        x_position Int NOT NULL ,
        y_position  Int NOT NULL ,
        empty_t       TinyINT NOT NULL ,
        bedrock_t     TinyINT NOT NULL ,
        gold        TinyINT NOT NULL ,
        cell_name   Varchar (1) NOT NULL ,
        id_path     Int NOT NULL
	,CONSTRAINT Cell_PK PRIMARY KEY (id_cell)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: User
#------------------------------------------------------------

CREATE TABLE User(
        id_user    Int NOT NULL ,
        login      Varchar (64) NOT NULL ,
        password   Varchar (64) NOT NULL ,
        name       Varchar (64) ,
        first_name Varchar (64) ,
        id_stats   Int ,
        id_player  Int
	,CONSTRAINT User_PK PRIMARY KEY (id_user)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Statistic
#------------------------------------------------------------

CREATE TABLE Statistic(
        id_stats           Int NOT NULL ,
        num_game           Int ,
        num_win            Int ,
        num_gold           Int ,
        num_round_saboteur Int ,
        id_user            Int NOT NULL
	,CONSTRAINT Statistic_PK PRIMARY KEY (id_stats)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Game
#------------------------------------------------------------

CREATE TABLE Game(
        id_game        Int NOT NULL ,
        token_position Int NOT NULL ,
        max_round      Int NOT NULL ,
        actual_round   Int NOT NULL ,
        grid_width   Int NOT NULL ,
        grid_height  Int NOT NULL
	,CONSTRAINT Game_PK PRIMARY KEY (id_game)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Player
#------------------------------------------------------------

CREATE TABLE Player(
        id_player   Int NOT NULL ,
        player_name Varchar (32) NOT NULL ,
        num_gold    Int ,
        position    Int ,
        saboteur    TinyINT ,
        id_user     Int NOT NULL ,
        id_team     Int
	,CONSTRAINT Player_PK PRIMARY KEY (id_player)
)ENGINE=InnoDB;



#------------------------------------------------------------
# Table: Team
#------------------------------------------------------------

CREATE TABLE Team(
        id_team   Int NOT NULL ,
        team_name Varchar (32) NOT NULL ,
        id_game   Int
	,CONSTRAINT Team_PK PRIMARY KEY (id_team)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: card_game
#------------------------------------------------------------

CREATE TABLE card_game(
        id_card Int NOT NULL ,
        id_game Int NOT NULL ,
        quantite_card_game Int NOT NULL
	,CONSTRAINT card_game_PK PRIMARY KEY (id_card,id_game)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: player_card
#------------------------------------------------------------

CREATE TABLE player_card(
        id_card   Int NOT NULL ,
        id_player Int NOT NULL ,
        quantite_card_player Int NOT NULL
	,CONSTRAINT player_card_PK PRIMARY KEY (id_card,id_player)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: player_handicap
#------------------------------------------------------------

CREATE TABLE player_handicap(
        id_handicap Int NOT NULL ,
        id_player   Int NOT NULL
	,CONSTRAINT player_handicap_PK PRIMARY KEY (id_handicap,id_player)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: has
#------------------------------------------------------------

CREATE TABLE has(
        id_cell Int NOT NULL ,
        id_game Int NOT NULL
	,CONSTRAINT has_PK PRIMARY KEY (id_cell,id_game)
)ENGINE=InnoDB;




ALTER TABLE Action_On_Player
	ADD CONSTRAINT Action_On_Player_Handicap0_FK
	FOREIGN KEY (id_handicap)
	REFERENCES Handicap(id_handicap);

ALTER TABLE Card
	ADD CONSTRAINT Card_Type0_FK
	FOREIGN KEY (id_type)
	REFERENCES Type(id_type);

ALTER TABLE Card
	ADD CONSTRAINT Card_Action_On_Grid1_FK
	FOREIGN KEY (id_action_on_grid)
	REFERENCES Action_On_Grid(id_action_on_grid);

ALTER TABLE Card
	ADD CONSTRAINT Card_Action_On_Player2_FK
	FOREIGN KEY (id_action_on_player)
	REFERENCES Action_On_Player(id_action_on_player);

ALTER TABLE Card
	ADD CONSTRAINT Card_Path3_FK
	FOREIGN KEY (id_path)
	REFERENCES Path(id_path);

ALTER TABLE Cell
	ADD CONSTRAINT Cell_Path0_FK
	FOREIGN KEY (id_path)
	REFERENCES Path(id_path);

ALTER TABLE User
	ADD CONSTRAINT User_Statistic0_FK
	FOREIGN KEY (id_stats)
	REFERENCES Statistic(id_stats);

ALTER TABLE User
	ADD CONSTRAINT User_Player1_FK
	FOREIGN KEY (id_player)
	REFERENCES Player(id_player);

ALTER TABLE User
	ADD CONSTRAINT User_Statistic0_AK
	UNIQUE (id_stats);

ALTER TABLE User
	ADD CONSTRAINT User_Player1_AK
	UNIQUE (id_player);

ALTER TABLE Statistic
	ADD CONSTRAINT Statistic_User0_FK
	FOREIGN KEY (id_user)
	REFERENCES User(id_user);

ALTER TABLE Statistic
	ADD CONSTRAINT Statistic_User0_AK
	UNIQUE (id_user);



ALTER TABLE Player
	ADD CONSTRAINT Player_User0_FK
	FOREIGN KEY (id_user)
	REFERENCES User(id_user);

ALTER TABLE Player
	ADD CONSTRAINT Player_Team1_FK
	FOREIGN KEY (id_team)
	REFERENCES Team(id_team);

-- ALTER TABLE Player
-- 	ADD CONSTRAINT Player_User0_AK
-- 	UNIQUE (id_user);

ALTER TABLE Team
	ADD CONSTRAINT Team_Game0_FK
	FOREIGN KEY (id_game)
	REFERENCES Game(id_game);

ALTER TABLE card_game
	ADD CONSTRAINT card_game_Card0_FK
	FOREIGN KEY (id_card)
	REFERENCES Card(id_card);

ALTER TABLE card_game
	ADD CONSTRAINT card_game_Game1_FK
	FOREIGN KEY (id_game)
	REFERENCES Game(id_game);

ALTER TABLE player_card
	ADD CONSTRAINT player_card_Card0_FK
	FOREIGN KEY (id_card)
	REFERENCES Card(id_card);

ALTER TABLE player_card
	ADD CONSTRAINT player_card_Player1_FK
	FOREIGN KEY (id_player)
	REFERENCES Player(id_player);

ALTER TABLE player_handicap
	ADD CONSTRAINT player_handicap_Handicap0_FK
	FOREIGN KEY (id_handicap)
	REFERENCES Handicap(id_handicap);

ALTER TABLE player_handicap
	ADD CONSTRAINT player_handicap_Player1_FK
	FOREIGN KEY (id_player)
	REFERENCES Player(id_player);

ALTER TABLE has
	ADD CONSTRAINT has_Cell0_FK
	FOREIGN KEY (id_cell)
	REFERENCES Cell(id_cell);

ALTER TABLE has
	ADD CONSTRAINT has_Grid1_FK
	FOREIGN KEY (id_game)
	REFERENCES Game(id_game);
