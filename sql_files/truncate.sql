SET FOREIGN_KEY_CHECKS = 0;
truncate table has;
truncate table player_handicap;
truncate table player_card;
truncate table card_game;
truncate table Player;
truncate table Team;
truncate table Game;
truncate table Statistic;
truncate table User;
truncate table Cell;
truncate table Card;
truncate table Path;
truncate table Action_On_Player;
truncate table Handicap;
truncate table Action_On_Grid;
truncate table Type;

-- DROP TRIGGER user_stat_ins;
-- DROP TRIGGER user_del;
-- DROP TRIGGER player_del;
-- DROP TRIGGER team_del;
-- DROP TRIGGER game_del;

SET FOREIGN_KEY_CHECKS = 1;
