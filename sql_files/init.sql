/* MYSQL initialisation
USE "mysql -u root -p < init.sql" for generate and fill the BDD */

DROP database saboteur;
CREATE database saboteur;
use saboteur;
source createTable.sql;
source triggers.sql;
source insertBiblio.sql;
