/* Trigger pour l'insertion de stats quand on crée un user */

DELIMITER |
/*
CREATE TRIGGER user_stat_ins AFTER INSERT
On User FOR EACH ROW
BEGIN
  INSERT INTO Statistic VALUES (new.id_User, 0, 0, 0, 0, new.id_User);
  END IF;
  END |*/
 
 
 create or replace TRIGGER User_Stat
after insert on User
FOR EACH ROW

begin
    insert Statistic 
        if NEW.num_game != 0 
        and NEW.num_win != 0
        and NEW.num_gold != 0 
        and NEW.num_round_saboteur != 0; 
            then 
            NEW.num_game = 0 
        and NEW.num_win = 0
        and NEW.num_gold = 0 
        and NEW.num_round_saboteur = 0; 
    WHERE id = NEW.id_user;
end; 


 
  

/* Trigger pour la suppression d'un user */

CREATE TRIGGER user_del AFTER DELETE
On User FOR EACH ROW
BEGIN
  DELETE FROM Player WHERE id_user= old.id_user;
  DELETE FROM Player_card WHERE id_player=old.id_user;
  DELETE FROM Statistic WHERE id_user = old.id_user;
  END |


  /* Trigger pour la suppression d'un player */

  CREATE TRIGGER player_del BEFORE DELETE
  On Player FOR EACH ROW
  BEGIN
    DELETE
    FROM player_card
    WHERE id_player=old.id_player;

    DELETE
    FROM player_handicap
    WHERE id_player=old.id_player;
END |


/* Trigger pour supprimer une team */

CREATE TRIGGER team_del BEFORE DELETE
On Team FOR EACH ROW
BEGIN
  UPDATE Player
  SET id_team = NULL
  WHERE id_team = OLD.id_team;
END |


/* Trigger pour supprimer une game */

CREATE TRIGGER game_del BEFORE DELETE
On Game FOR EACH ROW
BEGIN
  UPDATE Team
  SET id_game = NULL
  WHERE id_game = OLD.id_game;

  DELETE FROM card_game
  WHERE id_game=old.id_game;

  DELETE FROM has
  WHERE id_game=old.id_game;

  DELETE PP
  FROM player_card PP
  INNER JOIN Player P
  ON PP.id_player=P.id_player
  INNER JOIN Team T
  ON P.id_team=T.id_team
  INNER JOIN Game G
  ON T.id_game=G.id_Game
  WHERE G.id_game=old.id_game;

END |




/** archivage des données de l'utilisateur */

create or replace TRIGGER archivage_User
after delete on User
FOR EACH ROW

begin

        insert into archive values(id_user, :old.id_user, sysdate);
end;

/** archivage des Statistiques de l'utilisateur */

create or replace TRIGGER archivageStatistic
after delete on Statistic
FOR EACH ROW

begin

        insert into archive values(id_stats, :old.id_stats, sysdate);
end;


DELIMITER ;