#ifndef RSACRYPTAGE_H
#define RSACRYPTAGE_H
#include <math.h>
#include <string>
#include <iostream>
#include <vector>

#include <Tmp.h> 


class RsaCryptage
{
    public:

        //constructeur
        RsaCryptage();
        virtual ~RsaCryptage();


        long long int clePublic[2]; // Clef public [e,n]



        //Accesseurs
        long long int get_ClePrive ();
        long long int get_p ();
        long long int get_q ();
        long long int get_n ();
        long long int get_tn ();
        long long int get_e ();

        //Modificateur
        void set_ClePrive();
        void set_ClePublic();
        void set_p();
        void set_q();
        void set_n();
        void set_tn();
        void set_e();
        void set_d();

        //Methode de la class
        //vector<int> string_to_sacii(std:string mes);
        long long int cryptage(std::string messages, long long int cle); //utilisation de la clé public pour crypter les message
        std::string decryptage (long long int crypt, long long int cle); //utilisation de la cle prive


        //Methode utile la class
        void affichage ();

        
        vector<long long int> string_to_vector_int (string msg); //convertir un string en tableau d'entier
        vector<long long int> u_d_centaine (vector<long long int> t); //conversion en unite, dizaine, centaine


    protected:

    private:
        long long int clePrive[2]; //clef privee[d,n]
        long long int p; //nombre premier
        long long int q; //nombre premier
        long long int n; // n=p*q
        long long int tn; // tn = (p-1)*(q-1)
        long long int e; // p,q < e < tn et pgcd(tn,e) = 1
        long long int d; // e*d mod n = 1 et p,q < e < tn


};

#endif // RSACRYPTAGE_H
