#ifndef TPM_H
#define TPM_H

#include <math.h>
#include <string>
#include <iostream>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>


#include <vector>
#include <sstream>


/**
convertir un string en tableau d'entier
*/
std::vector<long long int> string_to_vector_int (std::string msg);

/**
Convertir un entier en string
*/
std::string int_to_string (int t);

/**
Retourne la taille d'une valeur
*/
int taille_val (int i);

/**
Calcul du modulo
*/
long long int modulo (long long int a , long long int b);

/**
Fonction puissance
*/
long long int puissance (long long int a, long long int b);

/**
Affiche un tableau d'entier
*/
void affiche (std::vector<long long int> t);

/**
    Test si un nombre est premier
*/
int est_premier (long long int a);

/**
    Return un nombre premier alléatoir
*/
long long int aleatoir_NbrPremier ();

/**
    avec a*u + b*v = pgcd(a,b) retourne la valeur de u
*/
long long int euclide(long long int a, long long int b);

/**
    Calcul du pgcd (a,b)
*/
long long int pgcd(long long int a, long long int b);



#endif // TPM_H
