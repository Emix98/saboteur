#include "RsaCryptage.h"

using namespace std;

//Constructeur
RsaCryptage::RsaCryptage()
{
    set_p();
    set_q();
    set_n();
    set_tn();
    set_e();
    set_d();
    set_ClePrive();
    set_ClePublic();
    affichage();
}

//Destructeur
RsaCryptage::~RsaCryptage()
{
    //dtor
}


//Accesseurs
long long int RsaCryptage::get_p ()
{
    return p;
}

long long int RsaCryptage::get_q ()
{
    return q;
}

long long int RsaCryptage::get_n ()
{
    return n;
}

long long int RsaCryptage::get_tn ()
{
    return tn;
}

long long int RsaCryptage::get_e ()
{
    return e;
}

//Modficateur

//Construction de la clef prive
void RsaCryptage::set_ClePrive()
{
    clePrive[0]= d;
    clePrive[1]= n;
}

//Construction de la clef public
void RsaCryptage::set_ClePublic()
{
    clePublic[0] = e;
    clePublic[1] = n;
}

//valeur prmier
void RsaCryptage::set_p()
{
    cout << "Donner un nombre premier " <<endl;
    cin >> p;
}

void RsaCryptage::set_q()
{
    cout << "Donner un nombre premier " <<endl;
    cin >> q;
}

//Calcul de n
void RsaCryptage::set_n()
{
    n = p*q;
}

//Calcul de tn
void RsaCryptage::set_tn()
{
    tn= (p-1)*(q-1);
}


//Calcul de la valeur de e
void RsaCryptage::set_e()
{
    if(p>q)
    {
        e=p+1;
        while (pgcd(tn,e)!= 1)
        {
            e++;
        }
    }
    else
    {
        e=q+1;
       while (pgcd(tn,e)!= 1)
       {
           e++;
       }
    }
}

//Calcul de la valeur de d
void RsaCryptage::set_d()
{
    if (p>q)
    {
        d=p+1;
        while (((e*d)%tn) != 1)
        {
            d++;
        }
    }
    else
    {
        d=q+1;
        while (((e*d)%tn) != 1)
        {
            d++;
        }
    }
    //d = euclide(e,tn);
}

//Calcul utile
long long int RsaCryptage::pgcd(long a, long b)
{
    long long int r;
    while (b!=0)
    {
        r=a%b;
        a=b;
        b=r;
    }
    return a;
}


/**
convertir un string en tableau d'entier
*/
vector<long long int> RsaCryptage::string_to_vector_int (string msg)
{
    vector<long long int> t;
    char a;
    long long int p;
    for(int i =0; i < msg.size(); i++)
    {
        a = msg[i];
        p = a;
        t.push_back(p);
    }
    return t;
}

/**
conversion en unite, dizaine, centaine
*/
vector<long long int> RsaCryptage::u_d_centaine (vector<long long int> t)
{
    vector<long long int> tp;
    for(int i = 0; i<t.size(); i++)
    {
        tp.push_back(t[i]/ 100 % 10);
        tp.push_back(t[i]/ 10 % 10);
        tp.push_back(t[i]% 10);
    }
    return tp;
}



//Methode d'affichage
void RsaCryptage::affichage()
{
    cout << "p " << p << "  q " << q << "  n " << n << "  tn " << tn << "  e " << e << "  d " << d << endl;
    cout << "cleP " << "(" <<clePrive[0] << ", " << clePrive[1] << ")"<< endl;
    cout << "clePu " << "(" <<clePublic[0] << ", " << clePublic[1] << ")"<< endl;
}
void RsaCryptage::affichageValeur( long long int valeur)
{
    cout << "Valeur" << valeur << endl;
}
