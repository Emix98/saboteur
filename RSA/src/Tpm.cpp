#include "Tpm.h"

using namespace std;


/**
convertir un string en tableau d'entier
*/
vector<long long int> string_to_vector_int(string msg)
{
    vector<long long int> t;
    char a;
    int p;
    for(int i =0; i < msg.size(); i++)
    {
        a = msg[i];
        p = a;
        t.push_back(p);
    }
    affiche(t);
    return t;
}


/**
Convertir un entier en string

*/
string int_to_string (int t)
{
    string resul;
    stringstream convert;
    convert << t;
    resul = convert.str();
    return resul;
}

int taille_val(int i)
{
   int c =0;
   do
   {
       i=i/10;
       c++;
   }
   while (i>0);

   return c;
}

long long int puissance (long long int a, long long int b)
{
    long long int c = a;
    long long int d = b;
    while (d>=1)
    {
        if (d!=1)
        {
            c*=a;
        }
        d--;
    }
    return c;
}

long long int modulo (long long int a , long long int b)
{
    long long int r = a - (b * int(a/b));

}


int est_premier (long long int a)
{
    int i;

    for(i=a-1;i!=1;i--)
    {
        if(a%i==0)
        {
            return 0;
        }
    }
    return 1;
}

long long int aleatoir_NbrPremier ()
{
    long long int valeur;
    srand((unsigned int)time(0));
    do
    {
      valeur = (rand()%9+1) + 1;
      //cout << valeur << endl;
    }
    while(est_premier(valeur) != 1);

    return valeur;
}


/**
    avec a*u + b*v = pgcd(a,b) retourne la valeur de u
*/
long long int euclide(long long int a, long long int b)
{
    /** r = PGCD(a, b) et r = au + bv */
    long long int r, u, v;
    r = a; u = 1; v = 0;
    long long int rp = b;
    long long int up = 0;
    long long int vp = 1;
    long long int q, rs, us, vs;
    while (rp != 0)
    {
        q = r / rp;
        rs = r; us = u; vs = v;
        r = rp; u = up; v = vp;
        rp = rs - q * rp; up = us - q * up; vp = vs - q * vp;
    }
    cout << "r = " << r << "; u = " << u << " ;v = " << v << endl;
    return u;
}

/**
    Calcul du pgcd (a,b)
*/

long long int pgcd(long long int a, long long int b)
{
    long long int r;
    while (b!=0)
    {
        r=a%b;
        a=b;
        b=r;
    }
    return a;
}

/**
Affiche un tableau d'entier
*/
void affiche (vector<long long int> t)
{
    for(int i = 0; i<t.size() ; i++)
    {
        cout << t[i] << endl;
    }
}



