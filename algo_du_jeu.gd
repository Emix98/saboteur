##################################
# CREATION DE LA PARTIE

game = Game.new()

# ajout des players dans la partie
for s in room.get_session_list():
    if s.is_signed_up():
        var p = game.add_user_by_id( s.get_database_id() )
        if p != null:
            s.set_player_instance( p )
        else:
            # user considéré non identifié
    else:
        var p = Player.new( s.get_name() )
        game.add_player( p )
        s.set_player_instance( p )

game.init_round() # => round = 0


##################################
# QUAND MESSAGE RECU
game = room.get_game()

var player = session.get_player_instance()

if !game.is_turn( player ):
    return false

# tester le type de la carte
# si action ou pas

# jeu d'un joueur
var valid = game.use_path_card(player_position, hand_position, x, y, rotation)
var valid = game.use_action_on_grid_card(player_position, hand_position, x, y)
var valid = game.use_action_on_player_card(player_position, hand_position, target_position)

# si une carte posée alors plus possible de faire autre chose
# seule option = fin de tour

# ajouter compteur de cartes discarded dans player, compter
# si inférieur à 3
dicard_card() => si discarded ajouter 1 au compteur
# si 3 cartes discarded alors plus possible de faire autre chose
# seule option = fin de tour




##################################
# quand joueur fait end_turn

# check si conditions de fin de jeu validées
if game.is_round_over(): # vérifie si main vide ou si path
    game.get_round_winner( game.get_round() )
    # notification au client de l'équipe qui a gagné le round
    if !game.next_round(): 
        # la partie est finie

# donner autant de cartes qu'il manque au joueur
player.draw_card(draw)

next_player() # => position_token += 1 

