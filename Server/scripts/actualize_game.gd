extends Button

func _on_actualize_game_pressed():
	var idx = 0;
	global.game_list.clear()
	get_node("../../informations/game_info").text = ""
	for game in global.game_rooms.values():
		global.game_list.add_item(str(game.get_id()) + ": " + game.get_name())
		global.game_list.set_item_metadata(idx, game.get_id())
		idx += 1 
