extends Node

# Façade pour le serveur
const MAX_STRING_SIZE = 30

var Room = load("res://scripts/objects/Room.gd")
var Game = load("res://kernel/objects/Game.gd")
var Player = load("res://kernel/objects/Player.gd")

func _init():
	pass
	
func create_room(room_name, max_p, room_password, sender_id):
	if (typeof(max_p) == TYPE_REAL):
		max_p = int(max_p)
	
	if (typeof(max_p) != TYPE_INT  || max_p < 3):
		rpc_node.rpc_id(sender_id, "server_error", "Can't create room, number of players must be an integer > 3")
		return false
	
	if (room_name.length() > MAX_STRING_SIZE || room_password.length() > MAX_STRING_SIZE):
		rpc_node.rpc_id(sender_id, "server_error", "Can't create room, name or password is too long")
		return false
	
	# checking if there is already a room with this name
	for r in global.game_rooms.values():
		if r.get_name() == room_name:
			rpc_node.rpc_id(sender_id, "server_error", "Can't create room, name already in use")
			return false
	
	if global.connected_session.has(sender_id) && global.connected_session[sender_id].get_room_id() != -1:
		rpc_node.rpc_id(sender_id, "server_error", "Can't create room, this player already is in a room")
		return false
		
	# choosing the room id
	var uid = global.global_room_id
	
	var new_room = Room.new(uid, room_name, max_p, room_password, sender_id)
	global.game_rooms[uid] = new_room
	global.connected_session[sender_id].join_room(uid)
	global.global_room_id += 1
	
	global.print_to_debug("Room created with id : " + str(uid) + " by user " + global.connected_session[sender_id].get_name())
	
	
	var game_info = {
		"room_name": room_name,
		"max_p": max_p
	}
	
	var json_encoded_request = JSON.print( game_info, " ", false)
	
	rpc_node.rpc_id(sender_id, "room_created", json_encoded_request)
	
func add_player_to_room(room_name, room_id, room_password, player_id):
	var room_instance = null
	
	# récupération de l'instance de la room
	if room_id == -1:
		for room in global.game_rooms.values():
			if room.get_name() == room_name:
				room_instance = room
	else:
		if global.game_rooms.has(room_id):
			room_instance = global.game_rooms[room_id]
	
	if room_instance == null:
		rpc_node.rpc_id(player_id, "server_error", "This game doesn't exist")
		return false
		
	if room_instance.has_started():
		return false
	
	if !room_instance.compare_password(room_password):
		rpc_node.rpc_id(player_id, "server_error", "Incorrect password")
		return false
		
	if global.connected_session[player_id].get_room_id() != -1:
		rpc_node.rpc_id(player_id, "server_error", "You can't join this room")
		return false
		
	if room_instance.is_full():
		rpc_node.rpc_id(player_id, "server_error", "This game is full")
		return false
		
	var session = global.connected_session[player_id]
	session.join_room(room_instance.get_id())
	
	var players_info = {}
	var player
	
	# we add the player in the scene for each person already in the lobby
	for id in room_instance.get_session_ids():
		rpc_node.rpc_id(id, "player_joined", session.get_name(), session.get_id())
		player = global.connected_session[id]
		# we fill the dictionnary with informations on the players already in the room
		players_info[id] = {}
		players_info[id]["name"] = player.get_name()
		players_info[id]["ready"] = player.is_ready()
	players_info["host"] = room_instance.get_admin_id()
	
	var json_encoded_response = JSON.print( players_info, " ", false)
	
	room_instance.add_player(player_id)
	rpc_node.rpc_id(player_id, "joined_room", json_encoded_response)
	
#	global.game_rooms[room_instance.get_id()].send_to_players("Player " + str (player_id) + " has joined the room")
	
	return true
	
func kick_player_from_room(order_id, id_to_kick):
	if !global.connected_session.has(order_id):
		return false
	
	var ordering_session = global.connected_session[order_id]	
	var room_id = ordering_session.get_room_id()
	# test si le joueur est dans une room
	if  room_id == -1:
		return false
		
	# si le joueur est dans une room qui n'existe pas
	if !global.game_rooms.has(room_id):
		return false
	
	var room = global.game_rooms[room_id]
	
	if room.get_admin_id() != order_id:
		rpc_node.rpc_id(order_id, "server_to_player", "You don't have the power to kick someone")
	
	# si l'id à kick n'est pas connecté
	if !global.connected_session.has(id_to_kick):
		return false
	
	# si l'id n'est pas dans cette room
	if !room.has_player_id(id_to_kick):
		return false
	
	if id_to_kick == room.get_admin_id():
		rpc_node.rpc_id(order_id, "server_to_player", "Player couldn't be kicked")
		return false
		
	# telling every player that id_to_kick left the room
	for id in room.get_session_ids():
		rpc_node.rpc_id(id, "player_kicked", id_to_kick)
	
	# kicking the player from the room
	room.kick_id(id_to_kick)
	global.connected_session[id_to_kick].leave_room()

	return true
	
func player_left(id):
	var room = room_of_player(id)
	if room == null:
		return false
		
	var session = global.connected_session[id]
	
	if room.get_admin_id() == id && room.get_player_number() != 1:
		room.change_admin(room.get_session_ids()[1])
		
	var new_admin = room.get_admin_id()
	
	var room_info = {
		"token": 1,
		"player_left_id": id,
		"new_admin": new_admin
	}
	var json_encoded_response = JSON.print( room_info, " ", false)
	
	for id_in_game in room.get_session_ids():
		rpc_node.rpc_id(id_in_game, "player_left", json_encoded_response)
	
	session.leave_room()

	
func change_ready_state(id, state):
	if !global.connected_session.has(id):
		return false
		
	var session = global.connected_session[id]
	
	if session.get_room_id() == -1 || !global.game_rooms.has(session.get_room_id()):
		return false
	
	session.change_ready_state(state)
	
	for id in global.game_rooms[session.get_room_id()].get_session_ids():
		rpc_node.rpc_id(id, "player_changed_state", session.get_id(), state)
	
	return true
	
func launch_game(id):
	if !global.connected_session.has(id):
		return false
	
	var session = global.connected_session[id]
	
	if session.get_room_id() == -1 || !global.game_rooms.has(session.get_room_id()):
		return false
	
	var room = global.game_rooms[session.get_room_id()]
	
	if room._admin_session_id != id:
		return false
	
	if !room.start_game():
		return false
	
	var players = {}
	
	for session in room.get_session_list():
#		if global.connected_session.has(id):
		players[str(session.get_id())] = room.get_player_json_infos(session.get_id())
			
	# NEW GAME => NEW HAND, NEW DRAW
	var game = Game.new()
	room.set_game(game)
	
	var player_turn_id
	# adding room players in the game
	for s in room.get_session_list():
		if s.is_signed_up():
			var p = game.add_user_by_id(s.get_database_id())
			if p != null:
				s.set_player_instance(p)
			else:
				# user considered non registered
				pass
		else:
			var p = Player.new(s.get_name())
			game.add_player(p)
			s.set_player_instance(p)
		# searching id of next player to play
		if s.get_player_instance().get_position() == 0:
			player_turn_id = s.get_id()
			
	game.init_round(true, 0)
	
	var game_info = {
		"token": 1,
		"players": players,
		"game_round": game.get_round(), # actual round, should be 0
		"player_turn": player_turn_id # player who will play first
	}
	
	var json_encoded_response
	
	for s in room.get_session_list():
		# getting hand for player
		game_info["role"] = s.get_player_instance().get_saboteur()
		game_info["player_hand"] = game.res_get_player_hand(s.get_player_instance().get_position())
#		print(game_info)
		json_encoded_response = JSON.print( game_info, " ", false)
		rpc_node.rpc_id(s.get_id(), "host_started_game", json_encoded_response)
		s.set_in_game(true)
		
	return true

# Player id requested to close his room	
func close_room(id):
	var room = room_of_player(id)
	if room == null:
		return false
		
	if room.get_admin_id() != id:
		# not authorised to close the room
		return false
	
	var response_info = {
		"token": 1
	}
	
	var json_encoded_response = JSON.print( response_info, " ", false)
	
	for s in room.get_session_list():
		rpc_node.rpc_id(s.get_id(), "room_closed", json_encoded_response)
		s.leave_room() 
	# all the players left the room, the room is closed
		
	return true
	
func player_message(sender_id, message):
	var room = room_of_player(sender_id)
	# si le joueur n'est pas dans une room il ne peut pas envoyer de message
	if room == null:
		return false
		
	var session = global.connected_session[sender_id]
	
	var msg = session.get_name() + ": "+ message
	var user_message = {
		"token": 1,
		"message": msg
	}
	
	var json_encoded_response = JSON.print( user_message, " ", false)
	for id in room.get_session_ids():
		rpc_node.rpc_id(id, "user_message", json_encoded_response)



####################### INTERACTION AVEC LE NOYAU ###############

func place_card_on_grid(sender_id, cell, card_position, rotation):
	var room = room_of_player(sender_id)
	if room == null:
		return false
	
	
	var game = room.get_game()
	var session = global.connected_session[sender_id]
	var player_instance = session.get_player_instance()
	var is_valid = true
	
	# CHECK IF IT'S SENDER_ID's TURN 
	if !game.is_turn(player_instance):
		is_valid = false
	
#	print(game.get_position_token())
#	print(player_instance.get_position())
#	print("grid")
#	print(is_valid)
	
	# CHECK IF IT'S POSSIBLE TO PUT THE CARD AT TARGET
	# SET THE CARD
	var card = player_instance.get_hand()[card_position]
	var card_id = card.get_card_id()
	var card_type = player_instance.get_hand()[card_position].get_object_type()
	
	# [id_card, id_type] 
	var card_info = [card_id, card_type]
	if is_valid:
		if card_type == 'P':
			is_valid = game.use_path_card(player_instance.get_position(), card_position, cell.x, cell.y, rotation)
			card_type = 1
		elif card_type == 'G':
			is_valid = game.use_action_on_grid_card(player_instance.get_position(), card_position, cell["x"], cell["y"])
			card_type = 3
	
	# is_valid is true if the kernel managed to do the action else false
	
	var response = {
		"token": 1,
		"player": sender_id,
		"cell": cell,
		"card_position": card_position, # position in hand
		"card_info": card_info,
		"valid": is_valid,
		"rotation": rotation
	}
	
	var json_encoded_response = JSON.print( response, " ", false)
	
	# if action is valid, broadcast
	if is_valid:
		player_instance.get_hand().remove(int(card_position))
		for id in room.get_session_ids():
			rpc_node.rpc_id(id, "ack_card_on_grid", json_encoded_response)
		global.print_to_debug("Card " + str(card_position) + " placed on " + str(cell["x"]) + "," + str(cell["y"])) # x,y
	else: # else it's only sent to the sender
		rpc_node.rpc_id(sender_id, "ack_card_on_grid", json_encoded_response)
		
		
func place_card_on_player(sender_id, player_id, card_position):
	var room = room_of_player(sender_id)
	if room == null:
		return false
	
	var game = room.get_game()
	var session = global.connected_session[sender_id]
	var player_instance = session.get_player_instance()
	var is_valid = true
	
	# CHECK IF IT'S SENDER_ID's TURN 
	if !game.is_turn(player_instance):
		is_valid = false
	
	# CHECK IF IT'S POSSIBLE TO PUT THE CARD AT TARGET
	# SET THE CARD
	var target_player
	var target_position
	if global.connected_session.has(player_id):
		target_player = global.connected_session[player_id].get_player_instance()
		target_position = target_player.get_position()
	else:
		is_valid = false
	
	if is_valid:
		is_valid = game.use_action_on_player_card(player_instance.get_position(), card_position, target_position)
	
	# is_valid is true if the kernel managed to do the action else false
	
	# STATE OF THE TARGET
	var player_state
	if is_valid:
		player_state = target_player.get_handicaps()
#		print(player_state)
	else:
		player_state = null
	
	var response = {
		"token": 1,
		"played_by": sender_id,
		"targeted_player": player_id, # target id
		"player_state": player_state, # target state
		"card_position": card_position,
		"valid": is_valid
	}
	
	var json_encoded_response = JSON.print( response, " ", false)
		
	# if action is valid, broadcast
	if is_valid:
		player_instance.get_hand().remove(int(card_position))
		for id in room.get_session_ids():
			rpc_node.rpc_id(id, "ack_card_on_player", json_encoded_response)
		global.print_to_debug("Card " + str(card_position) + " placed on player "+str(player_id))
	else: # else it's only sent to the sender
		rpc_node.rpc_id(sender_id, "ack_card_on_player", json_encoded_response)

func discard_card(sender_id, card_position):
	var room = room_of_player(sender_id)
	if room == null:
		return
	
	var game = room.get_game()
	var session = global.connected_session[sender_id]
	var player_instance = session.get_player_instance()
	var is_valid = true
	
	# CHECK IF IT'S SENDER_ID's TURN 
	if !game.is_turn(player_instance):
		is_valid = false
	
	# SUPPRESS THE CARD
	if is_valid:
		is_valid = player_instance.discard(card_position)
	
	# is_valid is true if the kernel managed to do the action else false
	
	var response = {
		"token": 1,
		"valid": is_valid,
		"card_position": card_position
	}
	
	var json_encoded_response = JSON.print( response, " ", false)
	
	global.print_to_debug("Card " + str(card_position) + " discarded by " + str(sender_id))
	# message only sent to the sender, no broadcast needed
	rpc_node.rpc_id(sender_id, "ack_discard_card", json_encoded_response)

func player_end_turn(sender_id):
	var room = room_of_player(sender_id)
	if room == null:
		return
	
	var game = room.get_game()
	global.print_to_debug("La pépite est en position " + str(game.get_grid().get_gold_position()) + " pour la game " +str(room.get_id()))
	var session = global.connected_session[sender_id]
	var player_instance = session.get_player_instance()
	var is_valid = true
	
	# CHECK IF IT'S SENDER_ID's TURN 
	if !game.is_turn(player_instance):
		is_valid = false
		
	if is_valid && game.is_round_over():
		# broadcast round ended
		print("end round detected")
		
		if ! game.next_round(): 
			end_game(room, game)
		else:
			end_round(room, game.get_round_winner(game.get_round()-1))
			
		return
	var next_player
	if is_valid:
		# on pioche autant de carte que de cartes on été jouées
		for i in range(player_instance.get_played_cards()):
			player_instance.draw_card(game.get_draw(), true) # true = pioche en tête de pioche, # false = pioche aléatoire
		player_instance.reset_card_counts()
		
		var next_player_pos = game.next_player() # return next player position
		for s in room.get_session_list():
			if s.get_player_instance().get_position() == next_player_pos:
				next_player = s.get_id()
	
	var response = {
		"token": 1,
		"valid": is_valid
	}
	
	var json_encoded_response
	
	# NEW HAND IN THE KERNEL
	
	####################################################### ERREUR NEW HAND TOO BIG
	var hand = game.res_get_player_hand(player_instance.get_position())
	
	# calling end turn on the player who called
	response["new_hand"] = hand
	json_encoded_response = JSON.print( response, " ", false)
	rpc_node.rpc_id(sender_id, "ack_end_turn", json_encoded_response)
	response.erase("new_hand")
	
	response["next_player_id"] = next_player
	response["game_round"] = game.get_round()
	json_encoded_response = JSON.print( response, " ", false)
	
	# if action is valid, broadcast
	if is_valid:
		for id in room.get_session_ids():
			rpc_node.rpc_id(id, "ack_new_turn", json_encoded_response)
		global.print_to_debug("It's now the turn of " + str(next_player))
	else: # else it's only sent to the sender
		rpc_node.rpc_id(sender_id, "ack_card_on_player", json_encoded_response)

func end_round(room, winner):
	print("round ended")
	
	var response = {
		"token": 1,
		"valid": true,
		"winner": winner
	}
	
	var json_encoded_response 
	
	for s in room.get_session_list():
		response["gold"] = s.get_player_instance().get_gold()
		json_encoded_response = JSON.print( response, " ", false)
		rpc_node.rpc_id(s.get_id(), "end_round", json_encoded_response)
	

func continue_game(sender_id):
	print("continue game called")
	var room = room_of_player(sender_id)
	if room == null:
		return
	
	var game = room.get_game()
	
	game.init_round(true,0)
	
	if room.get_admin_id() != sender_id:
		# not authorised to continue the game
		return false
		
	var players = {}
	
	for session in room.get_session_list():
#		if global.connected_session.has(id):
		players[str(session.get_id())] = room.get_player_json_infos(session.get_id())
	
	var player_turn_id
	for s in room.get_session_list():
		if s.get_player_instance().get_position() == 0:
			player_turn_id = s.get_id()
	
	var game_info = {
		"token": 1,
		"players": players,
		"game_round": game.get_round(),
		"player_turn": player_turn_id # player who will play first
	}
	
	var json_encoded_response
	
	for s in room.get_session_list():
		# getting hand for player
		game_info["role"] = s.get_player_instance().get_saboteur()
		game_info["player_hand"] = game.res_get_player_hand(s.get_player_instance().get_position())
#		print(game_info)
		json_encoded_response = JSON.print( game_info, " ", false)
		rpc_node.rpc_id(s.get_id(), "host_continued_game", json_encoded_response)
		s.set_in_game(true)
		
	return true
		
	return true
	
	
func end_game(room, game):
	
	var response = {
		"token": 1
	}
	
	var winner
	var winner_instance
	
	for s in room.get_session_list():
		if s.get_player_instance().get_position() == game.set_winner().get_position():
			winner = s.get_id()
			winner_instance = s.get_player_instance()
	
	response["game_winner_id"] = winner
	response["winner_gold"] = winner_instance.get_gold()
	var json_encoded_response = JSON.print( response, " ", false)
	for id in room.get_session_ids():
		rpc_node.rpc_id(id, "game_ended", json_encoded_response)


########## UTILITY
# returns the room in which the player is, null if he isn't in a room or if the player doens't exists
func room_of_player(id):
	if !global.connected_session.has(id):
		return null
	
	var session = global.connected_session[id]
	
	if session.get_room_id() == -1 || !global.game_rooms.has(session.get_room_id()):
		return null
	
	return global.game_rooms[session.get_room_id()]








