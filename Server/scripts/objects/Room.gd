extends Node

var _room_id
var _name
var _password
var _is_private
var _max_players
var _creation_date
var _creator_id
var _admin_session_id
var _game_sessions_id = []
var _game_has_started = false

# contient l'instance de game
var _game

#warning-ignore:unused_signal
signal player_left_room

func _init(room_id, name, max_player, password, admin_session_id):
	self._room_id = room_id
	self._name = name
	self._admin_session_id = admin_session_id
	self._creator_id = admin_session_id
	self._max_players = max_player
	if password == "":
		self._is_private = false
		self._password = password
	else:
		self._is_private = true
		self._password = password
	self._creation_date = OS.get_unix_time()
	_game_sessions_id.append(admin_session_id)
	if !is_connected("player_left_room", self, "_player_left"):
		connect("player_left_room", self, "_player_left")
	
func _player_left(id):
	if _game_sessions_id.has(id):
		_game_sessions_id.erase(id)
		
	if _game_sessions_id.size() == 0:
		global.game_rooms.erase(_room_id)
		self.queue_free()
		
func get_id():
    return self._room_id
	
func get_name():
    return self._name
	
func get_game():
	return self._game
	
func set_game(game):
	self._game = game

func get_max_players():
	return self._max_players
	
func get_creation_date():
	return self._creation_date
	
func get_admin_id():
	return self._admin_session_id

func is_private():
	return self._is_private
	
func is_full():
	return self.get_player_number() == self._max_players
	
func has_started():
	return self._game_has_started
		
func get_player_number():
	return self._game_sessions_id.size()
	
func get_session_ids():
	return self._game_sessions_id

func has_player_id(id):
	return self._game_sessions_id.has(id)

func set_to_public():
    self._is_private = false
    self._password = ""

func set_to_private(password):
    self._is_private = true
    self._password = password

func get_session_list():
	var sessions = []
	for one_session in _game_sessions_id:
			sessions.append(global.connected_session[one_session])
	return sessions

func add_player(id):
	self._game_sessions_id.append(id)

func how_much_are_ready():
	var ready_count = 0
	for session in get_session_list():
		if !session.is_ready():
			print("" + session.get_name() + " is not ready")
		else:
			ready_count += 1
	return ready_count
	# TODO : vérifier problèmes concurrence

func start_game(): 
	var ready_count = how_much_are_ready()
	
	# CHANGER A >= 3
	if ready_count >= 2 && self._game_sessions_id.size() == ready_count:
		global.print_to_debug("server " + self._name + " is starting a game with " + str(ready_count) + " players")
		_game_has_started = true
		return true
		#TODO
		#game_instance = Game.new(params)
	else :
		global.print_to_debug("server " + self._name + " tried to start a game but there wasn't enough players or they weren't ready")
		return false

func kick_id(id):
	_game_sessions_id.erase(id)

func send_to_players(message):
	for player_id in self._game_sessions_id:
		rpc_node.rpc_id(player_id, "user_message", message)

func close_room():
	print("close room")
	for player_id in _game_sessions_id :
		print("kick " + str(player_id))
		kick_id(player_id)

func change_host(id):
	if self._game_sessions_id.has(id):
		self._admin_session_id = id
		send_to_players("host has been changed to player : " + str(id))
		print("host has been changed to player : " + str(id))
		return true
	else:
		return false
	
func compare_password(password):
	return password == _password

func get_player_json_infos(id):
	var player = {
		"name": global.connected_session[id].get_name(),
		"gold": 0,
		"role": "miner",
		"status": {
			"pickaxe": true,
			"lantern": true,
			"cart": true
		},
	}
	return player

func change_admin(id):
	if self._game_sessions_id.has(id):
		self._admin_session_id = id
