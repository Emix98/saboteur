var _id
var _name
var _room_id
var _is_ready
var _signed_up
var _in_game
var _player_instance

func _init(id):
	self._id = id
	self._name = "guest_" + str(randi()%1001+1)
	self._is_ready = false
	self._room_id = -1
	self._signed_up = false

func get_name():
	return self._name
func get_id():
	return self._id
func get_room_id():
	return self._room_id
func is_signed_up():
	return self._signed_up
func is_ready():
	return _is_ready
func is_ingame():
	return _in_game
func set_name(name):
	self._name = name
func get_player_instance():
	return self._player_instance
func set_player_instance(p):
	self._player_instance =	p

func join_room(id):
	self._room_id = id

func connect_to_account(pseudo):
	print("connected as " + pseudo)
	self._name = pseudo
	self._signed_up = true

func change_ready_state(state):
	self._is_ready = state
	
func leave_room():
	if !global.game_rooms.has(_room_id):
		return false
	
	var room = global.game_rooms[_room_id]
		
	self._room_id = -1
	self._is_ready = false
	room.emit_signal("player_left_room", _id)
	
	return true

func set_in_game(status):
	self._in_game = status
	







