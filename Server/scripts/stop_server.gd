extends Button

func _on_stop_server_pressed():
	get_tree().set_network_peer(null)
	global.debug.text += "Server stopped\n"
