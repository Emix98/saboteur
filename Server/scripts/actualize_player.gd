extends Button

func _on_actualize_player_pressed():
	var idx = 0;
	global.player_list.clear()
	get_node("../../informations/player_info").text = ""
	for player in global.connected_session.values():
		global.player_list.add_item(str(player.get_id()) + ": " + player.get_name())
		global.player_list.set_item_metadata(idx, player.get_id())
		idx += 1 
