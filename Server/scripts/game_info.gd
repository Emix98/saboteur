extends TextEdit

func _ready():
	pass

func _on_game_list_item_selected(index):
	var game_id = get_node("../../list/game_list").get_item_metadata(index)
	if global.game_rooms.has(game_id):
		var game = global.game_rooms[game_id]
		self.text = "Game id: " + str(game.get_id())
		self.text += "\nName: " + game.get_name()
		self.text += "\nMax_players: " + str(game.get_max_players())
		self.text += "\nCreation date: " + str(game.get_creation_date())
		self.text += "\nAdmin of the game: " + str(game.get_admin_id()) + " = " + global.connected_session[game.get_admin_id()].get_name()
		self.text += "\nIs private: " + str(game.is_private())
		var session_list = game.get_session_list()
		self.text += "\nPlayers in this game:"
		for session in session_list:
			self.text += "\n\t" + session.get_name() + ": " + str(session.get_id())
	else:
		self.text = "This game doens't exist anymore"
