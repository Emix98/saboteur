extends Button

const MAX_PLAYERS = 10

#func _on_listen_pressed():
func _ready():
	var network = NetworkedMultiplayerENet.new()
	var port = get_node("../port").text
	network.create_server( int(port), MAX_PLAYERS)
	get_tree().set_network_peer(network)
	
	if (network.get_connection_status() == 2 ):	
		global.print_to_debug("Listening on port " + port)
	else:
		global.print_to_debug("Can't listen on port "+ port + ", status = " + str(network.get_connection_status()))
	
