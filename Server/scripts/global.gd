extends Node

var connected_session = {}
var game_rooms = {}
var global_room_id = 1

const DEBUG_SCENE_NAME = "Window"

#onready var debug = get_node("/root/" + DEBUG_SCENE_NAME + "/tabs_container/debug_tab/debug/debug_window")
#onready var player_list = get_node("/root/" + DEBUG_SCENE_NAME + "/tabs_container/players_tab/list/player_list")
#onready var game_list = get_node("/root/" + DEBUG_SCENE_NAME + "/tabs_container/games_tab/list/game_list")
var player_list
var game_list

func _ready():
	if (get_tree().get_current_scene().get_name() == DEBUG_SCENE_NAME):
		player_list = get_node("/root/" + DEBUG_SCENE_NAME + "/tabs_container/players_tab/list/player_list")
		game_list = get_node("/root/" + DEBUG_SCENE_NAME + "/tabs_container/games_tab/list/game_list")

func print_to_debug(message):
	if (get_tree().get_current_scene().get_name() == DEBUG_SCENE_NAME):
		var debug = get_node("/root/" + DEBUG_SCENE_NAME + "/tabs_container/debug_tab/debug/debug_window")
		debug.text += message + "\n"
	print(message)
	
func clear_text():
	if (get_tree().get_current_scene().get_name() == DEBUG_SCENE_NAME):
		var debug = get_node("/root/" + DEBUG_SCENE_NAME + "/tabs_container/debug_tab/debug/debug_window")
		debug.text = ""