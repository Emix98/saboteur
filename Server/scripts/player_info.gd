extends TextEdit

func _ready():
	pass


func _on_player_list_item_selected(index):
	var player_id = get_node("../../list/player_list").get_item_metadata(index)
	if global.connected_session.has(player_id):
		var player = global.connected_session[player_id]
		self.text = "Name: " + player.get_name()
		self.text += "\nId: " + str(player.get_id())
		self.text += "\nRoom id: " + ("Not in a room" if player.get_room_id() == -1 else str(player.get_room_id()))
		self.text += "\nSigned up: " + str(player.is_signed_up())
		self.text += "\nIs ready: " + str(player.is_ready())
		self.text += "\nIs in game: " + str(player.is_ingame())
		if player.is_ingame:
			var pi = player.get_game() #player_instance
			var handi = pi.get_handicaps()
			self.text += "\n\tHandicaps = P: " + handi[0] + " L: " + handi[1] + "C: " + handi[2] 
			self.text += "\n\tCards :"
			var i = 0
			for c in pi.get_hand():
				self.text += "\n\t\t"+str(i)+": " + c.get_card_id()
				i += 1
	else:
		self.text = "This user is disconnected"