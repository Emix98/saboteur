extends Button

func _ready():
	pass

func _on_send_to_all_pressed():
	var message = get_node("../message").text
	rpc_node.rpc("server_message", message)
	get_node("../message").text = ""
