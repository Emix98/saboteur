extends Node2D

var Session = load("res://scripts/objects/Session.gd")


func _ready():
	get_tree().connect("network_peer_connected", self,"_peer_connected")
	get_tree().connect("network_peer_disconnected", self,"_peer_disconnected")
	
# 	QUICK MYSQL TEST
	mysql_test()
	
func mysql_test():
	var mysql = MySQL.new()
	# si mysql.credentials renvoie true tout s'est bien passé
	if mysql.credentials("localhost", "root", "saboteur", true):
		print("Connection réussie")
	else:
		print("La connection a échouée")
		
	mysql.select_database("saboteur")
	mysql.execute("INSERT INTO User VALUES (34,'testing_user','admin','first_name','name',NULL,NULL)")
#	var name = mysql.query("SELECT * FROM `pw2tp2_users`", 3)
	var name = mysql.new_query("SELECT * FROM User")
#	global.print_to_debug(name)
	print(name)
	
func _peer_connected(id):
	var user = Session.new(id)
	
	if global.connected_session.has(id):
		global.print_to_debug("This user " + str(id) + " is already connected")
	else:
		global.connected_session[id] = user
		rpc_node.rpc_id(user.get_id(), "player_info", user.get_name(), id)
		global.print_to_debug("Connected " + str(id))

func _peer_disconnected(id):
	if !global.connected_session.has(id):
		global.print_to_debug("Trying to disconnect id : " + str(id) + " but he isn't connected")
	else:
		var room_id = global.connected_session[id].get_room_id()
		if  room_id != -1 && global.game_rooms.has(room_id):
			global.game_rooms[room_id].emit_signal("player_left_room", id)
			ServerFacade.player_left(id)
			
		global.connected_session.erase(id)
		global.print_to_debug("Disonnected " + str(id))
