extends 'res://addons/gut/test.gd'

var Card = load("res://kernel/objects/Card.gd")
var Path = load("res://kernel/objects/Path.gd")
var Action = load("res://kernel/objects/Action.gd")
var Cell = load("res://kernel/objects/Cell.gd")
var Grid = load("res://kernel/objects/Grid.gd")
var Action_on_grid = load("res://kernel/objects/Action_on_grid.gd")
var Player = load("res://kernel/objects/Player.gd")
var Team = load("res://kernel/objects/Team.gd")
var Action_on_player = load("res://kernel/objects/Action_on_player.gd")
var Game = load("res://kernel/objects/Game.gd")

func print_check(string, bl):
	assert_true(bl, string)
	if(bl):
		print(string + " : ok")
	else:
		print(string + " : no")

func print_game(g):
	print("game_id : " + str(g.get_game_id()))
	print("round : " + str(g.get_round()))
	print("position_token : " + str(g.get_position_token()))
	print("hand_length : " + str(g.get_hand_length()))
	print("play_time : " + str(g.get_play_time()))
	print("round_started : " + str(g.get_round_started()))
	print("gould_found : " + str(g.get_gold_found()))
	print("draw site : " + str(g.get_draw().size()))

func print_grid(game):
	var g = game.get_grid()
	var value = 0
	for i in range(g.get_height()):
		var string = " "
		for j in range(g.get_width()):
			value = 0;
			if (g.get_cell(i,j).get_self_path(0)):
				value += 1;
			if (g.get_cell(i,j).get_self_path(1)):
				value += 3;
			if (g.get_cell(i,j).get_self_path(2)):
				value += 5;
			if (g.get_cell(i,j).get_self_path(3)):
				value += 7;
			string = string + " " + str(g.get_cell(i,j).get_type() + str(value))
		print(string)
	print("starting_point : (" + str(g.get_starting_point()[0]) + "," + str(g.get_starting_point()[1]) + ")")
	print("ending_point : (" + str(g.get_ending_points()[0][0]) + "," + str(g.get_ending_points()[0][1]) + ") ; (" \
	+ str(g.get_ending_points()[1][0]) + "," + str(g.get_ending_points()[1][1]) + ") ; (" \
	+ str(g.get_ending_points()[2][0]) + "," + str(g.get_ending_points()[2][1]) + ")")

func print_teams(game):
	var teams = game.get_teams()
	for i in range(teams.size()):
		print("team : " + teams[i].get_name())
		for j in range (teams[i].get_players().size()):
			print("  -  player : " + teams[i].get_player(j).get_name() + " ; " + \
			"role : " + str(teams[i].get_player(j).get_saboteur()))

func print_players(game):
	var players = game.get_players()
	for i in range(players.size()):
		var p = players[i]
		print("player" + str(i) + " name : " + p.get_name() + " ; position : " + str(p.get_position()) + " ; is_saboteur : " + str(p.get_saboteur()) + " ; total_gold : " + str(p.get_gold()))

func print_draw(game):
	var draw = game.get_draw()
	for i in range(draw.size()):
		if draw[i].get_object_type() == 'P':
			print("Path : ["+str(draw[i].get_self_path()[0])+","+str(draw[i].get_self_path()[1])+","+str(draw[i].get_self_path()[2])+","+str(draw[i].get_self_path()[3])+"]")
		elif draw[i].get_object_type() == 'J':
			print("Action_on_player : [" + str(draw[i].get_handicap_item(0))+ "," + str(draw[i].get_handicap_item(1)) + "," + str(draw[i].get_handicap_item(2)) + "] ; bonus : " + str(draw[i].is_positive()))
		elif draw[i].get_object_type() == 'G':
			print("Action_on_grid : [" + str(draw[i].is_reveal()) +  "," + str(draw[i].is_remove()) + "," + str(draw[i].is_swap()) + "]")

func print_hands(game):
	for n in range(game.get_nb_players()):
		############## DOUBLE DECLARATION DE DRAW REVOIR
		var hand = game.get_player(n).get_hand()
		print("Player : " + game.get_player(n).get_name())
		for i in range(hand.size()):
			if hand[i].get_object_type() == 'P':
				print("Path : ["+str(hand[i].get_self_path()[0])+","+str(hand[i].get_self_path()[1])+","+str(hand[i].get_self_path()[2])+","+str(hand[i].get_self_path()[3])+"]")
			elif hand[i].get_object_type() == 'J':
				print("Action_on_player : [" + str(hand[i].get_handicap_item(0))+ "," + str(hand[i].get_handicap_item(1)) + "," + str(hand[i].get_handicap_item(2)) + "] ; bonus : " + str(hand[i].is_positive()))
			elif hand[i].get_object_type() == 'G':
				print("Action_on_grid : [" + str(hand[i].is_reveal()) +  "," + str(hand[i].is_remove()) + "," + str(hand[i].is_swap()) + "]")

func separator():
	print("-------------------------------------------------------")

func title(s):
	print("-----------" + s + "-----------")

# test if global.connected_session is correctly filled and cleaned
func test_game():
	#Initialisation du jeu
	var game = Game.new()
	#Création des joueurs
	var player1 = Player.new("Joueur 1")
	var player2 = Player.new("Joueur 2")
	var player3 = Player.new("Joueur 3")
	if game.get_database_enabled():
		game.add_user_by_id(1)
		game.add_user_by_id(2)
		game.add_user_by_id(3)
	else:
		#Affichage du nom des joueurs
		print("Joueur1 : " + player1.get_name())
		print("Joueur2 : " + player2.get_name())
		print("Joueur3 : " + player3.get_name())
		#Accés à la base de données
		#
		#
		#Ajout des joueurs au jeu
		title("Ajout des joueurs au jeu")
		game.add_player(player1)
		game.add_player(player2)
		game.add_player(player3)
	title("Ajout des mains")
	var hand1 = []
	hand1.push_back(Path.new(-1,true,true,true,true,false))
	hand1.push_back(Path.new(-1,true,true,true,true,false))
	hand1.push_back(Path.new(-1,true,false,true,false,false))
	hand1.push_back(Path.new(-1,true,false,true,false,false))
	hand1.push_back(Action_on_grid.new(-1,true,false,false))
	hand1.push_back(Action_on_grid.new(-1,false,true,false))
	hand1.push_back(Action_on_player.new(-1,true,false,false,false))
	hand1.push_back(Action_on_player.new(-1,false,false,true,true))
	var hand2 = []
	hand2.push_back(Path.new(-1,true,true,true,true,false))
	hand2.push_back(Path.new(-1,true,true,true,true,false))
	hand2.push_back(Path.new(-1,true,false,true,false,false))
	hand2.push_back(Path.new(-1,true,false,true,false,false))
	hand2.push_back(Action_on_grid.new(-1,true,false,false))
	hand2.push_back(Action_on_grid.new(-1,false,true,false))
	hand2.push_back(Action_on_player.new(-1,false,true,false,false))
	hand2.push_back(Action_on_player.new(-1,true,false,false,true))
	var hand3 = []
	hand3.push_back(Path.new(-1,true,true,true,true,false))
	hand3.push_back(Path.new(-1,true,true,true,true,false))
	hand3.push_back(Path.new(-1,true,false,true,false,false))
	hand3.push_back(Path.new(-1,true,false,true,false,false))
	hand3.push_back(Action_on_grid.new(-1,true,false,false))
	hand3.push_back(Action_on_grid.new(-1,false,true,false))
	hand3.push_back(Action_on_player.new(-1,false,false,true,false))
	hand3.push_back(Action_on_player.new(-1,false,true,false,true))
	
	game.get_player(0).set_hand(hand1)
	game.get_player(1).set_hand(hand2)
	game.get_player(2).set_hand(hand3)
	
	title("Mains des joueurs")
	print_hands(game)
	separator()
	
	#Affichage avant initialisation de la partie
	title("Avant initialisation de la manche")
	title("GAME")
	print_game(game)
	separator()
	
	title("PLAYERS")
	print_players(game)
	separator()
	
	title("GRID")
	print_grid(game)
	separator()
	
	title("Initialisation de la manche")
	game.init_draw(true)
	### REVOIR HAND LENGTH
	game.set_hand_length()
#	give_hands(true)
	game.give_roles()
	game.init_2_teams()
#	game.set_position_token(1)
	game.set_gold_found(false)
	game.set_starting_point_auto()
	game.set_ending_points(1,3,1,2)
	game.set_round_started(true)
#	game.next_round()
	#Initialistaion de la partie avec création des équipes
	title("Après initialisation de la manche")
	
	title("GAME")
	print_game(game)
	separator()
	
	title("GRID")
	print_grid(game)
	separator()
	
	title("PLAYERS")
	print_players(game)
	separator()
	
	title("TEAMS")
	print_teams(game)
	separator()
	
	title("Mains")
	print_hands(game)
	separator()
	
	title("Dépot des cartes chemins")
	print_check("insertion chemin",game.use_path_card(game.get_position_token(), 0, 8, 3, 0))
	game.get_player(game.get_position_token()).get_hand().remove(0)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("insertion chemin",game.use_path_card(game.get_position_token(), 2, 8, 4, -1))
	game.get_player(game.get_position_token()).get_hand().remove(2)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("insertion chemin",game.use_path_card(game.get_position_token(), 2, 7, 3, 4))
	game.get_player(game.get_position_token()).get_hand().remove(2)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("insertion chemin",game.use_path_card(game.get_position_token(), 2, 6, 3, 0))
	game.get_player(game.get_position_token()).get_hand().remove(2)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("insertion chemin",game.use_path_card(game.get_position_token(), 2, 5, 3, 2))
	game.get_player(game.get_position_token()).get_hand().remove(2)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("insertion chemin",game.use_path_card(game.get_position_token(), 2, 4, 3, -2))
	game.get_player(game.get_position_token()).get_hand().remove(2)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("insertion chemin",game.use_path_card(game.get_position_token(), 0, 3, 3, 0))
	game.get_player(game.get_position_token()).get_hand().remove(0)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("insertion chemin",game.use_path_card(game.get_position_token(), 0, 3, 4, 2))
	game.get_player(game.get_position_token()).get_hand().remove(0)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("insertion chemin",game.use_path_card(game.get_position_token(), 0, 2, 3, -2))
	game.get_player(game.get_position_token()).get_hand().remove(0)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	
	title("GAME")
	print_game(game)
	separator()
	
	title("Mains")
	print_hands(game)
	separator()
	
	title("Utilisation cartes action sur joueur")
	print_check("action sur joueur",game.use_action_on_player_card(game.get_position_token(), 3, 2))
	game.get_player(game.get_position_token()).get_hand().remove(3)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("joueur 3 pénalité donnée",game.get_player(2).get_handicap(0))
	print_check("action sur joueur",game.use_action_on_player_card(game.get_position_token(), 4, 2))
	game.get_player(game.get_position_token()).get_hand().remove(4)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("joueur 3 pénalité retirée",!game.get_player(2).get_handicap(0))

	print_check("action sur joueur",game.use_action_on_player_card(game.get_position_token(), 3, 1))
	game.get_player(game.get_position_token()).get_hand().remove(3)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("joueur 2 pénalité donnée",game.get_player(1).get_handicap(2))
	print_check("action sur joueur",game.use_action_on_player_card(game.get_position_token(), 3, 1))
	game.get_player(game.get_position_token()).get_hand().remove(3)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("joueur 2 pénalité retirée",!game.get_player(1).get_handicap(2))
	
	print_check("action sur joueur",game.use_action_on_player_card(game.get_position_token(), 3, 0))
	game.get_player(game.get_position_token()).get_hand().remove(3)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("joueur 1 pénalité donnée",game.get_player(0).get_handicap(1))
	print_check("action sur joueur",game.use_action_on_player_card(game.get_position_token(), 3, 0))
	game.get_player(game.get_position_token()).get_hand().remove(3)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("joueur 1 pénalité retirée",!game.get_player(0).get_handicap(1))
	
	title("Utilisation cartes action sur grille : révélations")
	print_check("revelation ending_points[0] rocher",!game.use_action_on_grid_card(game.get_position_token(), 1, 1, 1))
	game.get_player(game.get_position_token()).get_hand().remove(1)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("revelation ending_points[1] pépite",game.use_action_on_grid_card(game.get_position_token(), 1, 1, 3))
	game.get_player(game.get_position_token()).get_hand().remove(1)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("revelation ending_points[2] rocher",!game.use_action_on_grid_card(game.get_position_token(), 1, 1, 5))
	game.get_player(game.get_position_token()).get_hand().remove(1)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	
	title("Utilisation cartes action sur grille suppression")
	print_check("suppression d'une case de fin doit être impossible",!game.use_action_on_grid_card(game.get_position_token(), 1, 1, 1))
	game.get_player(game.get_position_token()).get_hand().remove(1)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("suppression d'une case chemin",game.use_action_on_grid_card(game.get_position_token(), 1, 3, 4))
	game.get_player(game.get_position_token()).get_hand().remove(1)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	print_check("suppression d'une case vide doit être impossible",!game.use_action_on_grid_card(game.get_position_token(), 1, 2, 5))
	game.get_player(game.get_position_token()).get_hand().remove(1)
	game.get_player(game.get_position_token()).draw_card(game.get_draw(),true)
	game.get_player(game.get_position_token()).reset_card_counts()
	game.next_player()
	
	title("Mains")
	print_hands(game)
	separator()
	#Insertion d'un chemin allant jusqu'à une pépite d'or
#	assert_true(game.get_grid().get_cell(8,3).set_path(true,true,true,true,0),"Position de la carte sur la grille")
#	assert_true(game.get_grid().get_cell(7,3).set_path(true,true,true,true,0),"Position de la carte sur la grille")
#	assert_true(game.get_grid().get_cell(6,3).set_path(true,true,true,true,0),"Position de la carte sur la grille")
#	assert_true(game.get_grid().get_cell(5,3).set_path(true,true,true,true,0),"Position de la carte sur la grille")
#	assert_true(game.get_grid().get_cell(4,3).set_path(1,1,1,1,0),"Position de la carte sur la grille")
#	assert_true(game.get_grid().get_cell(3,3).set_path(1,1,1,1,0),"Position de la carte sur la grille")
#	assert_true(game.get_grid().get_cell(2,3).set_path(1,1,1,1,0),"Position de la carte sur la grille")
#	assert_true(game.get_grid().get_cell(8,4).set_path(1,1,1,1,0),"Position de la carte sur la grille")
#	assert_true(game.get_grid().get_cell(8,5).set_path(1,1,1,1,0),"Position de la carte sur la grille")
#	assert_true(game.get_grid().get_cell(7,5).set_path(1,1,1,1,0),"Position de la carte sur la grille")
#
	title("Recherche d'un chemin")
	print_check("path_finding",game.get_grid().path_finding())
	print_grid(game)
	separator()
	
#	title("Action sur un joueur")
#	print("mauls sur un joueur")
#	assert_true(game.get_player(2).use_action_on_player_card(5,game.get_player(0),game.get_draw()), "bonus")
#	print("bonus sur un joueur")
#	assert_true(game.get_player(1).use_action_on_player_card(5,game.get_player(0),game.get_draw()), "bonus")
#	separator()
	
	print_check("round is over",game.is_round_over())
	
	#Tests d'insertion dans la BDD
	game.end_round()
	game.end_game()
	
	title("Score des joueurs")
	print_players(game)
	
	
	separator()
	separator()
	title("Game 2 : Initialisation aléatoire")
	separator()
	separator()
	var game2 = Game.new()
	game2.add_user_by_id(1)
	game2.add_user_by_id(2)
	game2.add_user_by_id(3)
	
	title("Players")
	print_players(game2)
	
	title("Pioche")
	print_draw(game2)	
	
	title("Initialisation du jeu")
	game2.next_round()
	
	title("GAME")
	print_game(game2)
	
	title("Teams")
	print_teams(game2)
	
	title("Mains")
	print_hands(game2)
	
	title("Pioche")
	print_draw(game2)	