extends "res://kernel/objects/Action.gd"

var _action = [] #pénalités/bonus : pioche, lanterne, chariot
var _positive #si pénalité ou bonus

#contructeur
func _init(id,pickaxe,lantern,cart,pos): #int, bool, bool, bool
	self._action = [pickaxe,lantern,cart]
	self._positive = pos
	self._object_type = 'J' #type carte Action_on_player
	self._card_id = id

#retourne si élément pioche/lanterne/chariot est définit
func get_handicap_item(accessory): #int
	if (accessory >= 0) && (accessory < 3):
		return self._action[accessory]
	return false

#retourne si la carte est un bonus
func is_positive():
	return self._positive

#modifie la carte
func assign_card(pickaxe,lantern,cart, heal): #bool, bool, bool, bool
	self._action = [pickaxe,lantern,cart]
	self._positive = heal

#applique la carte à un joueur
func apply(player): # Player
	print("apply action on player")
	for i in range(3):
		if (self._action[i]):
			player.set_handicap(i,!self._positive)