extends Node

var Team = load("res://kernel/objects/Team.gd")
var Path = load("res://kernel/objects/Path.gd")
var Action_on_grid = load("res://kernel/objects/Action_on_grid.gd")
var Action_on_player = load("res://kernel/objects/Action_on_player.gd")
var Grid = load("res://kernel/objects/Grid.gd")
var Player = load("res://kernel/objects/Player.gd")

var _game_id #Identifiant du jeu
var _round #Manche
var _position_token #Position du token
var _hand_length #Taille d'une main
var _play_time #Boolean de temps de jeu
var _round_started #Boolean de manche commencée
var _draw = [] #Pioche
var _players = [] #Joueurs
var _teams = [] #Equipes
var _grid #Grille
var _gold_found #Boolean si or trouvé
var _mysql #Object MYSQL
var _database_enabled #Boolean si base de données connectée
var _round_winners = [] #Gagnants de la dernière manche

#Constructeur
func _init():
	self._game_id = -1
	self._round = 0
	self._position_token = 0
	self._hand_length = 0
	self._play_time = false
	self._round_started = false
	self._gold_found = false
	self._database_enabled = false
	self._grid = Grid.new()
	#Connection à la BDD
	self._mysql = MySQL.new()
	if self._mysql.credentials("localhost", "root", "saboteur", true):
		self._database_enabled = true
	self._mysql.select_database("saboteur")
	create_game()

#Passe à la prochaine manche
func next_round():
	end_round() #Finalise la dernière manche
	if (self._round >= 0) && (self._round < 2):
		self._round += 1
		save_round()
#		init_round(true,0) #Initialise une nouvelle manche
		return true
	return false

#Initialise une nouvelle manche
#param mode : true si pioche des joueurs aléatoire
#param pepite_position : position de la pépite (entre 1 et 3), 0 si aléatoire
func init_round(mode,pepite_position): #bool, int
	init_draw(true)
	### REVOIR HAND LENGTH
	set_hand_length()
	give_hands(mode)
	give_roles()
	init_2_teams()
	set_position_token(0)
	set_gold_found(false)
	set_starting_point_auto()
	if mode:
		set_ending_points_auto()
	else:
		set_ending_points(1,3,1,pepite_position)
	set_round_started(true)

#Initialisation de deux équipes (chercheurs et saboteurs)
func init_2_teams():
	var player_tmp
	if (self._teams.size() > 0):
		delete_teams()
	var saboteurs = Team.new("Saboteurs")
	var chercheurs = Team.new("Chercheurs")
	for i in range(get_nb_players()):
		player_tmp = self._players[i]
		if(player_tmp.get_saboteur()):
			saboteurs.add_player(player_tmp)
		else:
			chercheurs.add_player(player_tmp)
	add_team(saboteurs)
	add_team(chercheurs)

#Suppression des équipes
func delete_teams():
	for t in range(self._teams.size()):
		self._teams[t].free()
	self._teams.clear()

#Distribution des mains en fonction de la taille des mains
#param mode : vrai si la pioche est aléatoire, faux sinon
func give_hands(mode): #bool
	clear_hands()
	print("get_hand_length : " +str(get_hand_length()))
	for j in range(get_hand_length()):
		for i in range(self._players.size()):
			if(self._draw.size()>0):
				self._players[i].draw_card(self._draw,mode)

#Distribue les pépites d'or aux derniers gagnants
#précondition : à appeller uniquement après is_round_over retourne vrai
func give_gold():
	var p
	for i in range(self._players.size()):
		p = self._players[i]
		if (get_gold_found() and !p.get_saboteur()):
			p.give_gold(1)
		if (!get_gold_found() and p.get_saboteur()):
			p.give_gold(1)
	save_gold()

#Finalise la manche
func end_round():
	give_gold()
	save_stats_end_round()
	clear_draw()
	clear_hands()
	delete_teams()
	self._grid.clear_cells()
	set_gold_found(false)
	set_round_started(false)

#Finalise la partie
func end_game():
	set_winner()
	save_stats_end_game()

#Retourne si la pioche et les mains de tous les joueurs sont vides
func is_empty_hands():
	var player_hands_empty = true
	if (self._draw.size() == 0):
		for i in range(self._players.size()):
			if (self._players[i].get_hand().size() != 0):
				player_hands_empty = false
				break
	else:
		return false
	return player_hands_empty

#Retourne si c'est au tour d'un joueur donné
func is_turn(player): #Player
	return (self._position_token == player.get_position())

#Passe au prochain joueur
func next_player():
	self._position_token += 1
	if(self._position_token > get_nb_players() - 1):
		self._position_token = 0
	save_position_token()
	return self._position_token

#Initialise la pioche
#param mode : vrai si initialisation de la pioche à partir de la base de données, faux sinon
func init_draw(mode): #bool
	if (self._draw.size() > 0):
		clear_draw()
	#Si la base de données est connectée et que le mode est activé
	if(self._database_enabled) && mode:
		init_draw_sql()
	else: #Sinon insertion de quelques cartes types manuellement (debug)
		for i in range(8):
			var p1 = Path.new(1,true,true,true,true,false)
			var p2 = Path.new(8,true,false,true,false,false)
			var p3 = Path.new(22,true,true,true,true,true)
			var p4 = Path.new(22,true,true,true,true,true)
			self._draw.push_back(p1)
			self._draw.push_back(p2)
			self._draw.push_back(p3)
			self._draw.push_back(p4)
		for i in range(3):
			var ap1 = Action_on_player.new(12,true,false,false,true)
			var ap2 = Action_on_player.new(9,true,false,false,false)
			var ag1 = Action_on_grid.new(18,true, false, false)
			var ag2 = Action_on_grid.new(19,false, true, false)
			self._draw.push_back(ap1)
			self._draw.push_back(ap2)
			self._draw.push_back(ag1)
			self._draw.push_back(ag2)

#Initialise la pioche à partir de la base de données
func init_draw_sql():
	print("init_draw_sql")
	var quantity
	var num_champs
	var card_type
	var cur_card
	var result = self._mysql.new_query("select a.id_card, a.quantity, a.id_type, b.id_action_on_grid, b.reveal, b.remove, b.swap, c.id_action_on_player, c.positive, e.id_handicap, e.pickaxe, e.mine_cart, e.lantern, d.id_path, d.blocked, d.north, d.south, d.east, d.west from Card a left outer join Action_On_Grid b on a.id_action_on_grid = b.id_action_on_grid left outer join Action_On_Player c on a.id_action_on_player = c.id_action_on_player left outer join Path d on a.id_path = d.id_path left outer join Handicap e on c.id_handicap = e.id_handicap order by a.id_card")
	for i in range(1, result.size()+1):
		quantity = int(result[i]["quantity"])
		card_type = int(result[i]["id_type"])
		if (card_type == 1):
			for j in range(quantity):
				cur_card = Path.new(int(result[i]["id_card"]),bool(int(result[i]["north"])), bool(int(result[i]["east"])), bool(int(result[i]["south"])), bool(int(result[i]["west"])), bool(int(result[i]["blocked"])))
				if (cur_card != null):
					self._draw.push_back(cur_card)
		elif (card_type == 2):
			for j in range(quantity):
				cur_card = Action_on_player.new(int(result[i]["id_card"]),bool(int(result[i]["pickaxe"])),bool(int(result[i]["lantern"])) ,bool(int(result[i]["mine_cart"])), !bool(int(result[i]["positive"])))
				if (cur_card != null):
					self._draw.push_back(cur_card)
		elif (card_type == 3):
			for j in range(quantity):
				if (bool(int(result[i]["reveal"]))):
					cur_card = Action_on_grid.new(int(result[i]["id_card"]),true, false, false)
				elif (bool(int(result[i]["remove"]))):
					cur_card = Action_on_grid.new(int(result[i]["id_card"]),false, true, false)
				elif (bool(int(result[i]["swap"]))):
					cur_card = Action_on_grid.new(int(result[i]["id_card"]),false, false, true)
				if (cur_card != null):
					self._draw.push_back(cur_card)

#Vide la pioche
func clear_draw():
	self._draw.clear()

#Vide toutes les mains des joueurs
func clear_hands():
	for i in range (self._players.size()):
		self._players[i].clear_hand()

#Ajoute un joueur
func add_player(player): #Player
	player.set_position(self._players.size())
	self._players.push_back(player)

#Ajoute un joueur avec un identifiant d'utilisateur
func add_player_id(player,ur_id): #Player, int
	player.set_position(self._players.size())
	player.set_user_id(ur_id)
	self._players.push_back(player)

#Ajoute une équipe
func add_team(t): #Team
	self._teams.push_back(t)
	create_team(t)

#Distribue les rôles aléatoirement en fonction des règles du jeu (nombre de joueurs)
func give_roles():
	var nb_players = get_nb_players()
	if (nb_players < 3 || nb_players > 10):
		print("players must be between 3 n 10")
	var nb_saboteurs = 0
	var nb_cherchers = 0
	if (nb_players == 3 || nb_players == 4):
		nb_saboteurs = 1
		nb_cherchers = nb_players
	elif (nb_players == 5 || nb_players == 6):
		nb_saboteurs = 2
		nb_cherchers = nb_players - 1
	elif (nb_players >= 7 && nb_players >= 9):
		nb_saboteurs = 3
		nb_cherchers = nb_players - 2
	elif (nb_players == 10):
		nb_saboteurs = 4
		nb_cherchers = 7
	var roles = []
	for i in range(nb_saboteurs):
		roles.push_back(true)
	for i in range(nb_cherchers):
		roles.push_back(false)
	randomize()
	for k in range(nb_players):
		var index = randi()%roles.size()
		self._players[k].set_saboteur(roles[index])
		roles.remove(index)

#Retourne l'identifiant de la partie
func get_game_id():
	return self._game_id

#Retourne le nombre de joueurs
func get_nb_players():
	return self._players.size()

#Retourne la taille d'une main
func get_hand_length():
	return self._hand_length

#Retourne la position du token
func get_position_token():
	return self._position_token

#Retourne si on est dans un temps de jeu
func get_play_time():
	return self._play_time

#Retourne la manche courante
func get_round():
	return self._round

#Retourne si la manche a commencl
func get_round_started():
	return self._round_started

#Retourne si l'or a été trouvé
func get_gold_found():
	return self._gold_found

#Retourne la grille
func get_grid():
	return self._grid

#Retourne un joueur
func get_player(position): #int
	return self._players[position]

#Retourne les équipes
func get_teams():
	return self._teams

#Retourne les joueurs
func get_players():
	return self._players

#Retourne la pioche
func get_draw():
	return self._draw

#Retourne les gagnants des 3 manches
func get_round_winners():
	return self._round_winners

#Retourne un gagnant de la dernière manche
func get_round_winner(index): #int
	return self._round_winners[index]

#Retourne si la base de données est connectée
func get_database_enabled():
	return self._database_enabled

#Définit l'identifiant du jeu
func set_game_id(g_id): #int
	self._game_id = g_id

#Calcule la taille d'une main en fonction du nombre joueurs (règles du jeu)
func set_hand_length():
	var nbr = get_nb_players()
	if (nbr >= 3) && (nbr <= 5):
		self._hand_length = 6
	elif (nbr >= 6) && (nbr <= 7):
		self._hand_length = 5
	elif (nbr >= 8) && (nbr <= 10):
		self._hand_length = 4
	else:
		self._hand_length = 4
		print("set_hand_length warning : too many players.")

#Définit la position du token
func set_position_token(position): #int
	self._position_token = position
	save_position_token()

#Définit si on est dans un temps de jeu
func set_play_time(p_time): #bool
	self._play_time = p_time

#Définit la manche courant
func set_round(rd): #int
	self._round = rd
	save_round()

#Définit si la manche a commencée
func set_round_started(r_started): #bool
	self._round_started = r_started

#Définit la grille
func set_grid(gd):
	self._grid = gd
	save_dimensions()

#Définit les équipes
func set_teams(tms): #arrayy<Team>
	self._teams = tms

#Définit la pioche
func set_draw(drw): #arrayy<Card>
	self._draw = drw

#Définit les joueurs
func set_players(plrs): #arrayy<Player>
	self._players = plrs

#Définit si l'or a été trouvé
func set_gold_found(gf): #int
	self._gold_found = gf

#Retourne si la manche est finie
func is_round_over():
	if (is_empty_hands()):
		self._round_winners.push_back('S')
		return true
	if (self._grid.path_finding()): 
		self._gold_found = true
		self._round_winners.push_back('C')
		return true
	return false

#Supprimme la main d'un joueur à une position donnée
func discard_card(player_position,card_position):
	self._players[player_position].discard(card_position)

#Définit la position de la cellule de départ
func set_starting_point(y_line, x_center):
	self._grid.set_starting_point(y_line, x_center)

#Définit les positions des cellules des rochers et de la pépite
func set_ending_points(y_line, x_center, space, position):
	self._grid.set_ending_points(y_line, x_center, space, position)

#Définit automatiquement la case de départ
func set_starting_point_auto():
	var x_column = ((self._grid.get_width() + 1) / 2) - 1
	var y_line = self._grid.get_height() - 2
	print("set_starting_point_auto : x_column = " + str(x_column) + " ; y_line : " + str(y_line))
	self._grid.set_starting_point(y_line, x_column)

#Définit automatiquement les cellules d'arrivée
func set_ending_points_auto():
	var x_center = ((self._grid.get_width() + 1) / 2) - 1
	var y_line = 1
	print("set_ending_points_auto : x_center = " + str(x_center) + " ; y_line : " + str(y_line))
	self._grid.set_ending_points(y_line, x_center, 1, 0)

#Utilise une carte Path
func use_path_card(player_position, hand_position, line, column, rotation):
	return self._players[player_position].use_path_card(hand_position,self._grid,line,column,self._draw,rotation)

#Utilise une carte Action_on_player
func use_action_on_player_card(player_position, hand_position, target_position):
	return self._players[player_position].use_action_on_player_card(hand_position, self._players[target_position], self._draw)

#Utilise une carte Action_on_grid
func use_action_on_grid_card(player_position, hand_position, line, column):
	return self._players[player_position].use_action_on_grid_card(hand_position, self._grid, line, column, self._draw)

#Définit le/les gagnants de la partie
#Précondition : fin de dernière manche
func set_winner():
	var cur_player
	var cur_winner
	var max_gold = 0
	for i in range(self._players.size()):
		cur_player = self.get_player(i)
		if (max_gold < cur_player.get_gold()):
			cur_winner = cur_player
			max_gold = cur_player.get_gold()
	if cur_winner != null :
		cur_winner.set_winner(true)
	return cur_winner
#
#
#	SQL FUNCTIONS
#
#

#Ajoute un nouveau utilisateur à la partie à partir du login
func add_user(login): #String
	if(self._database_enabled):
		if(!user_exists(login)):
			print("the player %s doesn't exist !" % login)
			return false
		else:
			var p = Player.new(login)
			add_player(p)

#Ajoute un nouveau utilisateur à la partie à partir de son identifiant
func add_user_by_id(usr_id): #int
	if(self._database_enabled):
		if(!user_exists_by_id(usr_id)):
			print("the player %d doesn't exist !" % usr_id)
			return null
		else:
			var p = Player.new("Player%d" % usr_id)
			p.set_user_id(usr_id)
			add_player_id(p,usr_id)
			return p
	return null

#Génère un nouveau identifiant de jeu
func generate_game_id():
	if (self._database_enabled):
		var result = self._mysql.new_query("Select max(id_game) from Game")
		return int(result[1][""])+1

#Génère un nouveau identifiant de joueur
func generate_player_id():
	if (self._database_enabled):
		var result =  self._mysql.new_query("Select max(id_player) from Player")
		return int(result[1][""])+1

#Génère un nouveau identifiant d'équipe
func generate_team_id():
	if (self._database_enabled):
		var result = self._mysql.new_query("Select max(id_team) from Team")
		return int(result[1][""])+1

#Génère un nouveau identifiant d'utilisateur
func generate_user_id():
	if (self._database_enabled):
		var result = self._mysql.new_query("Select max(id_user) from User")
		return int(result[1][""])+1

#Génère un nouveau identifiant de statistique
func generate_stat_id():
	if (self._database_enabled):
		var result = self._mysql.new_query("Select max(id_stats) from Statistic")
		return int(result[1][""])+1

#Mets à jour les informations du jeu dans la BDD
func save_game_info():
	if (self._database_enabled):
		var query = "Update Game set token_position = %d, actual_round = %d, grid_width = %d, grid_height = %d where id_game = %d"
		self._mysql.execute(query % [self._position_token, self._round, self._grid.get_width(), self._grid.get_height(), self._game_id])
		return true
	return false

#Retourne si l'utilisateur existe à partir du login dans la BDD
func user_exists(login): #String
	if (login == null):
		return false
	if (self._database_enabled):
		var query = "Select count(distinct id_user) from User where login = '%s' "
		return (int(self._mysql.query(query % login, 1)[0]) > 0)

#Retourne si l'utilisateur existe à partir de l'identifiant dans la BDD
func user_exists_by_id(id_user): #int
	if (id_user == null):
		return false
	if (self._database_enabled):
		var query = "Select count(distinct id_user) from User where id_user = %d "
		return (int(self._mysql.query(query % id_user, 1)[0]) > 0)

#Retourne si le jeu existe à partir de l'dentifiant dans la BDD
func player_exists(id_pl): #int
	if (id_pl == null):
		return false
	if (self._database_enabled):
		var query = "Select count(distinct id_player) from Player where id_player = %d "
		return (int(self._mysql.query(query % id_pl, 1)[0]) > 0)

#Sauvegarde les statistiques de la manche dans la BDD
func save_stats_end_round():
	if (self._database_enabled):
		var cur_player = 0
		for i in range(self._players.size()):
			cur_player = self.get_player(i)
			var query = "Update Statistic set num_round_saboteur = num_round_saboteur + 1 where id_user = %d"
			self._mysql.execute(query % [cur_player.get_user_id()])
		return true
	return false

#Sauvegarde les statistiques de la partie dans la BDD
func save_stats_end_game():
	if (self._database_enabled):
		var cur_player = 0
		for i in range(self._players.size()):
			cur_player = self.get_player(i)
			var query = "Update Statistic set num_game = num_game + 1, num_win = num_win + %d, num_gold = num_gold + %d where id_user = %d"
			self._mysql.execute(query % [int(cur_player.get_winner()), cur_player.get_gold(), cur_player.get_user_id()])
		return true
	return false

#Mets à jour la manche courante dans la BDD
func save_round():
	if (self._database_enabled):
		var query = "Update Game set actual_round = %d where id_game = %d"
		self._mysql.execute(query % [self._round, self._game_id])
		return true
	return false

#Mets à jour le nombre de pépites de tous les joueurs dans la BDD
func save_gold():
	if (self._database_enabled):
		var cur_player = 0
		for i in range(self._players.size()):
			cur_player = self.get_player(i)
			var query = "Update Player set num_gold = %d where id_player = %d"
			self._mysql.execute(query % [cur_player.get_gold(), cur_player.get_player_id()])
		return true
	return false

#Mets à jour la position du token dans la BDD
func save_position_token():
	if (self._database_enabled):
		var query = "Update Game set token_position = %d where id_game = %d"
		self._mysql.execute(query % [self._position_token, self._game_id])
		return true
	return false

#Mets à jour les dimensions de la grille dans la BDD
func save_dimensions():
	if (self._database_enabled):
		var query = "Update Game set grid_width = %d, grid_height = %d where id_game = %d"
		self._mysql.execute(query % [self._grid.get_width(), self._grid.get_height(), self._game_id])
		return true
	return false


#Mets à jour les information d'un joueur dans la BDD
func save_player(p): #Player
	if (self._database_enabled):
		var query = "Update Player set num_gold = %d, position = %d, saboteur = %d, id_user = %d, id_team = %d where id_player = %d"
		self._mysql.execute(query % [p.get_gold(), p.get_position(), int(p.get_saboteur()), p.get_user_id(), p.get_team_id(), p.get_player_id()])
		return true
	return false

#Inère une nouveau utilisateur dans la BDD
func create_stat(usr_id): #int
	var stat_id = -1
	if (self._database_enabled):
		stat_id = generate_stat_id()
		var query = "Insert into Statistic (id_stats, num_game, num_win, num_gold, num_round_saboteur, id_user) values (%d,0,0,0,0,%d)"
		self._mysql.execute(query % [stat_id, usr_id])
	return stat_id

#Inère une nouvelle statistique dans la BDD
func create_user(logn,passwd,first_nm,nm): #String, String, String, String
	var user_id = -1
	if (self._database_enabled):
		user_id = generate_user_id()
		var query = "nsert into User (id_user, login, password, name, first_name) values (%d,'%s','%s','%s','%s')"
		self._mysql.execute(query % [user_id,logn,passwd,first_nm,nm])
	return user_id

#Inère une nouvelle équipe dans la BDD
func create_team(t): #Team
	var team_id = -1
	if (self._database_enabled):
		team_id = generate_team_id()
		t.set_team_id(team_id)
		var query = "Insert into Team values (%d,'%s',%d)"
		self._mysql.execute(query % [team_id, t.get_name(), self._game_id])
		var cur_player = 0
		for i in range(t.get_players().size()):
			cur_player = t.get_players()[i]
			cur_player.set_team_id(t.get_team_id())
			if (player_exists(cur_player.get_player_id())):
				save_player(cur_player)
			else:
				create_player(cur_player)
	return team_id

#Inère un nouveau joueur dans la BDD
func create_player(p): #Player
	var pl_id = -1
	if (self._database_enabled):
		pl_id = generate_player_id()
		p.set_player_id(pl_id)
		var query = "Insert into Player values (%d,'%s',%d,%d,%d,%d,%d)"
		print(query % [p.get_player_id(), p.get_name(), p.get_gold(), p.get_position()+1, int(p.get_saboteur()), p.get_user_id(), p.get_team_id()])
		self._mysql.execute(query % [p.get_player_id(), p.get_name(), p.get_gold(), p.get_position()+1, int(p.get_saboteur()), p.get_user_id(), p.get_team_id()])
	return pl_id

#Inère un nouveau jeu dans la BDD
func create_game():
	if (self._database_enabled):
		self._game_id = generate_game_id()
		var query = "Insert into Game values (%d,%d,3,%d,%d,%d)"
		self._mysql.execute(query % [self._game_id, self._position_token, self._round, self._grid.get_width(), self._grid.get_height()])
		return self._game_id
	return -1

#
#
#Fonctions réseaux
#
#

#Retourne la main d'un joueur
func res_get_player_hand(position): #int
	var hand_res = []
	var player_hand = self._players[position].get_hand()
	var cur_card
	for i in range (player_hand.size()):
		cur_card = player_hand[i]
		var card_row = []
		card_row.push_back(cur_card.get_card_id())
		if cur_card.get_object_type() == 'P':
			card_row.push_back(1)
		elif cur_card.get_object_type() == 'J':
			card_row.push_back(2)
		elif cur_card.get_object_type() == 'G':
			card_row.push_back(3)
		hand_res.push_back(card_row)
	return hand_res

#Retourne toutes les mains de tous les joueurs
func res_get_players_hand():
	var hand_lines = []
	for i in range (self._players.size()):
		var hand_columns = []
		hand_columns.push_back(i)
		var player_tmp = self._players[i]
		var player_hand = player_tmp.get_hand()
		for j in range (player_hand.size()):
			hand_columns.push_back(player_hand[j].get_card_id())
		hand_lines.push_back(hand_columns)
	return hand_lines

#Retourne les informations d'un joueur
func res_get_player_info(position):
	var res = []
	res.push_back(self._players[position].get_gold())
	res.push_back(self._players[position].get_handicap(0))
	res.push_back(self._players[position].get_handicap(1))
	res.push_back(self._players[position].get_handicap(2))
	return res
