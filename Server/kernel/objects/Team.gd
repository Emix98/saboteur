extends Node

var _team_id #Identifiant de l'équipe
var _game_id #Identifiant de la partie
var _team_name #Nom de l'équipe
var _players = [] #Joueurs de

#Constructeur
func _init(team_name): #String
	self._team_id = -1
	self._game_id = -1
	self._team_name = team_name

#Retourne un joueur
func get_player(position): #int
	return self._players[position]

#Retourne les joueurs
func get_players():
	return self._players

#Définit les joueurs 
func set_players(players): #array<Player>
	self._players

#Définit l'identifiant de l'équipe 
func set_team_id(tm_id): #int
	self._team_id = tm_id

#Retourne l'identifiant de l'équipe
func get_team_id():
	return self._team_id

#Ajoute un joueur
func add_player(player): #Player
	self._players.push_back(player)

#Supprimme un joueur
func delete_player(player): #Player
	for i in range(self._players.size()):
		if (self._players[i] == player):
			self._players.remove(i)

#Supprimme un joueur à une position données
func delete_player_at(position): #int
	self._players.remove(position)

#Retourne le nom de l'équipe
func get_name():
	return self._team_name

#Définit le nom de l'équipe
func set_name(nm): #String
	self._team_name = nm