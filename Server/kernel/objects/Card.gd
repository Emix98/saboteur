extends Node

var _object_type #type de la carte (instanciation)
var _card_id #identifiant de la carte

#contructeur
func _init():
	self._object_type = 'C' #type carte Card

#retourne le type de la carte (instanciation)
func get_object_type():
	return self._object_type

#retourne l'identifiant de la carte
func get_card_id():
	return self._card_id