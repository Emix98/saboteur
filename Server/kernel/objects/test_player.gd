extends 'res://addons/gut/test.gd'

var Card = load("res://kernel/objects/Card.gd")
var Path = load("res://kernel/objects/Path.gd")
var Action = load("res://kernel/objects/Action.gd")
var Cell = load("res://kernel/objects/Cell.gd")
var Grid = load("res://kernel/objects/Grid.gd")
var Action_on_grid = load("res://kernel/objects/Action_on_grid.gd")
var Player = load("res://kernel/objects/Player.gd")
var Team = load("res://kernel/objects/Team.gd")
var Action_on_player = load("res://kernel/objects/Action_on_player.gd")
var Game = load("res://kernel/objects/Game.gd")

func test_players():
	var handicaps = [false, false, false]
	var p1name = "Joueur 1"
	var p2name = "Joueur 2"
	var p3name = "Joueur 3"
	
	var player1 = Player.new(1, true, p1name, handicaps, 0, 9, 55555)
	var player2 = Player.new(2, false, p2name, handicaps, 0, 2, 66666)
	var player3 = Player.new(4, true, p3name, handicaps, 0, 4, 44444)
	
	# Tests joueur 1
	assert_eq(player1.get_name(), p1name, "Nom du joueur 1 incorrect!")
	for i in range(3):
		assert_eq(player1.get_handicap(i), handicaps[i], "Handicaps du joueur 1 incorrects!")
		
	# Tests joueur 2
	assert_eq(player2.get_name(), p2name, "Nom du joueur 2 incorrect!")
	for i in range(3):
		assert_eq(player2.get_handicap(i), handicaps[i], "Handicaps du joueur 2 incorrects!")
		
	# Tests joueur 3
	assert_eq(player3.get_name(), p1name, "Nom du joueur 3 incorrect!")
	for i in range(3):
		assert_eq(player3.get_handicap(i), false, "Handicaps du joueur 3 incorrects!")
		
	# Ajout d'une position et d'un rôle au joueur 3
	player3.set_position(3)
	assert_eq(player3.get_position(), 3, "Position du joueur 3 incorrecte")
	player3.set_saboteur(false)
	assert_false(player3.get_saboteur(), "Le joueur 3 ne devrait pas être saboteur")
	
	# Initialisation pioche
	var draw = []
	for i in range(12):
		draw.push_back(Card.new())
	assert_eq(draw.size(), 12, "Erreur initialisation pioche")
	
	# Constitution de la main du joueur 1
	player1.draw_hand(draw, 5)
	assert_eq(player1.get_hand().size(), 5, "Le joueur 1 n'a pas 5 cartes")
	
	# Constitution d'une main de taille négative
	player2.draw_hand(draw, -3)
	assert_eq(player2.get_hand().size(), 0, "Le joueur 2 a des cartes")
	
	# Constitution de la main du joueur 2
	player2.draw_hand(draw, 4)
	assert_eq(player2.get_hand().size(), 4, "Le joueur 2 n'a pas 4 cartes")
	
	# Défausse d'une carte du joueur 1
	player1.discard(1)
	assert_eq(player1.get_hand().size(), 4, "Joueur 1 devrait avoir perdu une carte")
	for i in range(6):
		player2.discard(0)
	assert_eq(player2.get_hand().size(), 0, "Le joueur 2 devrait avoir 0 cartes")
	player1.discard(-1)
	assert_eq(player1.get_hand().size(), 4, "Joueur 1 ne devrait pas avoir perdu de carte")
	
	for i in range(4):
		player2.draw_card(draw)
		assert_eq(draw.size(), 3-(i+1) if (3-(i+1)) > 0 else 0, "La pioche ne devrait pas avoir autant de cartes")
		
	# Application d'un handicap 2 au joueur 1 et de handicap 0,1 au joueur 2
	player1.set_handicap(2, true)
	assert_true(player1.get_handicap(2), "Handicap 2 non appliqué au joueur 1")
	player2.set_handicap(0, true)
	assert_true(player1.get_handicap(0), "Handicap 0 non appliqué au joueur 2")
	player2.set_handicap(1, true)
	assert_true(player1.get_handicap(1), "Handicap 1 non appliqué au joueur 2")
	# Suppression du handicap 0 au joueur 1
	player1.set_handicap(0, false)
	assert_false(player1.get_handicap(0), "Joueur 1 n'est pas sensé avoir un handicap 0")
	player2.set_handicap(1, false)
	assert_false(player2.get_handicap(1), "Joueur 2 n'est plus censé avoir un handicap 1")
	
	# Ajout d'une carte handicap 0 dans la pioche et don de cette carte au joueur 2
	var handicap0 = Action_on_player.new();
	handicap0.assign_card('c', false)
	assert_true(handicap0.get_handicap_item(0), "La carte handicap0 n'a pas de handicap chariot")
	assert_false(handicap0.get_handicap_item(1), "La carte handicap0 ne devrait pas avoir de handicap lampe")
	assert_false(handicap0.get_handicap_item(2), "La carte handicap0 ne devrait pas avoir de handicap pioche")
	draw.push_back(handicap0)
	
	player2.draw_card(draw)
	
	# Application de la carte handicap au joueur 1
	for i in range(player2.get_hand().size()):
		player2.use_action_on_player_card(player2.get_hand()[i], player1)
	assert_true(player1.get_handicap(0), "Joueur 1 devrait avoir un handicap 0")	
	assert_false(player1.get_handicap(1), "Joueur 1 ne devrait pas avoir de handicap 1")
	assert_true(player1.get_handicap(2), "Joueur 1 ne devrait avoir un handicap 2")
	
	# Ajout de 4 d'or au joueur 1 et 2 d'or au joueur 2
	player1.give_gold(4)
	player2.give_gold(2)
	assert_eq(player1.get_gold(), 4, "Joueur 1 devrait avoir 4 d'or")
	assert_eq(player2.get_gold(), 2, "Joueur 2 devrait avoir 2 d'or")
	# Retrait de 6 or au joueur 1, devrait en avoir 0
	player1.give_gold(-6)
	assert_eq(player1.get_gold(), 0, "Joueur 1 devrait avoir 0 d'or")
	# Don de trois d'or au joueur 1
	player1.give_gold(3)
	assert_eq(player1.get_gold(), 3, "Joueur 1 devrait avoir 3 d'or")
	
	
	
	
	
	
	