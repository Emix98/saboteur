extends Node

var _position #Position du joueur (ordre de jeu)
var _is_saboteur #Boolean si saboteur
var _player_name #Nom du joueur
var _handicap = [] #Handicaps du joueur (pioche,lanterne,chariot)
var _hand = [] #Main (cartes)
var _total_gold #Pépites d'or cumulées
var _player_id #Identifiant du joueur
var _team_id #Identifiant de l'équipe
var _user_id #Identifiant de l'utilisateur
var _is_winner #Boolean si gagnant
var _played_cards #Nombre de cartes jouées ce tour
var _discarded_cards #Nombre de cartes discard ce tour

#Constructeur
func _init(player_name): #String
	self._player_name = player_name
	self._position = 0
	self._is_saboteur = false
	self._user_id = -1
	self._team_id = -1
	self._player_id = -1
	self._played_cards = 0
	self._discarded_cards = 0
	for i in range(3):
		self._handicap.insert(i,false)
	self._total_gold = 0
	self._is_winner = false

#Retourne le nom du joueur
func get_name():
	return self._player_name

#Retourne si c'est un saboteur
func get_saboteur():
	return self._is_saboteur

#Retourne la main
func get_hand():
	return self._hand

#Retourne une carte à une position données
func get_card(pos): #int
	return self._hand[pos]

#Retourne la position du joueur
func get_position():
	return self._position

#Définit si c'est le gagnant
func set_winner(winner): #bool
	self._is_winner=winner

#Retourne si c'est le gagnant
func get_winner():
	return self._is_winner

#Retourne le handicap demandé
func get_handicap(position):
	if (position >= 0) && (position < 3):
		return self._handicap[position]
	return false

#Retourne les handicaps
func get_handicaps():
	return self._handicap

#Retourne les pépites d'or gagnées
func get_gold():
	return self._total_gold

#Retourne l'identifiant du joueur
func get_player_id():
	return self._player_id

#Retourne l'identifiant de l'utilisateur
func get_user_id():
	return self._user_id

#Retourne l'identifiant de l'équipe
func get_team_id():
	return self._team_id

#Définit la main
func set_hand(hnd): #array<Card>
	self._hand = hnd

#Définit un handicap
func set_handicap(handicap,apply): #int, bool
	if (handicap >= 0) && (handicap < 3):
		self._handicap[handicap] = apply
		return true
	print("set_handicap error : handicap existant")
	return false

#Définit si c'est un saboteur
func set_saboteur(saboteur): #bool
	self._is_saboteur = saboteur

#Définit la position du joueur
func set_position(pos): #int
	if (pos > 0):
		self._position = pos

#Définit l'identifiant de l'utilisateur
func set_user_id(ur_id): #int
	self._user_id = ur_id

#Définit l'identifiant du joueur
func set_player_id(pl_id): #int
	self._player_id = pl_id

#Définit l'identifiant de l'équipe
func set_team_id(tm_id): #int
	self._team_id = tm_id

#Donne une somme de pépites
func give_gold(gold): #int
	self._total_gold += gold
	if self._total_gold < 0:
		self._total_gold = 0

#Donne le nombre de cartes jouées à ce tour
func get_played_cards():
	return self._played_cards + self._discarded_cards # should never be > 3
	
#Reset le nombre de carte jouées
func reset_card_counts():
	self._played_cards = 0
	self._discarded_cards = 0
	
#Pioche une carte dans la pioche données
#Si mode à true alors bioche aléatoire, sinon en tête de pioche
func draw_card(draw, mode): #array<Card>, bool
	if (draw.size() != 0):
		if(!mode):
			self._hand.push_back(draw[0])
			draw.remove(0)
		else:
			randomize()
			var drawn_card = randi()%draw.size()
			self._hand.push_back(draw[drawn_card])
			draw.remove(drawn_card)
	else:
		print("draw_card error : pioche vide !")

#Pioche un nombre d'itérations donné
func draw_hand(draw,size): #array<Card>, int
	if(size >= 0):
		for i in range(size):
			draw_card(draw,true)
	else:
		print("draw_hand error : taille passée négative !")

#Supprimme une carte de la main
func discard(index): #int
	if (index >= 0) && (index < self._hand.size()) && (self._played_cards < 1) && (self._discarded_cards <= 3):
		self._hand.remove(index)
		self._discarded_cards += 1
		print(self._discarded_cards)
		print(self._played_cards)
		return true
	else:
		print("discard error : carte inexistate.")
		return false

#Utilise une carte chemin
func use_path_card(hand_position,grid,line,column,draw,rotation): #int, Grid, int, int, array<Card>, int
	#Vérification des arguments
	if self._handicap[0] || self._handicap[1] || self._handicap[2]:
		print("use_path_card error : Le joueur a un handicap.")
		return false
	if(draw.size() < hand_position):
		print("use_path_card error : La position n'existe pas.")
		return false
	if self._hand[hand_position].get_object_type() != 'P':
		print("use_path_card error :  la carte n'est pas un Path.")
		return false
	if self._played_cards >= 1 || self._discarded_cards >= 1:
		print("use_path_card error : une carte a déjà été jouée (sur grid ou player) ou 3 cartes on été discard") 
		return false
	var test = grid.add_path(self._hand[hand_position],line,column,rotation)
	if test:
		self._played_cards += 1
#		discard(hand_position)
#		draw_card(draw,true)
	return test

#Utilise une carte Action_on_player
func use_action_on_player_card(hand_position, target, draw): #int, Player, array<Card>
	#Vérification des arguments
	print("action on player")
	if (draw.size() < hand_position):
		print("use_cation_player_card error : position de la carte inexistante.")
		return false
	var card = self._hand[hand_position]
	if card.get_object_type() != 'J':
		print("use_cation_player_card error :  la carte n'est pas un Action_on_player.")
		return false
	if self._played_cards >= 1 || self._discarded_cards >= 1:
		print("use_path_card error : une carte a déjà été jouée (sur grid ou player) ou 3 cartes on été discard") 
		return false
	#cast de card vers action_on_player
	card.apply(target)
	self._played_cards += 1
#	discard(hand_position)
#	draw_card(draw,true)
	return true

#Utilise une carte Action_on_grid
func use_action_on_grid_card(hand_position, grid, line, column, draw): #int, Grid, int, int, array<Card>
	#Vérification des arguments
	print("action on grid")
	if (draw.size() < hand_position):
		print("use_action_on_grid_card error : position de la carte inexistante.")
		return false
	var card = self._hand[hand_position]
	if card.get_object_type() != 'G':
		print("use_action_on_grid_card error :  la carte n'est pas un Action_on_grid.")
		return false
	if self._played_cards >= 1 || self._discarded_cards >= 1:
		print("use_path_card error : une carte a déjà été jouée (sur grid ou player) ou 3 cartes on été discard") 
		return false
	#Cast de card vers action_on_grid
	var return_test = card.apply(grid, line, column)
	if (!return_test):
		if card.is_reveal():
			print("is_reveal")
			self._played_cards += 1
#			discard(hand_position)
#			draw_card(draw,true)
		return false
	self._played_cards += 1
#	discard(hand_position)
#	draw_card(draw,true)
	return true

#Vide la main
func clear_hand():
	self._hand.clear()