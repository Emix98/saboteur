extends "res://kernel/objects/Action.gd"

var _reveal #carte révélation
var _remove #carte suppression de chemin
var _swap #carte échange de carte chemin

#constructeur
func _init(id, reveal, remove, swap):#int,bool,bool,bool
	self._reveal = reveal 
	self._remove = remove
	self._swap = swap
	self._object_type = 'G' #type Action_on_grid
	self._card_id = id #id de la carte

#retourne si carte révélation
func is_reveal(): 
	return self._reveal

#retourne si carte suppression de chemin
func is_remove():
	return self._remove

#retourne si carte échange de carte chemin
func is_swap():
	return self._swap

#application de la carte sur la grille
func apply(grid, line, column):#Grid,int,int
	#récupération de la cellule
	var current_cell = grid.get_cell(line, column) 
	if (self._reveal): #si carte révélation
		if (current_cell.is_gold()): #si la cellule est une pépite
			return true
	elif (self._remove): #si carte suppression
		if (!(current_cell.is_indestructible())) && (!current_cell.is_empty()):
			current_cell.reset_all()
			return true
	elif (self._swap): #si carte échange
		#PAS IMPLEMENTE POUR LE MOMENT
		return false
	return false