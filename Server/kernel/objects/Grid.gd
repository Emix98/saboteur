extends Node

var _height #Hauteur de la grille
var _width #Largeur de la grille
var _cells = [] #Cellules
var _gold_position #Position de la pépite
var _starting_point = [] #Coordonnées du point de départ
var _ending_points = [] #Coordonnées des points d'arrivée

var Cell = load("res://kernel/objects/Cell.gd")

#constructeur
func _init(h=11, w=7): #int, int
	#vérification des dimensions
	if (w%2 != 1) || (w < 5) || (h < 11):
		print("_init:Grid error : les dimensions de la grille doivent être plus hautes que 11 et plus larges que 5, la largeur doit être un chiffre impaire")
		print("les dimensions de la grille ont été définies par défaut : 11x5")
		print("h = " +str(h)+ ", w = " +str(w))
		self._height = 11
		self._width = 5
	else:
		self._height = h
		self._width = w
	#initialisation des cellules
	for i in range(self._height):
		var cells_tmp = []
		for j in range(self._width):
			cells_tmp.push_back(Cell.new())
		self._cells.push_back(cells_tmp)
	self._starting_point = [0,0]
	self._ending_points = [[0,0], [0,0], [0,0]]

#Définit la largeur
func set_width(size): #int
	self._width = size

#Définit la hauteur
func set_height(size): #int
	self._height = size

#Retourne la largeur
func get_width():
	return self._width

#Retourne la hauteur
func get_height():
	return self._height

#Retourne les coordonnées du point de départ
func get_starting_point():
	return self._starting_point

#Retourne les coordonnées des points d'arrivée
func get_ending_points():
	return self._ending_points

#Vérifie l'autorisation du placement d'une carte chemin sur une cellule
#En fonction des cartes environnantes et de la cellule elle-même
func try_placement(path, line, column, rotation): #Path, int, int, int
	#Récupération des frontières pour la rotation
	var north = path.get_self_path()[0]
	var est = path.get_self_path()[1]
	var sud = path.get_self_path()[2]
	var west = path.get_self_path()[3]
	var tmp
	#rotation
	var iteration = rotation % 4
	if rotation < 0:
		iteration = 4 - rotation % 4
	while(iteration > 0):
		tmp = west
		west = sud
		sud = est
		est = north
		north = tmp
		iteration = iteration - 1
	#vérifications des arguments
	if (line < 0) || (line >= self._height) || (column < 0) || (column >= self._width):
		print("try_placement error : coordonnées invalides.")
		return false
	#vérification si la cellule est vide
	if (!self._cells[line][column].is_empty()):
		print("try_placement error : la cellule n'est pas vide")
		return false
	var surrounding_path = false
	#test la cohérence du placement avec les cellules environnantes
	if (line - 1 >= 0):
		if (self._cells[line-1][column].get_type() == 'P') || (self._cells[line-1][column].get_type() == 'S'):
			surrounding_path = true
			if self._cells[line-1][column].get_self_path(2) != north:
				print("try_placement : case haut incompatible")
				return false
	if (column + 1 >= 0 && column + 1 < self._width):
		if (self._cells[line][column+1].get_type() == 'P') || (self._cells[line][column+1].get_type() == 'S'):
			surrounding_path = true
			if self._cells[line][column+1].get_self_path(3) != est:
				print("try_placement : case droite incompatible")
				return false
	if (line + 1 >= 0 && line + 1 < self._height):
		if (self._cells[line+1][column].get_type() == 'P') || (self._cells[line+1][column].get_type() == 'S'):
			surrounding_path = true
			if self._cells[line+1][column].get_self_path(0) != sud:
				print("try_placement : case bas incompatible")
				return false
	if (column - 1 >= 0):
		if (self._cells[line][column-1].get_type() == 'P') || (self._cells[line][column-1].get_type() == 'S'):
			surrounding_path = true
			if self._cells[line][column-1].get_self_path(1) != west:
				print("try_placement : case gauche incompatible")
				return false
	if (!surrounding_path):
		print("try_placement : aucun chemin autour")
	return surrounding_path

#Ajoute un chemin à des coordonnées données
func add_path(card, line, column, rotation): #Path, int, int, int
	if (card.get_object_type() != 'P'):
		print("add_path error : la carte n'est pas un Path")
		return false
	#Récupération des frontières pour la rotation
	var north = card.get_self_path()[0]
	var est = card.get_self_path()[1]
	var sud = card.get_self_path()[2]
	var west = card.get_self_path()[3]
	var tmp
	var iteration = rotation % 4
	if rotation < 0:
		iteration = 4 - rotation % 4
	#rotation
	while(iteration > 0):
		tmp = west
		west = sud
		sud = est
		est = north
		north = tmp
		iteration = iteration - 1
	#vérifications du placement
	if (!try_placement(card, line, column, rotation)):
		return false
	self._cells[line][column].set_path(north,est,sud,west,card.is_blocked())
	return true

#retourne la cellule aux coordonnées données
func get_cell(line, column): #int, int
	return self._cells[line][column]

#formate une cellule aux coordonnées données
func reset_cell(line, column): #int, int
	self._cells[line][column].reset_all()

#Définit la position de la case de départ
func set_starting_point(line, column): #int, int
	#vérification des arguments
	if(self._width < column + 1) || (column - 1 < 0):
		print("starting_point error : coordonnées x invalides.")
		return false
	if(self._height < line + 2) || (line < 0):
		print("starting_point error : coordonnées y invalides.")
		return false
	self._starting_point[0] = line;
	self._starting_point[1] = column;
	self._cells[line][column].set_start()
	return true

#Définit la position des cases d'arrivée
func set_ending_points(line, column_center, space, position): #int, int, int, int
	#vérification des arguments
	if(self._width < column_center + space + 1) || (column_center - space - 1 < 0):
		print("starting_point error : coordonnées x invalides.")
		return false
	if(self._height < line + 2) || (line < 0):
		print("starting_point error : coordonnées y invalides.")
		return false
	self._ending_points[0][0] = line
	self._ending_points[0][1] = column_center - space - 1
	self._ending_points[1][0] = line
	self._ending_points[1][1] = column_center
	self._ending_points[2][0] = line
	self._ending_points[2][1] = column_center + space + 1
	var arrival_number = 0
	#placement aléatoire de la pépite si la position en argument vaut 0
	randomize()
	if(position==0):
		arrival_number = randi()%3
	else:
		arrival_number = position -1
	if arrival_number == 0:
		self._cells[line][self._ending_points[0][1]].set_gold()
		self._cells[line][self._ending_points[1][1]].set_rock()
		self._cells[line][self._ending_points[2][1]].set_rock()
	if arrival_number == 1:
		self._cells[line][self._ending_points[0][1]].set_rock()
		self._cells[line][self._ending_points[1][1]].set_gold()
		self._cells[line][self._ending_points[2][1]].set_rock()
	if arrival_number == 2:
		self._cells[line][self._ending_points[0][1]].set_rock()
		self._cells[line][self._ending_points[1][1]].set_rock()
		self._cells[line][self._ending_points[2][1]].set_gold()
	self._gold_position = arrival_number + 1
	return true

#Retourne la position de la pépite d'or
func get_gold_position():
	return self._gold_position

#Supprimme toutes les cellules destructibles
func clear_destructible_cells():
	for i in range(self._height):
		for j in range(self._width):
			if (!self._cells[i][j].is_indestructible()):
				self._cells[i][j].reset_all()

#Supprimme toutes les cellules
func clear_cells():
	for i in range(self._height):
		for j in range(self._width):
				self._cells[i][j].reset_all()

#Recherche d'un chemin avec un parcours en profondeur
func path_finding():
	var count_path = 0
	#comptage du nombre de cellules chemins avant tests (optimisation)
	for i in range(self._height):
		for j in range(self._width):
			if (self._cells[i][j].get_type() == 'P'):
				count_path += 1
	#si assez de chemins pour arriver à une pépite, recherche par un parcours en profondeur
	if count_path >= (self._starting_point[0] - self._ending_points[1][0] - 1):
		var line = self._starting_point[0]
		var column = self._starting_point[1]
		var cell_vector = [] #tableau stockant les cellules marquées
		if (path_finding_aux(cell_vector, line, column)):
			#démarquage des cellules
			for i in range(cell_vector.size()):
				cell_vector[i].set_marked(false)
			#delete (cell_vector)
			return true
		#démarquage des cellules
		for i in range(cell_vector.size()):
			cell_vector[i].set_marked(false)
		#delete (cell_vector)
	return false

#Fonction auxiliaire pour le parcours en profondeur
func path_finding_aux(cell_vector, line, column): #array, int, int
	self._cells[line][column].set_marked(true)
	cell_vector.push_back(self._cells[line][column])
	var cur_cell = self._cells[line][column]
	#parcours les voisins si un chemin existe jusqu'à trouver la pépite d'or
	if (line - 1 > 0) && cur_cell.get_self_path(0): #haut
		var up_cell = self._cells[line-1][column]
		if up_cell.get_type() == 'G':
			return true
		if(up_cell.get_type() == 'P') && (!up_cell.is_blocked()) && (!up_cell.get_marked()):
			if (path_finding_aux(cell_vector, line-1, column)):
				return true
	if ((column + 1) < self._width) && cur_cell.get_self_path(1): #droite
		var right_cell = self._cells[line][column+1]
		if right_cell.get_type() == 'G':
			return true
		if (right_cell.get_type() == 'P') && (!right_cell.is_blocked()) && (!right_cell.get_marked()):
			if (path_finding_aux(cell_vector, line, column + 1)):
				return true
	if ((line + 1) < self._height) && cur_cell.get_self_path(2): #bas
		var down_cell = self._cells[line + 1][column]
		if down_cell.get_type() == 'G':
			return true;
			if (down_cell.get_type() == 'P') && (!down_cell.is_blocked()) && (!down_cell.get_marked()):
				if (path_finding_aux(cell_vector, line + 1, column)):
					return true
	if ((column - 1) > 0) && cur_cell.get_self_path(3): #gauche
		var left_cell = self._cells[line][column - 1]
		if (left_cell.get_type() == 'G'):
			return true;
		if (left_cell.get_type() == 'P') && (!left_cell.is_blocked()) && (!left_cell.get_marked()):
			if (path_finding_aux(cell_vector, line, column - 1)):
				return true
	return false