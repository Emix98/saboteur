extends Node

var _path = [] #directions chemin : nord, est, sud, ouest
var _blocked #boolean chemin bloqué
var _indestructible #boolean cheim indestructible
var _type #type de la cellule : P(chemin), V(vide), G(or), R(rocher), S(départ), O(obstacle)
var _marked #marquage pour la parcours de chemin vers la pépite

#constructeur
func _init():
	self._blocked = false 
	self._indestructible = false
	self._path = [false,false,false,false]
	self._type = 'V'

#définit la cellule en pépite
func set_gold():
	self._type = 'G' 
	self._indestructible = true
	self._blocked = true

#retourne si la cellule est une pépite
func is_gold():
	return self._type == 'G'

#définit la cellule comme vide
func set_empty():
	self._type = 'V'
	reset_all()

#retourne si la cellule est vide
func is_empty():
	return self._type == 'V'

#retourne si indestructible
func is_indestructible():
	return self._indestructible

#définit comme indestructible
func set_indestructible():
	self._indestructible = true

#définit si c'est indestructible
func set_indestructible_by_val(bol): #bool
	self._indestructible = bol

#retire l'indestructibilité
func reset_indestructible():
	self._indestructible = false

#retourne si c'est bloqué
func is_blocked():
	return self._blocked

#définit si c'est bloqué
func set_blocked_by_val(val): #bool
	self._blocked = val

#Retire le bloquage
func reset_blocked():
	self._blocked = false

#Définit le chemin de la cellule
func set_path(n,e,s,w,b): #bool, bool, bool, bool, bool
	self._path[0] = n
	self._path[1] = e
	self._path[2] = s
	self._path[3] = w
	self._blocked = b
	self._indestructible = false
	self._type = 'P'

#Retourne le chemin de la cellule
func get_self_path(i): #int
	return self._path[i]

#Supprime le chemin
func reset_path():
	for i in range(4):
		self._path[i] = false

#retourne le type de la cellule
func get_type():
	return self._type

#Définit la cellule en rocher
func set_rock():
	self._type = 'R'
	self._indestructible = true
	self._blocked = true

#Formate la cellule et la vide
func reset_all():
	reset_blocked()
	reset_indestructible()
	reset_path()
	self._type = 'V'

#Définit la cellule en obstacle
func set_obstacle():
	self._type = 'O'
	self._indestructible = false
	self._blocked = true

#Définit la cellule en départ
func set_start():
	self._type = 'S'
	for i in range(4):
		self._path[i] = true
	self._indestructible = true
	self._blocked = false

#Marque la cellule (parcours de chemin)
func set_marked(bol): #bool
	self._marked = bol

#Retourne le marquage
func get_marked():
	return self._marked