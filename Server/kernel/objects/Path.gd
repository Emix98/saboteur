extends "res://kernel/objects/Card.gd"

var _path = [] #Directions du chemin : Nord, Ouest, Sud, Est
var _blocked #Boolean bloquage du chemin

#Constructeur
func _init(id, Nord, Est, Sud, Ouest, is_blocked): #int, bool, bool, bool, bool, bool 
	self._object_type = 'P'
	self._path.insert(0,Nord);
	self._path.insert(1,Est);
	self._path.insert(2,Sud);
	self._path.insert(3,Ouest);
	self._blocked = is_blocked;
	self._card_id = id

#Retourne le chemin
func get_self_path():
    return self._path

#Retourne si c'est un bloquage
func is_blocked():
    return self._blocked