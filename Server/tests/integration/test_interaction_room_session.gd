extends 'res://addons/gut/test.gd'

var Session = load("res://scripts/objects/Session.gd")
var Room = load("res://scripts/objects/Room.gd")

var _session
var _room
var creator_session
var player15_session
var player23_session
	
const ROOM_CREATOR = 13
const A_PLAYER_15 = 15
const A_PLAYER_23 = 23
const A_PLAYER_56 = 56


# simulation of 4 connections to the server
func before_each():
	get_tree().emit_signal("network_peer_connected", ROOM_CREATOR)
	get_tree().emit_signal("network_peer_connected", A_PLAYER_15)
	get_tree().emit_signal("network_peer_connected", A_PLAYER_23)
	get_tree().emit_signal("network_peer_connected", A_PLAYER_56)
	ServerFacade.create_room("room name", 3, "password123", ROOM_CREATOR)
	creator_session = global.connected_session[ROOM_CREATOR]
	_room = global.game_rooms[creator_session.get_room_id()]
	player23_session = global.connected_session[A_PLAYER_23]
	player15_session = global.connected_session[A_PLAYER_15]
	
func after_each():
	get_tree().emit_signal("network_peer_disconnected", ROOM_CREATOR)
	get_tree().emit_signal("network_peer_disconnected", A_PLAYER_15)
	get_tree().emit_signal("network_peer_disconnected", A_PLAYER_23)
	get_tree().emit_signal("network_peer_disconnected", A_PLAYER_56)

# test get_player_number
func test_player_number():
	assert_eq(_room.get_player_number(), 1, "There should be one player in this room")

# test join_room
func test_session_overload():
	assert_false(ServerFacade.add_player_to_room("", _room.get_id(), "password123", ROOM_CREATOR), "13 shouldn't be added because he already is in this room")
	assert_true(ServerFacade.add_player_to_room("", _room.get_id(), "password123", A_PLAYER_23), "23 should be added to the room")
	assert_eq(_room.get_player_number(), 2, "There should be 2 players in this room")
	assert_true(ServerFacade.add_player_to_room("", _room.get_id(), "password123", A_PLAYER_15), "15 should be added to the room")
	assert_false(ServerFacade.add_player_to_room("", _room.get_id(), "password123", A_PLAYER_56), "56 shouldn't be added to the room")
		

# test change_host	
func test_change_host():
	_room.change_host(12)
	assert_eq(_room.get_admin_id(), ROOM_CREATOR, "Admin shouldn't change, 12 isn't in the room")
	assert_true(ServerFacade.add_player_to_room("", _room.get_id(), "password123", A_PLAYER_23), "23 should be added to the room")
	_room.change_host(23)
	assert_eq(_room.get_admin_id(), A_PLAYER_23, "Admin should have changed")

# tester get_sessions_list
func test_get_sessions_list():
	assert_true(ServerFacade.add_player_to_room("", _room.get_id(), "password123", A_PLAYER_15), "15 should be added to the room")
	var sessions_list = _room.get_session_list()
	assert_eq(sessions_list.size(), 2, "There should be 2 sessions")
#	assert_eq(sessions_list[0].get_id(), 13, "The session of id 13 should be in this list")
#	assert_eq(sessions_list[1].get_id(), 15, "The session of id 15 should be in this list")
#	assert_eq(sessions_list[0].get_room_id(), _room.get_id(), "The session should have a room id equal to this room")
	

# tester how_much_are_ready
func test_how_much_are_ready():
	assert_eq(_room.how_much_are_ready(), 0, "There shouldn't be any player ready")
	assert_true(ServerFacade.add_player_to_room("", _room.get_id(), "password123", A_PLAYER_15), "15 should be added to the room")
	ServerFacade.change_ready_state(A_PLAYER_15, "ready")
	assert_true(player15_session.is_ready(), "15 should be ready")
	assert_eq(_room.how_much_are_ready(), 1, "There shoul be one player ready")

# tester kick_id
func test_kick_session_by_id():
	assert_true(ServerFacade.add_player_to_room("", _room.get_id(), "password123", A_PLAYER_15), "15 should be added to the room")
	assert_eq(_room.get_player_number(), 2, "There should be 2 players in this room")
	assert_true(ServerFacade.kick_player_from_room(ROOM_CREATOR, A_PLAYER_15), "15 should be kicked from the room")
	assert_eq(player15_session.get_room_id(), -1, "15 should be in room -1 because he was kicked")
	assert_false(ServerFacade.kick_player_from_room(ROOM_CREATOR, A_PLAYER_23), "23 shouldb't be kicked from the room because he isn't in it")
	assert_false(ServerFacade.kick_player_from_room(ROOM_CREATOR, ROOM_CREATOR), "13 shouldn't be kicked from the room because he is the admin")

# tester close_room
func test_close_room():
	var room_id = _room.get_id()
	assert_true(ServerFacade.add_player_to_room("", room_id, "password123", A_PLAYER_15), "15 should be added to the room")
	assert_eq(_room.get_player_number(), 2, "There should be 2 players in this room")
	ServerFacade.close_room(ROOM_CREATOR)
	assert_false(global.game_rooms.has(room_id), "The room shouldn't be there anymore")
	assert_eq(player15_session.get_room_id(), -1, "15 shouldn't be in a room")
	assert_eq(creator_session.get_room_id(), -1, "13 shouldn't be in a room")
	
# test ready_state
func test_change_ready_state():
	pending()
	
# tester start_game
func test_start_game():
	pending()

