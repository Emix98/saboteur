extends 'res://addons/gut/test.gd'

const ROOM_CREATOR = 13
const A_PLAYER_15 = 15

# test if global.connected_session is correctly filled and cleaned
func test_player_connections_disconnections():
	get_tree().emit_signal("network_peer_connected", ROOM_CREATOR)
	get_tree().emit_signal("network_peer_connected", A_PLAYER_15)
	assert_true(global.connected_session.has(ROOM_CREATOR), "Id 13 should be connected")
	assert_true(global.connected_session.has(A_PLAYER_15), "Id 15 should be connected")
	assert_eq(global.connected_session.size(), 2, "2 players should be connected")
	get_tree().emit_signal("network_peer_disconnected", ROOM_CREATOR)
	assert_eq(global.connected_session.size(), 1, "1 players should be connected")
	assert_true(global.connected_session.has(A_PLAYER_15), "Id 15 should still be connected")
	get_tree().emit_signal("network_peer_disconnected", A_PLAYER_15)
	assert_eq(global.connected_session.size(), 0, "No player should be connected")
	
func test_in_room_disconnection_behavior():
	get_tree().emit_signal("network_peer_connected", ROOM_CREATOR)
	ServerFacade.create_room("room name", 3, "password123", ROOM_CREATOR)
	assert_eq(global.connected_session.size(), 1, "There should be one player connected")
	assert_eq(global.game_rooms.size(), 1, "There should be one game")
	
	get_tree().emit_signal("network_peer_disconnected", ROOM_CREATOR)
	assert_eq(global.connected_session.size(), 0, "No player should be connected")
	assert_eq(global.game_rooms.size(), 0, "There should be no game")
