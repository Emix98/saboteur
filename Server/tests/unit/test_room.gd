extends 'res://addons/gut/test.gd'

var Room = load("res://scripts/objects/Room.gd")
var _room
var _room_not_private

func before_each():
	_room = Room.new(15, "room name", 2, "password123", 24)
	_room_not_private = Room.new(165, "not private", 4, "", 267)
	
# test get_id
func test_id():
	assert_eq(_room.get_id(), 15, "Room id should be 15")

# test get_name	
func test_name():
	assert_eq(_room.get_name(), "room name", "Room name should be 'room name'")
	
# test get_max_players
func test_max_players():
	assert_eq(_room.get_max_players(), 2, "Max number of players should be 2")
	
# test get_admin_id
func test_room_admin():
	assert_eq(_room.get_admin_id(), 24, "Admin id of this room should be 24")
	
# test is_private
func test_is_private():
	assert_true(_room.is_private(), "Room 'room' should be private")
	assert_false(_room_not_private.is_private(), "Room 'not private' shouldn't be private")

# test set_to_public / set_to_private
func test_set_to_public_or_private():
	_room.set_to_public()
	assert_false(_room.is_private(), "Room 'room' shouldn't be private")
	_room.set_to_private("password")
	assert_true(_room.is_private(), "Room 'room' should be private")



