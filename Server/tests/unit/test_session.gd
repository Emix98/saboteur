extends 'res://addons/gut/test.gd'

var Session = load("res://scripts/objects/Session.gd")
var _session

func before_each():
	_session = Session.new(15)
	
# test get_id
func test_correct_id():
	assert_eq(_session.get_id(), 15, "Id should be 15")
	
# test get_room_id
func test_room_id():
	assert_eq(_session.get_room_id(), -1, "Session shouldn't be in a room")
	
# test is_signed_up
func test_signed_up():
	assert_false(_session.is_signed_up(), "Session shouldn't be signed up")
	
# test is_ready && change_ready_state
func test_ready():
	assert_false(_session.is_ready(), "Session shouldn't be ready")
	_session.change_ready_state(true)
	assert_true(_session.is_ready(), "Session should be ready")

# test connect_to_account
func test_connect():
	_session.connect_to_account("new name")
	assert_eq(_session.get_name(), "new name", "Name should be 'new name'")
	assert_true(_session.is_signed_up(), "Session should be signed up")
	
	
	
