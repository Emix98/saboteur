# This is where all the rpc calls will be processed
extends Node

func _ready():
	pass
	
remote func create_room (json_encoded_message):
 	# décriptage du message	
	var received_message = parse_and_check(json_encoded_message, ["token", "name", "password", "max_p"])
	if received_message == null:
		return
	
	var room_name = received_message.name
	var room_password = received_message.password
	var max_p = received_message.max_p

	var sender_id = get_tree().get_rpc_sender_id()
	
	ServerFacade.create_room(room_name, max_p, room_password, sender_id)

remote func kick_session (json_encoded_message):
	# décriptage du message
	var received_message = parse_and_check(json_encoded_message, ["token", "player_id"])
	if received_message == null:
		return

	var id_to_kick = int(received_message.player_id)
	var sender_id = get_tree().get_rpc_sender_id()
	
	global.print_to_debug("Player "+ str(sender_id)+" requested to kick "+str(id_to_kick))
	
	ServerFacade.kick_player_from_room(sender_id, id_to_kick)

remote func join_room (json_encoded_message):
	# décriptage du message	
	var received_message = parse_and_check(json_encoded_message, ["token", "id", "name", "password"])
	if received_message == null:
		return

	var room_id = int(received_message.id)
	var room_name = received_message.name
	var room_password = received_message.password

	var sender_id = get_tree().get_rpc_sender_id()
	
	ServerFacade.add_player_to_room(room_name, room_id, room_password, sender_id)

remote func change_ready_state (json_encoded_message):
	# décriptage du message
	var received_message = parse_and_check(json_encoded_message, ["token", "state"])
	if received_message == null:
		return
	
	var state = received_message.state
	
	var sender_id = get_tree().get_rpc_sender_id()
	
	ServerFacade.change_ready_state(sender_id, state)
	
remote func leave_room(json_encoded_message):
	# décriptage du message
	var received_message = parse_and_check(json_encoded_message, ["token"])
	if received_message == null:
		return
		
	ServerFacade.player_left(get_tree().get_rpc_sender_id())

remote func start_game(json_encoded_message):
	# décriptage du message
	var received_message = parse_and_check(json_encoded_message, ["token"])
	if received_message == null:
		return
	
	# vérification de l'état des joueurs
	# envoi à tous les joueurs que la partie commence => appel rpc à start_game coté client ?
	var player_id = get_tree().get_rpc_sender_id()
	
	ServerFacade.launch_game(player_id)

remote func close_room (json_encoded_message):
	# décriptage du message
	var received_message = parse_and_check(json_encoded_message, ["token"])
	if received_message == null:
		return
	
	var sender_id = get_tree().get_rpc_sender_id()
	
	ServerFacade.close_room(sender_id)
	
	
remote func message(json_encoded_message):
	# décriptage du message
	var received_message = parse_and_check(json_encoded_message, ["token", "message"])
	if received_message == null:
		return
	
	var sender_id = get_tree().get_rpc_sender_id()
	var message = received_message.message
	
	ServerFacade.player_message(sender_id, message)
	
# fonction provisoire / début authentification => permet de changer son pseudo
remote func name_change(name):
	print("new name = " + name)
	var sender_id = get_tree().get_rpc_sender_id()
	
	if name != "":
		global.connected_session[sender_id].set_name(name)
		rpc_node.rpc_id(sender_id, "player_info", name, sender_id)


############## FUNCTIONS USED IN-GAME

remote func place_card_on_grid(json_encoded_message):
	# décriptage du message
	var received_message = parse_and_check(json_encoded_message, ["token", "cell", "card_position", "rotation"])
	if received_message == null:
		return
	
	var cell = received_message.cell
	var card_position = int(received_message.card_position)
	var rotation = int(received_message.rotation)
	
	var sender_id = get_tree().get_rpc_sender_id()
	
	ServerFacade.place_card_on_grid(sender_id, cell, card_position, rotation)
	
remote func place_card_on_player(json_encoded_message):
	# décriptage du message
	var received_message = parse_and_check(json_encoded_message, ["token", "player_id", "card_position"])
	if received_message == null:
		return
	
	print("place_card_on_player")
	
	var player_id = int(received_message.player_id)
	var card_position = int(received_message.card_position)
	
	var sender_id = get_tree().get_rpc_sender_id()
	
	ServerFacade.place_card_on_player(sender_id, player_id, card_position)
	
remote func discard_card(json_encoded_message):
	# décriptage du message
	var received_message = parse_and_check(json_encoded_message, ["token", "card_position"])
	if received_message == null:
		return
	
	print("discard")
	
	var card_position = int(received_message.card_position)
	
	var sender_id = get_tree().get_rpc_sender_id()
	
	ServerFacade.discard_card(sender_id, card_position)
	
remote func end_turn(json_encoded_message):
	# décriptage du message
	var received_message = parse_and_check(json_encoded_message, ["token"])
	if received_message == null:
		return
		
	var sender_id = get_tree().get_rpc_sender_id()
	
	ServerFacade.player_end_turn(sender_id)

remote func continue_game(json_encoded_message):
	# décriptage du message
	print("continue game reçu")
	var received_message = parse_and_check(json_encoded_message, ["token"])
	if received_message == null:
		return
	
	var sender_id = get_tree().get_rpc_sender_id()
	
	ServerFacade.continue_game(sender_id)


###################### UTILITY
# used when receiving a message from the client to check if it's the correct message
func parse_and_check(json_encoded_message, fields):
	var received_message =  JSON.parse ( json_encoded_message ) 
	
	if typeof(received_message.result) != TYPE_DICTIONARY:
		return null
		
	# checking if all the expected fields are in the message
	if !received_message.result.has_all(fields):
		return null
	
	return received_message.result
	
	
sync func server_message(message):
	global.print_to_debug("Server : " + message)
	
sync func user_message(message):
	global.print_to_debug(global.connected_session[get_tree().get_rpc_sender_id()]._name + ": " + message)
	
	
	
	