#include "controler.h"
#include "./lib/libsrc/headers/libsaboteur.h"

bool TTS::say_text(String txt) {

    //convert Godot String to Godot CharString to C string
    return festival_say_text(txt.ascii().get_data());
}

void TTS::_bind_methods() {

    ClassDB::bind_method(D_METHOD("say_text", "txt"), &TTS::say_text);
}

TTS::TTS() {
    festival_initialize(true, 210000); //not the best way to do it as this should only ever be called once.
}