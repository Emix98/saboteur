#include "Cell.h"
#include "Path.h"
#include <iostream>

// void Cell::_bind_methods()
// {
//     ClassDB::bind_method(D_METHOD("set_gold"), &Cell::set_gold);
//     ClassDB::bind_method(D_METHOD("is_gold"), &Cell::is_gold);
//     ClassDB::bind_method(D_METHOD("is_empty"), &Cell::is_empty);
//     ClassDB::bind_method(D_METHOD("set_empty"), &Cell::set_empty);
//     ClassDB::bind_method(D_METHOD("is_indestructible"), &Cell::is_indestructible);
//     ClassDB::bind_method(D_METHOD("set_indestructible"), &Cell::set_indestructible);
//     ClassDB::bind_method(D_METHOD("reset_indestructible"), &Cell::reset_indestructible);
//     ClassDB::bind_method(D_METHOD("is_blocked"), &Cell::is_blocked);
//     ClassDB::bind_method(D_METHOD("set_blocked"), &Cell::set_blocked);
//     ClassDB::bind_method(D_METHOD("set_blocked", "val"), &Cell::set_blocked);
//     ClassDB::bind_method(D_METHOD("reset_blocked"), &Cell::reset_blocked);
//     ClassDB::bind_method(D_METHOD("set_color", "color"), &Cell::set_color);
//     ClassDB::bind_method(D_METHOD("get_color"), &Cell::get_color);
//     ClassDB::bind_method(D_METHOD("reset_color"), &Cell::reset_color);
//     ClassDB::bind_method(D_METHOD("set_path", "n", "e", "s", "w", "b"), &Cell::set_path);
//     ClassDB::bind_method(D_METHOD("set_path","card"), &Cell::set_path;
//     ClassDB::bind_method(D_METHOD("get_path","i"), &Cell::get_path);
//     ClassDB::bind_method(D_METHOD("reset_path"), &Cell::reset_path);
//     ClassDB::bind_method(D_METHOD("get_type"), &Cell::get_type);
//     ClassDB::bind_method(D_METHOD("set_rock"), &Cell::set_rock);
//     ClassDB::bind_method(D_METHOD("reset_all"), &Cell::reset_all);
//     ClassDB::bind_method(D_METHOD("set_obstacle"), &Cell::set_obstacle);
//     ClassDB::bind_method(D_METHOD("set_start"), &Cell::set_start);
//     ClassDB::bind_method(D_METHOD("clear_cells"), &Cell::clear_cells);
//     ClassDB::bind_method(D_METHOD("set_marked","b"), &Cell::set_marked);
//     ClassDB::bind_method(D_METHOD("get_marked"), &Cell::get_marked);
// }

//Constructeur
Cell::Cell()
{
    this->blocked = false;
    this->indestructible = false;
    for (int i = 0; i < 4; i++)
    {
        this->path[i] = false;
    }
    this->color = -1;
    this->type = 'V';
}

Cell::~Cell()
{
}

void Cell::set_indestructible(bool val)
{
    indestructible = val;
}

void Cell::set_blocked(bool val)
{
    blocked = val;
}

char Cell::get_type()
{
    return this->type;
}

void Cell::set_empty()
{
    type = 'V';
    reset_all();
}

void Cell::set_path(bool n, bool e, bool s, bool w, bool b)
{
    this->path[0] = n;
    this->path[1] = e;
    this->path[2] = s;
    this->path[3] = w;
    this->blocked = b;
    this->indestructible = false;
    this->type = 'P';
    //std::cout << "set_path : " << this->path[0]<<this->path[1]<<this->path[2]<<this->path[3]<<this->blocked<<std::endl;
}

void Cell::set_path(Card *card)
{
    Path *path = static_cast<Path *>(card);
    //std::cout << "set_path : " << path->get_path(0)<<path->get_path(1)<<path->get_path(2)<<path->get_path(3)<<path->is_blocked()<<std::endl;
    set_path(path->get_path(0), path->get_path(1), path->get_path(2), path->get_path(3), path->is_blocked());
}

void Cell::set_rock()
{
    this->type = 'R';
    this->indestructible = true;
    this->blocked = true;
}

void Cell::set_obstacle()
{
    this->type = 'O';
    this->indestructible = false;
    this->blocked = true;
}

void Cell::set_gold()
{
    this->type = 'G';
    this->indestructible = true;
    this->blocked = true;
}

void Cell::set_start()
{
    this->type = 'S';
    this->indestructible = true;
    this->blocked = false;
    this->path[0] = true;
    this->path[1] = true;
    this->path[2] = true;
    this->path[3] = true;
}

//-------------

bool Cell::is_gold()
{
    return this->type == 'G';
}

bool Cell::is_empty()
{
    return this->type == 'V';
}

bool Cell::is_indestructible()
{
    return indestructible;
}

void Cell::set_indestructible()
{
    indestructible = true;
}

void Cell::reset_indestructible()
{
    indestructible = false;
}

bool Cell::is_blocked()
{
    return blocked;
}

void Cell::set_blocked()
{
    if (blocked == false)
    {
        blocked = true;
    }
    else
    {
        blocked = false;
    }
}

void Cell::reset_blocked()
{
    blocked = false;
}

void Cell::set_color(int color)
{
    this->color = color;
}

int Cell::get_color()
{
    return this->color;
}

void Cell::reset_color()
{
    this->color = -1;
}

bool Cell::get_path(int i)
{
    return this->path[i];
}

void Cell::reset_path()
{
    for (int i = 0; i < 4; i++)
    {
        path[i] = false;
    }
}

void Cell::reset_all()
{
    reset_path();
    reset_blocked();
    reset_indestructible();
    reset_color();
    this->type = 'V';
}

void Cell::set_marked(bool b)
{
    this->marked = b;
}

bool Cell::get_marked()
{
    return this->marked;
}
