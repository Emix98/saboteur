#include "Action_on_player.h"

// void Action_on_player::_bind_methods()
// {
//   ClassDB::bind_method(D_METHOD("is_positive"), &Action_on_player::is_positive);
//   ClassDB::bind_method(D_METHOD("get_handicap_item", "accessory"), &Action_on_player::get_handicap_item);
//   ClassDB::bind_method(D_METHOD("assign_card", "item", "heal"), &Action_on_player::assign_card);
//   ClassDB::bind_method(D_METHOD("apply", "target"), &Action_on_player::apply);
// }

Action_on_player::Action_on_player(void)
{
  this->action[0] = false;
  this->action[1] = false;
  this->action[2] = false;
  this->positive = false;
  this->object_type = 'J';
}

Action_on_player::Action_on_player(char type, bool positive)
{
  //L -> lanterne, P -> pioche, C -> chariot
  if (type == 'P')
  {
    this->action[0] = true;
  }
  else if (type == 'L')
  {
    this->action[1] = true;
  }
  else if (type == 'C')
  {
    this->action[2] = true;
  }
  this->positive = positive;
  this->object_type = 'J';
}

Action_on_player::~Action_on_player() {}

bool Action_on_player::get_handicap_item(int accessory)
{
  if (accessory >= 0 && accessory < 3)
  { //Car trois types d'action, changer si on en met plus
    return this->action[accessory];
  }
  else
    return false;
}

bool Action_on_player::is_positive()
{
  return this->positive;
}

void Action_on_player::assign_card(char item, bool heal)
{
  //Chariot,Lanterne,Pioche fix ou non
  if (item == 'c' || item == 'C')
  {
    this->action[0] = true;
    //std::cout << "Application handicap chariot" << std::endl;
  }

  if (item == 'l' || item == 'L')
  {
    this->action[1] = true;
    //std::cout << "Application handicap lanterne" << std::endl;
  }

  if (item == 'p' || item == 'P')
  {
    this->action[2] = true;
    //std::cout << "Application handicap pioche" << std::endl;
  }

  this->positive = heal;
}

void Action_on_player::apply(Player *target)
{
  for (int i = 0; i < 3; i++)
  { //Toujours car trois types d'action
    //std::cout << action[i] << std::endl;
    if (action[i])
    {
      target->set_handicap(i, !positive);
      //std::cout << "apply() : Handicap appliqué !" << std::endl;
    }
  }
}
