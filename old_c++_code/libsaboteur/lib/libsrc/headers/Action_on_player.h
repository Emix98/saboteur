#ifndef ACTIONONPLAYER_H
#define ACTIONONPLAYER_H

#include "Action.h"
#include "Player.h"
// #include "core/reference.h"

class Player;

class Action_on_player : public Action//, public Reference
{
public:

	// GDCLASS(Action_on_player, Reference);

	//Constructeur
	Action_on_player(void);
	Action_on_player(char,bool);
	void assign_card(char item,bool heal); //permet d'attribuer les effets d'une carte : Chariot c, Lanterne l, Pioche p
	bool get_handicap_item(int accessory);
	bool is_positive(void);
	void apply(Player*); //Applique l'effet de la carte à un joueur cible

	//Destructeur
	~Action_on_player();

protected:
	static void _bind_methods();

private:

	bool action[3]; //tableau définissant le type d'objet : 0 -> pioche, 1 -> lanterne, 2 -> chariot
	bool positive; //déretmine si c'est un bonus (true) ou un malus (false)
};

#endif
