#ifndef ACTIONONGRID_H
#define ACTIONONGRID_H

#include "Action.h"
#include "Grid.h"
// #include "core/reference.h"

class Action_on_grid : public Action //, public Reference
{
public:
	// GDCLASS(Action_on_grid, Reference);
	//Méthodes qui ne concerne que les cartes ayant une action sur le terrain

	//Constructeur
	Action_on_grid(bool reveal, bool remove, bool swap);
	~Action_on_grid();
	// R Reveal
	// M  reMove
	// S Swap
	void assign_card(char c); //pour affecter une carte Action_on_grid
	bool is_reveal();
	bool is_remove();
	bool is_swap();

	/**Applique l'effet de la carte à une case*/
	bool apply(Grid *, int, int);

protected:
	static void _bind_methods();

private:
	bool reveal, remove, swap;
};

#endif
