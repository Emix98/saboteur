#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>

#include "Card.h"
#include "Action.h"
#include "Action_on_player.h"
#include "Action_on_grid.h"
#include <mysql/mysql.h>

// #include "core/reference.h"
//#include "Game.h"

class Action_on_player;

/**
  * class Player
  *
  */
class Player //: public Reference
{
public:
  // GDCLASS(Player, Reference);

  //CONSTRUCTEUR/DESTRUCTEUR

  Player(int, bool, std::string, bool[3], int, int, int);
  Player(std::string, int);
  virtual ~Player();

  //METHODES

  /**
    * Définit si le joueur est un saboteur ou pas
    * @param saboteur Vrai si le joueur est un saboteur
    */
  void set_saboteur(bool);

  /**
    * Tire une carte de la pioche passée en paramètre
    * @param draw Pioche de cartes
    * @param mode Vrai = pioche la première carte, faux = pioche une carte aléatoire
    */
  void draw_card(std::vector<Card *> *, bool);

  /**
    * Tire une nouvelle main de taille size dans la pioche passée en paramètre
    * @param draw Pioche de cartes
    * @param size Taille de la main
    */
  void draw_hand(std::vector<Card *> *, int);

  /**Sélectionne une carte*/
  void select_card(Card); //Non présente dans le .cpp ?

  /**
    * Joue une carte chemin et en repioche une si le joueur a pu jouer
    * @param hand_position Position de la carte dans la main
    * @param grid Grille du jeu
    * @param line Ligne où poser la carte
    * @param column Colonne où poser la carte
    * @param draw Pioche où piocher une nouvelle carte si la carte a pu être jouée
    * \returns Vrai si la carte a été jouée, faux sinon
    */
  bool use_path_card(int, Grid *, int, int, std::vector<Card *> *);

  /**
    * Joue une carte action sur un joueur (si c'est bien le bon type de carte)
    * et repioche une carte si le joueur a pu jouer
    * @param hand_position Position de la carte dans la main
    * @param target Joueur ciblé par la carte
    * @param draw Pioche où piocher une nouvelle carte si la carte a pu être jouée
    * \returns Vrai si la carte a été jouée, faux sinon
    */
  bool use_action_on_player_card(int, Player *, std::vector<Card *> *);

  /**
    * Joue une carte action sur le plateau aux coordonnées indiquées
    * (si c'est bien le bon type de carte) et repioche une carte si
    * le joueur a pu jouer
    * @param hand_position Position de la carte dans la main
    * @param grid Grille du jeu
    * @param line Ligne où poser la carte
    * @param column Colonne où poser la carte
    * @param draw Pioche où piocher une nouvelle carte si la carte a pu être jouée
    * \returns Vrai si la carte a été jouée, faux sinon
    */
  bool use_action_on_grid_card(int, Grid *, int, int, std::vector<Card *> *);

  /**
    * Défausse une carte
    * @param index Index de la carte dans la main
    */
  void discard(int);

  /**
    * Ajoute une certaine quantité d'or au joueur
    * @param gold Or à donner au joueur (peut techniquement valoir moins que zéro)
    */
  void give_gold(int);

  /**Retourne la position du joueur*/
  int get_position() const;

  /**Retourne si le joueur est un saboteur*/
  bool get_saboteur() const;

  /**Retourne le nom du joueur*/
  std::string get_name() const;

  /**
    * Retourne un handicap du joueur (passé en paramètre)
    * @param i Handicap à retourner
    */
  bool get_handicap(int) const;

  /**Retourne la main du joueur*/
  std::vector<Card *> *get_hand() const;

  /**
    * Retourne une carte du joueur
    * @param position Index de la carte à retourner
    */
  Card *get_card(int) const;

  /**Retourne la quantité d'or détenue par le joueur*/
  int get_gold() const;

  /**Retourne l'identifiant du joueur*/
  int get_player_id() const;

  /**
  * Modifie l'identifiant de l'équipe du joueur
  * @param tm_id l'identifiant de l'équipe
  */
  void set_team_id(int);

  /**Retourne l'identifiant de l'équipe du joueur*/
  int get_team_id() const;

  /**Retourne l'identifiant de l'utilisateur du joueur*/
  int get_user_id() const;

  /**Retourne si le joueur a gagné ou non*/
  bool get_winner() const;


  /**
    * Définit l'identifiant de l'utilisateur du joueur
    * @param ur_id l'identifiant de l'utilisateur
  */
  void set_user_id(int);

  /**
    * Définit l'identifiant du du joueur
    * @param p_id l'identifiant du joueur
  */
  void set_player_id(int);

  /**
    * Définit si le joueur a gagné ou non
    * @param winner vrai si le joueur a gagné, faux sinon
  */
  void set_winner(bool);

  /**
    * Ajoute/supprime un handicap au joueur
    * @param handi Handicap à modifier
    * @param apply Vrai pour appliquer le handicap, faux pour l'enlever
    */
  void set_handicap(int, bool);

  /**
    * Définit la position du joueur
    * @param pos Position du joueur
    */
  void set_position(int);

protected:

  /**Assure le lien entre GODOT et cette classe*/
	static void _bind_methods();

private:
  /**Position dans la partie*/
  int position;
  /**Indique si le joueur est un saboteur*/
  bool is_saboteur;
  /**Nom du joueur*/
  std::string name;
  /**Handicaps actuels du joueur*/
  bool handicap[3];
  /**Main du joueur*/
  std::vector<Card *> *hand;
  /**Or possédé*/
  int total_gold;
  /**Identifiant du joueur*/
  int player_id;
  /**Identifiant de l'équipe du joueur*/
  int team_id;
  /**Instance de l'accés à la BDD MYSQL*/
  int user_id;
  /**définit si c'est l'équipe gagnante*/
  bool is_winner;
};

std::ostream &operator<<(std::ostream &stream, Player const &player);

#endif // PLAYER_H
