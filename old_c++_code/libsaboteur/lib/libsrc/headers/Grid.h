#ifndef GRID_H
#define GRID_H

#include <iostream>
#include <vector>
#include "Cell.h"
#include "Card.h"
#include "Path.h"
// #include "core/reference.h"

class Grid //: public Reference
{
  public:
    // GDCLASS(Grid, Reference);
    //constructeurs
    Grid();
    Grid(int, int);
    Grid(Cell ***, int, int);
    virtual ~Grid();                                        //destructeur
    void set_width(int);                                    //modifie la largeur de la grille
    void set_height(int);                                   //modifie la hauteur de la grille
    int get_width();                                        //retourne la largeur de la grille
    int get_height();                                       //retourne la hauteur de la grille
    bool add_path(Card *, int, int);                        //ajoute une carte sur la grille, renvoye faux si l'action a échouée
    void reset_cell(int, int);                              //supprimme une carte sur la grille
    Cell *get_cell(int, int);                               //retourne la cellule
    void set_starting_point(int, int, int);                 //Définit les cases de départ
    void set_arrival_points(int, int, int, int);            //Définit les cases d'arrivée
    bool try_placement(Path *, int, int);                   //test si la carte peut être posée
    int get_gold_position();                                //révèle la position de la pépite d'or
    void void_cells();                                      //nettoye toutes les célulles
    int *get_starting_point();                              //retourne les coordonnées des points de départ
    int **get_ending_points();                              //retourne les coordonnées des points d'arrivée
    bool path_finding();                                    //recherche d'un chemin vers une pépite d'or
    bool path_finding_aux(std::vector<Cell *> *, int, int); //fonction auxiliaire de la recherche de chemin

  protected:
    static void _bind_methods();

  private:
    int height, width;   //hauter et largeur de la  grille
    Cell ***cells;       //cellules contenant les cartes
    int gold_position;   //position de l'or en partant de la gauche, à compter de 1
    int *starting_point; //position des points de départ
    int **ending_points; // position des points d'arrivée
};

#endif
