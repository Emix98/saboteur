#ifndef CELL_H
#define CELL_H

#include <iostream>
#include "Card.h"
// #include "core/reference.h"

class Cell  //public Reference
{
  public:
    // GDCLASS(Cell, Reference);

    //Constructeur
    Cell();
    virtual ~Cell();

    //METHODES

    /**
      * Définit cette case comme la pépite d'or
      */
    void set_gold();

    /**
      * Renvoie si cette case est la pépite d'or
      * \returns Vrai si la case est la pépite d'or, faux sinon
      */
    bool is_gold();

    /**
      * Alterne entre vide et non vide, vrai par défaut
      */
    void set_empty();

    /**
      * Renvoie si la case est vide ou non
      * \returns Vrai si la case est vide, faux sinon
      */
    bool is_empty();

    /**
      * Renvoie si la case est indestructible, faux par défaut
      * \returns Vrai si la case est indestructible, faux sinon
      */
    bool is_indestructible();

    /**
      * Définit cette case comme étant indestructible
      */
    void set_indestructible();

    /**
      * Définit cette case comme étant indestructible ou destructible
      * @param val Vrai pour définir case comme étant indestructible, faux sinon
      * \deprecated utiliser set_indestructible()
      */
    void set_indestructible(bool);

    /**
      * Définit la case comme étant destructible (valeur par défaut)
      */
    void reset_indestructible();

    /**
      * Retourne si la case est bloquante ou non
      * \returns Vrai si la case est bloquante, faux sinon
      */
    bool is_blocked();

    /**
      * Alterne entre bloquante et non bloquante, faux par défaut
      */
    void set_blocked();

    /**
      * Définit cette case comme étant bloquante ou non bloquante
      * @param val Vrai pour définir case comme étant bloquante, faux sinon
      */
    void set_blocked(bool);

    void reset_blocked();

    void set_color(int color);                   //Définir la couleur de la case
    int get_color();                             //Renvoyer la couleur de la case
    void reset_color();                          //Réinitialiser la couleur de la case
    void set_path(bool, bool, bool, bool, bool); //Définir la position i du tableau
    void set_path(Card *);                       //Définir la position i du tableau
    bool get_path(int);                          //Renvoyer la position i du tableau
    void reset_path();                           //Reset le tableau

    char get_type();       //Retourne le type de la cellule
    void set_rock();       //Définir cette case comme un rocher
    void reset_all();      //Réinitialise toutes les variables de la céllule
    void set_obstacle();   //Définir cette case comme un obstacle
    void set_start();      //Définir cette case comme un départ
    void clear_cells();    //Vide toutes les cases
    void set_marked(bool); //définit le marquage
    bool get_marked();     //retourne le marquage

  protected:
    static void _bind_methods();

  private:
    bool path[4];
    bool blocked;
    bool indestructible;
    int color;
    char type; //R -> rocher, S -> départ, G -> or, P -> chemin, V -> vide, O -> obstacle
    bool marked;
};

#endif
