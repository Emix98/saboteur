#ifndef CARD_H
#define CARD_H

#include <string>
#include <vector>
#include <iostream>
#include <string>
// #include "core/reference.h"

class Action_on_player ;
class Action_on_grid ;
class Path ;
class Action ;

class Card {
// public Reference {
  // GDCLASS(Card, Reference);

public:
  Card();
  ~Card();
  char get_object_type();

protected:
  char object_type;
  static void _bind_methods();

private:
};

#endif // CARD_H
