#ifndef TEAM_H
#define TEAM_H

#include "Player.h"
// #include "core/reference.h"
#include <vector>

class Team //: public Reference
{

	public:
	  // GDCLASS(Team, Reference);

		//contructeurs / destructeur
		Team(std::string);
		Team(int, int, int, std::string);
		virtual ~Team();

		std::vector<Player*>* get_players(); //retourne les joueurs
		Player* get_player(int); //retourne un joueur
		void set_players(std::vector<Player*>*); //modifie le vecteur de joueurs
		void add_player(Player*); //ajoute un joueur à l'équipe
		void delete_player(Player*); //retire un joueur de l'équipe
		void delete_player(int); //retire un joueur de l'équipe à la position n
		std::string get_name(); //retourne le nom de l'équipe
		void set_name(std::string); //modifie le nom de l'équipe
		void set_team_id(int); //modifie l'id de la team
		int get_team_id(); //Retourne l'identifiant de la team

  protected:
    static void _bind_methods();

	private:
		int team_id; //identifiant de l'équipe
		int total_gold; //totalité des pépites d'or
		int game_id; //identifiant du jeu
		std::string name; //nom de l'équipe
		std::vector<Player*>* players; //joueurs de l'équipe
		// bool is_winner; //définit si c'est l'équipe gagnante
};

#endif
