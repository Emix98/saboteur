#ifndef INCLUDE_H
#define INCLUDE_H

#include "Card.h"
#include "Path.h"
#include "Action.h"
#include "Cell.h"
#include "Grid.h"
#include "Action_on_grid.h"
#include "Player.h"
#include "Team.h"
#include "Action_on_player.h"
#include "Game.h"

#endif