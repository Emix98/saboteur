#ifndef PATH_H
#define PATH_H
#include "Card.h"
// #include "core/reference.h"

#include <string>
#include <vector>

class Path : public Card//, public Reference
{
  public:
    // GDCLASS(Path, Reference);

    Path(bool Nord, bool Est, bool Sud, bool Ouest, bool is_blocked);
    ~Path();

    /**Indique si le chemin est ouvert dans la direction indiquée
     * 0 = Nord, 1 = Est, 2 = Sud, 3 = Ouest
     * */
    bool get_path(int direction);
   	/**
	  *Indique si le chemin est bloqué ou non
	  * \returns Vrai si il est bloqué, faux sinon
	  */
    bool is_blocked();
    /**Réalise la rotation d'une carte*/
    void rotate(char);

  protected:
    static void _bind_methods();

  private:
    bool path[4]; //Directions de la carte
    bool blocked; //Indique si le chemin est bloqué

};

#endif // PATH_H
