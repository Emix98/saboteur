#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <vector>
#include "Player.h"
#include "Team.h"
#include "Grid.h"
#include <mysql/mysql.h>

class Game
{
   public:
	// GDCLASS(Game, Reference);
	//Constructeur
	Game();
	//Destructeur
	virtual ~Game();

  /**Passe à la manche suivante*/
	void next_round();

	/**Exécute toute une manche*/
	void init_round();

	/**Crée deux équipes, une de saboteurs et une de chercheurs*/
	void init_2_teams();

	/**Détruit les équipes*/
	void delete_teams();

	/**
    *Distribue les cartes pour chaque joueur
    *@param mode : true -> tirage dans la pioche aléatoire, false -> tirage en tête de liste
    */
	void give_hands(bool);

	/**Distribue les pépites d'or*/
	void give_gold();

	//void draw(); //piocher une carte.

	/**Défausse une carte*/
	void discard();

	/**Utilise une carte*/
	void use_card();

	/**Affiche les scores et les statistiques de fin de partie*/
	void end_game();

	/**Affiche les scores de fin de manche et réinitialise les variables*/
	void end_round();

	/**
	  * Teste si les mains de tous les joueurs sont vides
	  * \returns Vrai si les mains de tous les joueurs sont vides, faux sinon
	  */
	bool is_empty_hands();

	/**
	  * Indique si c'est le tour d'un joueur
	  * @param player Joueur dont on veut savoir si c'est le tour
	  * \return Vrai si c'est le tour du joueur, faux sinon
	  */
	bool is_turn(Player *);

	/**Change le joueur dont c'est le tour (passe au joueur suivant)*/
	void next_player();

	/**
	  * Initialise la pioche de jeu
	  */
	void init_draw();

  /**
	  * Initialise la pioche de jeu
	  * @param mode Vrai = pioche de test, faux = lecture dans la base de données
	  */
	bool init_draw_sql();

	/**Vide la pioche*/
	void void_draw();		   //Sérieux les gens, vider c'est clear, pas void

	/**
	  * Ajoute un joueur à la partie et à la BDD
	  * @param p Joueur à ajouter
    * @param ur_id identifiant du joueur
	  */
	void add_player(Player *, int);

  /**
    * Ajoute un joueur à la partie
    * @param p Joueur à ajouter
    */
  void add_player(Player *);

	/**
	  * Ajoute une équipe à la partie
	  * @param t Equipe à ajouter
	  */
	void add_team(Team *);

	/**Distribue les rôles aux joueurs*/
	void give_roles();

	/**
	  * Indique s'il y a un chemin jusqu'à la pépite
	  * \returns Vrai s'il existe un chemin jusqu'à la pépite, faux sinon
	  * \deprecated Ne fonctionne pas, utiliser l'équivalent dans Grid à la place
	  */
	bool way_to_nugget();

	/**
	  * Indique s'il existe un chemin jusqu'au départ depuis la cellule spécifiée
	  * @param current_cell Cellule à tester
	  * \returns Vrai s'il existe un chemin jusqu'au départ, faux sinon
	  * \deprecated Ne fonctionne pas, utiliser l'équivalent dans Grid à la place
	  */
	bool way_to_start(Cell *);

	/**
	  * Retourne l'ID du jeu
	  * \returns Identifiant de la partie
	  */
	int get_game_id();

	/**
	  * Retourne le nombre de joueurs
	  * \returns Nombre de joueurs dans la partie
	  */
	int get_nb_players();

	/**
	  * Retourne la taille maximale d'une main
	  * \returns Taille maximale d'une main
	  */
	int get_hand_length();

	/**
	  * Retourne la position du joueur dont c'est le tour
	  * \returns Position du joueur dont c'est le tour
	  */
	int get_position_token();

	/**
	  * Indique si on est dans un moment où il est possible de jouer
	  * \returns Vrai si on peut jouer, faux sinon
	  */
	bool get_play_time();

	/**
	  * Indique le numéro de la manche actuelle
	  * \returns Numéro de la manche actuelle
	  */
	int get_round();

	/**
	  * Indique si une manche a commencé
	  * \returns Vrai si une manche a commencé, faux sinon
	  */
	bool get_round_started();

	/**
	  * Indique si l'or a été trouvé
	  * \returns Vrai si l'or a été trouvé, faux sinon
	  */
	bool get_gold_found();

	/**
	  * Retourne la grille du jeu
	  * \returns Grille de la partie actuelle
	  */
	Grid *get_grid();

	/**
	  * Retourne un joueur spécifique
	  * @param position Position du joueur que l'on veut récupérer
	  * \returns Joueur se situant à la position donnée
	  */
	Player *get_player(int);

  /**
    * Retourne les différents équipes en jeu
    * \returns Equipes en jeu
    */
	std::vector<Team *> *get_teams();

	/**
	  * Retourne tous les joueurs en jeu
	  * \returns Joueurs dans la partie
	  */
	std::vector<Player *> *get_players();

	/**
	  * Retourne la pioche
	  * \returns Pioche
	  */
	std::vector<Card *> *get_draw();

  /**
	  * Ajoute un utilisateur au jeu
    * @param login le pseudo de l'utilisateur
    * \returns faux si l'utilisateur n'existe pas
	  */
	bool add_user(std::string);

  /**
	  * Ajoute un utilisateur au jeu
    * @param usr_id l'id de l'utilisateur
    * \returns faux si l'utilisateur n'existe pas
	  */
	bool add_user(int);

  /**
	  * Retourne l'identifiant de l'utilisateur s'il existe
    * @param login le pseudo de l'utilisateur
	  * \returns user_id
	  */
	int get_user_id(std::string);

  /**
    * Enregistre un nouvel utilisateur dans la base de données
    * @param logn le pseudo de l'utilisateur
    * @param passwd le mot de passe de l'utilisateur
    * @param first_nm le prénom de l'utilisateur
    * @param nm le nom de l'utilisateur
    * \returns retourne des codes d'erreur si les arguments ne sont pas respectés
    */
  int create_user(std::string,std::string,std::string,std::string);

	/**
	  * Modifie l'identifiant de la partie
	  * @param g_id Nouvel identifiant de partie
	  */
	void set_game_id(int);

	/**
	  * Définit la taille maximale des mains en fonction du nombre de joueurs
	  */
	void set_hand_length();

	/**
	  * Modifie le joueur dont c'est le tour
	  * @param p_token Nouvelle position du token
	  */
	void set_position_token(int);

	/**
	  * Définit si on peut jouer
	  * @param p_time Si oui ou non c'est le moment de jouer
	  */
	void set_play_time(bool);

	/**
	  * Modifie la manche en cours
	  * @param r Numéro de la nouvelle manche
	  */
	void set_round(int);

	/**
	  * Définit si une manche a démarré
	  * @param r_started Si oui ou non la manche a démarré
	  */
	void set_round_started(bool);

	/**
	  * Change la grille de jeu
	  * @param gd Nouvelle grille de jeu
	  */
	void set_grid(Grid *);

  /**
    * Modifie la liste d'équipes
    * @param tms Nouvelle liste d'équipes
    */
	void set_teams(std::vector<Team *> *);

  /**
    * Modifie la pioche
    * @param drw Nouvelle pioche
    */
	void set_draw(std::vector<Card *> *);

  /**
    * Modifie la liste des joueurs
    * @param plrs Nouvelle liste de joueurs
    */
	void set_players(std::vector<Player *> *);

  /**
    * Modifie l'état de découverte de la pépite (trouvée ou non)
    * @param gf Vrai si on veut que la pépite soit trouvée, faux sinon
    */
	void set_gold_found(bool);

  /**
    * Teste si la partie est finie
    * \returns Vrai si la partie est finie (pépite trouvée ou mains vides), faux sinon
    */
	bool is_over();

  /**
    * Définit le point de départ
    * @param y_line Colonne du point de départ
    * @param x_center Ligne du point de départ
    * @param space Inutile
    */
	void set_starting_point(int, int, int);

  /**
    * Définit les points d'arrivée
    * @param y_line Colonne des cases d'arrivée
    * @param x_center Ligne de la case d'arrivée centrale
    * @param space Nombre de cases entre chaque case d'arrivée
    * @param position 0 pour une position aléatoire, 1, 2 ou 3 pour la définir manuellement
    */
	void set_arrival_points(int, int, int, int); //Définit les cases d'arrivée

  /**Définit le point de départ par défaut*/
	void set_starting_point();

  /**Définit les points d'arrivée par défaut*/
	void set_arrival_points();

  /**Génère un id de jeu en fonction des id disponibles*/
  int generate_game_id();

  /**Génère un id de team en fonction des id disponibles*/
  int generate_team_id();

  /**Génère un id d'utilisateur en fonction des id disponibles*/
  int generate_user_id();

  /**Génère un id de statistiques en fonction des id disponibles*/
  int generate_stat_id();

  /**Mets à jours les données dans la table game de la base de données*/
  bool save_game_info();

  /**
    * Teste si l'utilisateur existe dans la BDD
    * @param id_user l'identifiant de l'utilisateur
    * \returns Vrai l'utilisateur existe, faux sinon
    */
  bool user_exists(int);

  /**
  * Teste si l'utilisateur existe dans la BDD
  * @param login le pseudo de l'utilisateur
  * \returns Vrai l'utilisateur existe, faux sinon
  */
  bool user_exists(std::string);

  /**
    * Teste si le joueur existe dans la BDD
    * @param id_pl l'identifiant du joueur
    * \returns Vrai le joueur existe, faux sinon
    */
  bool player_exists(int);

  /**
  * Enregistre les statistiques de chaque joueur dans la bdd en fin de manche
  * \returns vrai si succée, faux si échec
  */
  bool save_stats_end_round();

  /**
  * Enregistre les statistiques de chaque joueur dans la bdd en fin de jeu
  * \returns vrai si succée, faux si échec
  */
  bool save_stats_end_game();

  /**
  * Enregistre l'or de chaque joueur dans la BDD
  * \returns vrai si succée, faux si échec
  */
  bool save_gold();

  /**
  * Enregistre la position actuelle en manche dans la BDD
  * \returns vrai si succée, faux si échec
  */
  bool save_round();

  /**
  * Enregistre la position du jeu token_position dans la BDD
  * \returns vrai si succée, faux si échec
  */
  bool save_position_token();

  /**
  * Enregistre les dimension de la grille
  * \returns vrai si succée, faux si échec
  */
  bool save_dimensions();

  /**
  * Définit le joueur gagnant, joueur avec le plus d'or de la partie
  */
  void set_winner();

  /**
  * Met à jour les informations d'un joueur
  * @param Player* le joueur
  * \returns vrai si succée, faux si échec
  */
  bool save_player(Player*);

  /**
  * Crée une définition de statistiques pour un utilisateur dans la BDD
  * @param usr_id l'identifiant de l'utilisateur
  * \returns -1 si erreur, id_stats sinon
  */
  int create_stat(int);

  /**
  * Crée équipe dans la BDD
  * @param Team* l'équipe
  * \returns -1 si erreur, id_team sinon
  */
  int create_team(Team*);

  /**
  * Crée le joueur dans la BDD
  * @param Player* le joueur
  * \returns -1 si erreur, id_player sinon
  */
  int create_player(Player*);

  /**
  * Crée une définition de jeu dans la BDD
    * \returns -1 si erreur, id_game sinon
  */
  int create_game();

  /**Génère un id de player en fonction des id disponibles*/
  int generate_player_id();


  protected:
  /**Assure le lien entre GODOT et cette classe*/
	static void _bind_methods();

  private:
  /**Numéro d'identifiant de la partie*/
	int game_id;
  /**Numéro de la manche en cours*/
	int round;
  /**Position du joueur dont c'est le tour*/
	int position_token;
  /**Nombre de cartes dans une main*/
	int hand_length;
  /**Indique s'il est possible de jouer*/
	bool play_time;
  /**Indique si une manche a démarré*/
	bool round_started;
  /**Pioche*/
	std::vector<Card *> *draw;
  /**Liste des joueurs*/
	std::vector<Player *> *players; //Liste des équipes
  /**Liste des équipes*/
	std::vector<Team *> *teams;
  /**Grille du jeu*/
	Grid *grid;
  /**Indique si la pépite a été trouvée*/
	bool gold_found;
  /**Instance de l'accés à la BDD MYSQL*/
  MYSQL mysql;
  /**Boolean testant la connection à la base des données établie*/
  bool database_enabled;
};

#endif
