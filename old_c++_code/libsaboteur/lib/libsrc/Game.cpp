//Execution mysql mysql -u root -p

#include "Game.h"
#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <string.h>
#include <mysql/mysql.h>

using namespace std;

// void Game::_bind_methods()
// {
//   ClassDB::bind_method(D_METHOD("next_round"), &Game::next_round);
//   ClassDB::bind_method(D_METHOD("init_round"), &Game::init_round);
//   ClassDB::bind_method(D_METHOD("init_2_teams"), &Game::init_2_teams);
//   ClassDB::bind_method(D_METHOD("delete_teams"), &Game::delete_teams);
//   ClassDB::bind_method(D_METHOD("give_hands"), &Game::give_hands);
//   ClassDB::bind_method(D_METHOD("give_gold"), &Game::give_gold);
//   ClassDB::bind_method(D_METHOD("discard"), &Game::discard);
//   ClassDB::bind_method(D_METHOD("use_card"), &Game::use_card);
//   ClassDB::bind_method(D_METHOD("end_game"), &Game::end_game);
//   ClassDB::bind_method(D_METHOD("end_round"), &Game::end_round);
//   ClassDB::bind_method(D_METHOD("is_empty_hands"), &Game::is_empty_hands);
//   ClassDB::bind_method(D_METHOD("is_turn", "player"), &Game::is_turn);
//   ClassDB::bind_method(D_METHOD("next_player"), &Game::next_player);
//   ClassDB::bind_method(D_METHOD("init_draw", "mode"), &Game::init_draw);
//   ClassDB::bind_method(D_METHOD("void_draw"), &Game::void_draw);
//   ClassDB::bind_method(D_METHOD("add_player", "p"), &Game::add_player);
//   ClassDB::bind_method(D_METHOD("add_team", "t"), &Game::add_team);
//   ClassDB::bind_method(D_METHOD("give_roles"), &Game::give_roles);
//   ClassDB::bind_method(D_METHOD("way_to_nugget"), &Game::way_to_nugget);
//   ClassDB::bind_method(D_METHOD("way_to_start", "current_cell"), &Game::way_to_start);
//   ClassDB::bind_method(D_METHOD("get_game_id"), &Game::get_game_id);
//   ClassDB::bind_method(D_METHOD("next_round"), &Game::next_round);
//   ClassDB::bind_method(D_METHOD("get_nb_players"), &Game::get_nb_players);
//   ClassDB::bind_method(D_METHOD("get_hand_length"), &Game::get_hand_length);
//   ClassDB::bind_method(D_METHOD("get_position_token"), &Game::get_position_token);
//   ClassDB::bind_method(D_METHOD("get_play_time"), &Game::get_play_time);
//   ClassDB::bind_method(D_METHOD("get_round"), &Game::get_round);
//   ClassDB::bind_method(D_METHOD("get_round_started"), &Game::get_round_started);
//   ClassDB::bind_method(D_METHOD("get_gold_found"), &Game::get_gold_found);
//   ClassDB::bind_method(D_METHOD("get_grid"), &Game::get_grid);
//   ClassDB::bind_method(D_METHOD("get_player", "position"), &Game::get_player);
//   ClassDB::bind_method(D_METHOD("get_teams"), &Game::get_teams);
//   ClassDB::bind_method(D_METHOD("get_players"), &Game::get_players);
//   ClassDB::bind_method(D_METHOD("get_draw"), &Game::get_draw);
//   ClassDB::bind_method(D_METHOD("set_game_id", "g_id"), &Game::set_game_id);
//   ClassDB::bind_method(D_METHOD("set_hand_length"), &Game::set_hand_length);
//   ClassDB::bind_method(D_METHOD("set_position_token", "p_token"), &Game::set_position_token);
//   ClassDB::bind_method(D_METHOD("set_play_time", "p_time"), &Game::set_play_time);
//   ClassDB::bind_method(D_METHOD("set_round", "r"), &Game::set_round);
//   ClassDB::bind_method(D_METHOD("set_round_started", "r_started"), &Game::set_round_started);
//   ClassDB::bind_method(D_METHOD("set_grid", "gd"), &Game::set_grid);
//   ClassDB::bind_method(D_METHOD("set_teams", "tms"), &Game::set_teams);
//   ClassDB::bind_method(D_METHOD("set_draw", "drw"), &Game::set_draw);
//   ClassDB::bind_method(D_METHOD("set_players", "plrs"), &Game::set_players);
//   ClassDB::bind_method(D_METHOD("set_gold_found", "gf"), &Game::set_gold_found);
//   ClassDB::bind_method(D_METHOD("is_over"), &Game::is_over);
//   ClassDB::bind_method(D_METHOD("set_starting_point", "y_line", "x_center", "space"), &Game::set_starting_point);
//   ClassDB::bind_method(D_METHOD("set_arrival_points", "y_line", "x_center", "space", "position"), &Game::set_arrival_points);
//   ClassDB::bind_method(D_METHOD("set_starting_point"), &Game::set_starting_point);
//   ClassDB::bind_method(D_METHOD("set_arrival_points"), &Game::set_arrival_points);
// }

Game::Game()
{
  this->game_id = -1;                          //Identifiant du jeu
  this->round = 0;                             //Manche en cours
  this->position_token = 0;                    //Définit à qui est le tour de jouer
  this->hand_length = 0;                       //Taille d'une main
  this->play_time = false;                     //Si on est en temps de jeu
  this->round_started = false;                 //Si la manche a commencé
  this->draw = new std::vector<Card *>();      //Liste des cartes
  this->players = new std::vector<Player *>(); //Liste des joueurs
  this->teams = new std::vector<Team *>();     //Liste des équipes
  this->grid = new Grid();                     //Grille du jeu
  this->gold_found = false;
  this->database_enabled = true;
  mysql_init(&mysql);
  mysql_options(&mysql, MYSQL_READ_DEFAULT_GROUP, "default");
  if(!mysql_real_connect(&mysql, "localhost", "root", "saboteur", "saboteur", 0, NULL, 0)){
    this->database_enabled = false;
    cerr << "connection MYSQL échouée" << endl;
  }
  else{
    cout << "connection MYSQL réussie !" << endl;
  }
  create_game();
}

Game::~Game()
{
  delete (this->draw);
  delete (this->players);
  delete (this->teams);
}

int Game::generate_game_id(){
  MYSQL_RES *result = NULL;
  MYSQL_ROW row;
  int val=0;
  if(mysql_query(&this->mysql, "Select max(id_game) from Game")){
    cout << "select MYSQL échouée" << endl;
  }
  else{
    cout << "select MYSQL réussi !" << endl;
  }
  //On met le jeu de résultat dans le pointeur result
  result = mysql_use_result(&this->mysql);
  //Récupération du résultat
  if (!(row = mysql_fetch_row(result))){
    cout << "extraction MYSQL échouée !" << endl;
  }
  cout << "row[0] : " << row[0] << endl;
  val=atoi(row[0])+1;
  mysql_free_result(result);
  return val;
}

int Game::generate_team_id(){
  MYSQL_RES *result = NULL;
  MYSQL_ROW row;
  int val=0;
  if(mysql_query(&this->mysql, "Select max(id_team) from Team")){
    cout << "select MYSQL échouée" << endl;
  }
  else{
    cout << "select MYSQL réussi !" << endl;
  }
  //On met le jeu de résultat dans le pointeur result
  result = mysql_use_result(&this->mysql);
  //Récupération du résultat
  if (!(row = mysql_fetch_row(result))){
    cout << "extraction MYSQL échouée !" << endl;
  }
  cout << "row[0] : " << row[0] << endl;
  val=atoi(row[0])+1;
  mysql_free_result(result);
  return val;
}

int Game::get_game_id()
{
  return this->game_id;
}

void Game::set_game_id(int g_id)
{
  this->game_id = g_id;
  //Pas de changement car incohérence avec la BDD
  // if(database_enabled){
  //   char querry[100];
  //   sprintf(querry,"Update Game set id_game = %d where id_game = %d",g_id, this->game_id);
  //   if(mysql_query(&this->mysql, querry)){
  //     cerr <<  "MYSQL Error : Game::set_game_id : " << mysql_error(&mysql) << endl;
  //     return;
  //   }
  // }
}

int Game::get_round()
{
  return this->round;
}

void Game::set_round(int rd)
{
  this->round = rd;
  save_round();
}

bool Game::get_round_started()
{
  return this->round_started;
}

void Game::set_round_started(bool r_started)
{
  this->round_started = r_started;
}

int Game::get_position_token()
{
  return this->position_token;
}

void Game::set_position_token(int p_token)
{
  this->position_token = p_token;
  save_position_token();
}

bool Game::get_play_time()
{
  return this->play_time;
}

void Game::set_play_time(bool p_time)
{
  this->play_time = p_time;
}

Grid *Game::get_grid()
{
  return this->grid;
}

void Game::set_grid(Grid *gd)
{
  this->grid = gd;
  save_dimensions();
}

std::vector<Team *> *Game::get_teams()
{
  return this->teams;
}

std::vector<Player *> *Game::get_players()
{
  return this->players;
}

void Game::set_teams(std::vector<Team *> *tms)
{
  this->teams = tms;
}

void Game::set_players(std::vector<Player *> *plrs)
{
  this->players = plrs;
}

void Game::set_gold_found(bool gf)
{
  this->gold_found = gf;
}

std::vector<Card *> *Game::get_draw()
{
  return this->draw;
}

void Game::set_draw(std::vector<Card *> *drw)
{
  this->draw = drw;
}

bool Game::get_gold_found()
{
  return this->gold_found;
}

// Initialise une Manche
// précondition : tous les jouers sont déjà dans la partie
void Game::init_round()
{
  next_round(); //incrémenter le nombre de la manche
  init_draw(); //initialisation de la pioche
  cout << "draw size : " << this->draw->size() << endl;
  set_hand_length();     //définition de la taille des mains des joueurs
  give_hands(true);          //distribuer les cartes à tous les joueurs
  give_roles();          //distribution des rôles
  init_2_teams();        //création de 2 équipes saboteurs/chercheurs
  set_position_token(1); //a perfectionner
  set_gold_found(false);
  set_starting_point();
  set_arrival_points();
  set_round_started(true);
}

void Game::end_game()
{
  set_winner(); //Définit le/les gagnants
  save_stats_end_game(); //Sauvegarde des statistiques dans la BDD
}



void Game::end_round()
{
  give_gold(); //distribution des pépites d'or aux joueurs
  save_stats_end_round();
  void_draw();
  delete_teams();
  grid->void_cells();
  set_gold_found(false);
  set_round_started(false);
}

void Game::next_round()
{
  this->round++;
  save_round();
}

int Game::get_nb_players()
{
  return this->players->size();
}

int Game::get_hand_length()
{
  return this->hand_length;
}

void Game::set_hand_length()
{
  int nbr = get_nb_players();
  if (nbr >= 3 && nbr <= 5)
  {
    this->hand_length = 6;
  }
  else if (nbr >= 6 && nbr <= 7)
  {
    this->hand_length = 5;
  }
  else if (nbr >= 8 && nbr <= 10)
  {
    this->hand_length = 4;
  }
  else if (nbr < 3 || nbr > 10)
  {
    this->hand_length = -1;
    cout << "Nb players have to be between 3 and 10, actually : "
         << nbr << " players present." << endl;
  }
}

// Ajoute un joueur à la partie et à la BDD
void Game::add_player(Player *p, int ur_id)
{
  this->players->push_back(p);            //ajout du joueur
  p->set_position(this->players->size()); //entrée de la position du joueur
  p->set_user_id(ur_id); //entrée de la position du joueur
}

// Ajoute un joueur à la partie
void Game::add_player(Player *p)
{
  this->players->push_back(p);            //ajout du joueur
  p->set_position(this->players->size()); //entrée de la position du joueur
}

void Game::init_2_teams()
{
  if (this->teams->size() > 0)
  { //Si des équipes existent, détruire les équipes
    delete_teams();
  }
  //Création des équipes
  Team *saboteurs = new Team("Saboteurs");
  Team *chercheurs = new Team("Chercheurs");
  for (int i = 0; i < get_nb_players(); i++)
  {
    Player *player_tmp = this->players->at(i);
    if (player_tmp->get_saboteur())
    {
      saboteurs->add_player(player_tmp);
    }
    else
    {
      chercheurs->add_player(player_tmp);
    }
  }
  add_team(saboteurs);
  add_team(chercheurs);
}

void Game::delete_teams(){
  for (int i = 0; i < this->teams->size(); i++)
  {
    delete this->teams->at(i);
  }
  this->teams->clear();
}

void Game::add_team(Team *t){
  this->teams->push_back(t);
  create_team(t);
}

void Game::next_player(){
  if (this->position_token >= get_nb_players())
  {
    this->position_token = 0; //Back to the first player
    save_position_token();
  }
  else
  {
    this->position_token++;
    save_position_token();
  }
}

// Check the end of game state
bool Game::is_empty_hands(){
  bool player_hands_empty = true;
  if (draw->size() == 0)
  {
    for (int i = 0; i < this->players->size() && player_hands_empty; i++)
    {
      if (this->players->at(i)->get_hand()->size() != 0)
        player_hands_empty = false;
    }
  }
  else
  {
    return false;
  }
  return player_hands_empty;
}

//Teste si la partie est finie
bool Game::is_over(){
  if (is_empty_hands()){
    // set_winners('S');
    return true;
  }
  if (this->grid->path_finding())
  {
    // set_winners('C');
    set_gold_found(true);
    return true;
  }
  return false;
}

void Game::set_winner(){
  int max_gold = 0;
  for (int i = 0 ; i < this->players->size() ; i++){
    Player *p = get_player(i);
    if(max_gold < (p->get_gold())){
      max_gold = p->get_gold();
    }
  }
  for (int i = 0 ; i < this->players->size() ; i++){
    Player *p = get_player(i);
    if(max_gold == p->get_gold()){
      p->set_winner(true);
    }
  }
}

void Game::give_roles(){
  int nb_players = get_nb_players();
  if (nb_players < 3 || nb_players > 10)
  {
    cout << "Nb players have to be between 3 and 10, actually : "
         << nb_players << " players present." << endl;
  }
  int nb_saboteurs = 0;
  int nb_chercheurs = 0;
  if (nb_players == 3 || nb_players == 4)
  {
    nb_saboteurs = 1;
    nb_chercheurs = nb_players;
  }
  else if (nb_players == 5 || nb_players == 6)
  {
    nb_saboteurs = 2;
    nb_chercheurs = nb_players - 1;
  }
  else if (nb_players >= 7 || nb_players <= 9)
  {
    nb_saboteurs = 3;
    nb_chercheurs = nb_players - 2;
  }
  else if (nb_players == 10)
  {
    nb_saboteurs = 4;
    nb_chercheurs = 7;
  }
  //distribution aléatoire des rôles
  //initialisation du vecteur avec les rôles possibles
  std::vector<bool> vector = std::vector<bool>();
  for (int i = 0; i < nb_saboteurs; i++)
  {
    vector.push_back(true);
  }
  for (int j = 0; j < nb_chercheurs; j++)
  {
    vector.push_back(false);
  }
  // bool saboteur_given = false; //pour forcer la présence d'au moins un saboteur
  //pioche aléatoire des rôles pour chaque joueur
  for (int k = 0; k < nb_players; k++)
  {
    // if ((k == nb_players -1 )&&(!saboteur_given)) { //pour forcer la présence d'au moins un saboteur
    //   this->players[k]->set_saboteur(true);
    //   //cout << "saboteur imposé" << endl;
    // }
    // else{ //sinon aléatoire de la pioche de roles
    std::srand(std::time(0));
    int index = rand() % vector.size();
    this->players->at(k)->set_saboteur(vector[index]);
    // if (vector[index]) {
    //   saboteur_given = true;
    //   //cout << "saboteur trouvé" << endl;
    // }
    vector.erase(vector.begin() + index);
    // }
  }
}

void Game::give_hands(bool mode)
{
  int n = 0;
  // Distribution 1 par 1 à chaque joueur
  for (int j = 0; j < get_hand_length() && this->draw->size() > 0; j++)
  {
    for (int i = 0; i < this->players->size() && this->draw->size() > 0; i++)
    {
      this->players->at(i)->draw_card(this->draw, mode); // Give card to player
      n++;
    }
  }
  cout << "cards distributed : " << n << " ; " << get_hand_length() << " cards per player" << endl;
}

//distribution des pépites
//TODO  : distribution du nombre de pépites en fonction de l'ordre des derniers joueurs
void Game::give_gold()
{
  cout << "coucou" << endl;
  Player *player;
  for (int i = 0; i < this->players->size(); i++)
  {
    player = this->players->at(i);
    if (get_gold_found() && !player->get_saboteur())
      player->give_gold(1);
    if (!get_gold_found() && player->get_saboteur())
      player->give_gold(1);
  }
  save_gold();
}

// Test si le joeueur a le droit de jouer en ce moment
bool Game::is_turn(Player *player){
  return ((this->position_token == player->get_position()) && play_time);
}

// Read the database to initialize the cards
void Game::init_draw(){
  if (this->draw->size() > 0) //Si la pioche n'est pas vide, on la vide
  {
    void_draw();
  }
  if (this->database_enabled){ //Si la conenction BDD est établie, on lit dans la BDD
    init_draw_sql();
  }
  else //Sinon on utilise des cartes test
  {
    cout << "Lecture des cartes dans la BDD échouée, insertion de test activée. \n";
    // Insertion de quelques cartes "types" pour les phases de tests
    for (int i = 0; i < 8; i++)
    { // Insertion des cartes chemin
      Path *p1 = new Path(1, 1, 1, 1, 0);
      Path *p2 = new Path(1, 0, 1, 0, 0);
      Path *p3 = new Path(1, 1, 1, 1, 1);
      Path *p4 = new Path(1, 1, 1, 1, 1);
      this->draw->push_back(p1);
      this->draw->push_back(p2);
      this->draw->push_back(p3);
      this->draw->push_back(p4);
    }
    for (int i = 0; i < 3; i++)
    { // Insertion des cartes action
      Action *ap1 = new Action_on_player('P', 1);
      Action *ap2 = new Action_on_player('P', 0);
      Action *ag1 = new Action_on_grid(1, 0, 0);
      Action *ag2 = new Action_on_grid(0, 1, 0);
      this->draw->push_back(ap1);
      this->draw->push_back(ap2);
      this->draw->push_back(ag1);
      this->draw->push_back(ag2);
    }
  }
}

//Vide la pioche
void Game::void_draw(){
  this->draw->clear();
}

//Définit les cases de départ par défaut
void Game::set_starting_point(){
  int x_center = (this->grid->get_width() + 1) / 2 - 1;
  cout << "x_center = " << x_center << endl;
  cout << "x_right = " << x_center + 1 + 1 << endl;
  cout << "x_left = " << x_center - 1 - 1 << endl;
  int y_line = this->grid->get_height() - 2;
  this->grid->set_starting_point(y_line, x_center, 1);
}

//Définit les cases de départ
void Game::set_starting_point(int y_line, int x_center, int space){
  this->grid->set_starting_point(y_line, x_center, space);
}

//Définit les cases d'arrivée par défaut
void Game::set_arrival_points(){
  int x_center = (this->grid->get_width() + 1) / 2 - 1;
  int y_line = 1;
  cout << "x_center = " << x_center << endl;
  cout << "x_right = " << x_center + 1 + 1 << endl;
  cout << "x_left = " << x_center - 1 - 1 << endl;
  this->grid->set_arrival_points(y_line, x_center, 1, 2);
}

//Définit les cases d'arrivée
void Game::set_arrival_points(int y_line, int x_center, int space, int position){
  this->grid->set_arrival_points(y_line, x_center, space, position);
}

Player *Game::get_player(int position){
  return this->players->at(position);
}

bool Game::init_draw_sql(){
  MYSQL_RES *result = NULL;
  MYSQL_ROW row;
  int quantity = 0;
  int num_champs = 0;
  cout << "querry : Select * from Card" << endl;
  if(mysql_query(&this->mysql, "select a.id_card, a.quantity, a.id_type, b.id_action_on_grid,\
  b.reveal, b.remove, b.swap, c.id_action_on_player, c.positive, e.id_handicap,\
  e.pickaxe, e.mine_cart, e.lantern, d.id_path, d.blocked, d.north, d.south, d.east, d.west \
  from Card a \
  left outer join Action_On_Grid b on a.id_action_on_grid = b.id_action_on_grid \
  left outer join Action_On_Player c on a.id_action_on_player = c.id_action_on_player \
  left outer join Path d on a.id_path = d.id_path \
  left outer join Handicap e on c.id_handicap = e.id_handicap order by a.id_card")){
    cout << "select MYSQL échouée" << endl;
    return false;
  }
  else{
    cout << "select MYSQL réussi !" << endl;
  }
  //On met le jeu de résultat dans le pointeur result
  result = mysql_use_result(&this->mysql);
  //On récupère le nombre de champs
  num_champs = mysql_num_fields(result);
  //Récupération du résultat
  while ((row = mysql_fetch_row(result))){
    quantity=atoi(row[1]); //La quantité de chaque carte
    // cout << "quantity : " << quantity << endl;
    int type_card = atoi(row[2]);
    // unsigned long *lengths;
    for (int i = 0 ; i < quantity ; i++){ //n cartes en fonction de la quantité définie dans la bdd
      if(type_card==1){ //Carte chemin
        // cout << "row[2] == 1 \n";
        cout << "Path(" << atoi(row[15]) << ", " << atoi(row[16]) << ", " << atoi(row[17]) << ", "
        << atoi(row[18]) << ", " << atoi(row[14]) << ")\n";
        Path *p = new Path(atoi(row[15]), atoi(row[16]), atoi(row[17]), atoi(row[18]), atoi(row[14]));
        this->draw->push_back(p);
      }
      else if(type_card==2){ //Carte action sur un joueur
        // cout << "row[2] == 2 \n";
        char type_handicap;
        if (atoi(row[10])){ //pioche
          type_handicap = 'P';
        }
        else if(atoi(row[11])){ //chariot
          type_handicap = 'C';
        }
        else { //lanterne
          type_handicap = 'L';
        }
        cout << "Action_on_grid(" << type_handicap << ", " << atoi(row[8]) <<"); \n";
        Action *ap = new Action_on_player(type_handicap, atoi(row[8]));
        this->draw->push_back(ap);
      }
      else if(type_card==3){ //Carte action sur la grille
        Action *ag;
        if (atoi(row[4])) { //carte reveal
          ag= new Action_on_grid(1, 0, 0);
          cout << "Action_on_grid(1, 0, 0); \n";
        }
        else if(atoi(row[5])){ //carte remove
          ag= new Action_on_grid(0, 1, 0);
          cout << "Action_on_grid(0, 1, 0); \n";
        }
        else{ //carte swap
          ag= new Action_on_grid(0, 0, 1);
          cout << "Action_on_grid(0, 0, 1); \n";
        }
        this->draw->push_back(ag);
      }
    }
  }
  mysql_free_result(result);
  cout << "fin de l'initialisation de la pioche \n";
  return true;
}

bool Game::add_user(string login){
  if(database_enabled){
    if(!user_exists(login)){
      cerr << "Le pseudo \"" << login << "\" n'existe pas." << endl;
      return false;
    }
    MYSQL_RES *result = NULL;
    MYSQL_ROW row;
    int id_user=0;
    char querry[200];
    sprintf(querry,"Select distinct id_user from User where login = '%s' ", login.c_str());
    if(mysql_query(&this->mysql, querry)){
      cerr <<  "MYSQL Error : Game::add_user : " << mysql_error(&mysql) << endl;
      return false;
    }
    //On met le jeu de résultat dans le pointeur result
    result = mysql_use_result(&this->mysql);
    //Récupération du résultat
    if (!(row = mysql_fetch_row(result))){
      cout << "extraction MYSQL échouée !" << endl;
    }
    cout << "row[0] : " << row[0] << endl;
    id_user=atoi(row[0]);
    mysql_free_result(result);
    add_player(new Player(login, 0),id_user);
    return true;
  }
  return false;
}

bool Game::add_user(int usr_id){
  if(database_enabled){
    if(!user_exists(usr_id)){
      cerr << "L'utilisateur " << usr_id << " n'existe pas." << endl;
      return false;
    }
    MYSQL_RES *result = NULL;
    MYSQL_ROW row;
    char querry[200];
    sprintf(querry,"Select distinct login from User where id_user = %d ", usr_id);
    cout << "querry add_user : " << querry << endl;
    if(mysql_query(&this->mysql, querry)){
      cerr <<  "MYSQL Error : Game::add_user : " << mysql_error(&mysql) << endl;
    }
    //On met le jeu de résultat dans le pointeur result
    result = mysql_use_result(&this->mysql);
    //Récupération du résultat
    if (!(row = mysql_fetch_row(result))){
      cout << "extraction MYSQL échoué !" << endl;
      // return false;
    }
    cout << "row[0] add_user : " << row[0] << endl;
    string name(row[0]);
    mysql_free_result(result);
    add_player(new Player(name, 0),usr_id);
    return true;
  }
  return false;
}

bool Game::user_exists(string login){
  MYSQL_RES *result = NULL;
  MYSQL_ROW row;
  int val=0;
  char querry[200];
  sprintf(querry,"Select count(distinct id_user) from User where login = '%s' ", login.c_str());
  if(mysql_query(&this->mysql, querry)){
    cerr <<  "MYSQL Error : Game::user_exists : " << mysql_error(&mysql) << endl;
  }
  //On met le jeu de résultat dans le pointeur result
  result = mysql_use_result(&this->mysql);
  //Récupération du résultat
  if (!(row = mysql_fetch_row(result))){
    cout << "extraction MYSQL échouée !" << endl;
  }
  cout << "row[0] : " << row[0] << endl;
  val=atoi(row[0]);
  mysql_free_result(result);
  return (val>0);
}

bool Game::user_exists(int id_user){
  MYSQL_RES *result = NULL;
  MYSQL_ROW row;
  char querry[200];
  int val=0;
  sprintf(querry,"Select count(distinct id_user) from User where id_user = %d ", id_user);
  if(mysql_query(&this->mysql, querry)){
    cerr <<  "MYSQL Error : Game::user_exists : " << mysql_error(&mysql) << endl;
  }
  //On met le jeu de résultat dans le pointeur result
  result = mysql_use_result(&this->mysql);
  //Récupération du résultat
  if (!(row = mysql_fetch_row(result))){
    cout << "extraction MYSQL échouée !" << endl;
  }
  cout << "row[0] : " << row[0] << endl;
  val=atoi(row[0]);
  mysql_free_result(result);
  return (val>0);
}

bool Game::player_exists(int id_pl){
  MYSQL_RES *result = NULL;
  MYSQL_ROW row;
  char querry[200];
  int val=0;
  sprintf(querry,"Select count(distinct id_player) from Player where id_player = %d ", id_pl);
  if(mysql_query(&this->mysql, querry)){
    cerr <<  "MYSQL Error : Game::player_exists : " << mysql_error(&mysql) << endl;
  }
  //On met le jeu de résultat dans le pointeur result
  result = mysql_use_result(&this->mysql);
  //Récupération du résultat
  if (!(row = mysql_fetch_row(result))){
    cout << "extraction MYSQL échouée !" << endl;
  }
  cout << "row[0] : " << row[0] << endl;
  val=atoi(row[0]);
  mysql_free_result(result);
  return (val>0);
}

int Game::create_user(string logn,string passwd,string first_nm,string nm){
  char querry[300];
  int user_id = -1;
  if (this->database_enabled){
    user_id=generate_user_id();
    sprintf(querry,"Insert into User (id_user, login, password, name, first_name) values (%d,'%s','%s','%s','%s')",
      user_id,logn.c_str(), passwd.c_str(),first_nm.c_str(),nm.c_str());
    cout << "querry : " << querry << endl;
    if(mysql_query(&this->mysql, querry)){
      cerr <<  "MYSQL Error : Game::create_user : " << mysql_error(&mysql) << endl;
      return -1;
    }
  }
  cout << "Identifiant de l'utilisateur : " << user_id << endl;
  create_stat(user_id);
  return user_id; //gestion d'erreurs nécessaires
}

int Game::create_team(Team *t){
  int team_id = -1;
  if(database_enabled){
    //Insertion de la team dans la bdd
    char querry[200];
    team_id=generate_team_id();
    t->set_team_id(team_id);
    sprintf(querry,"Insert into Team values (%d,'%s',%d)",
      team_id,t->get_name().c_str(),this->game_id);
    cout << "querry : " << querry << endl;
    if(mysql_query(&this->mysql, querry)){
      cerr <<  "MYSQL Error : Game::create_team : " << mysql_error(&mysql) << endl;
      return -1;
    }
    for (int i = 0 ; i < t->get_players()->size() ; i++){
      Player *p = t->get_players()->at(i);
      p->set_team_id(team_id);
      if (player_exists(p->get_player_id())){
        save_player(p); //On met à jour les informations du joueur_player(p);
      }
      else{
        create_player(p); //Crée le joueur dans la bdd
      }
    }
  }
  return team_id;
}

bool Game::save_player(Player *p){
  if(database_enabled){
    //Insertion de la team dans la bdd
    char querry[200];
    sprintf(querry,"Update Player set num_gold = %d, position = %d, saboteur = %d, id_user = %d, \
    id_team = %d where id_player = %d",
    p->get_gold(),p->get_position(),p->get_saboteur(),p->get_user_id(),p->get_team_id(),p->get_player_id());
    cout << "querry : " << querry << endl;
    if(mysql_query(&this->mysql, querry)){
      cerr <<  "MYSQL Error : Game::save_player : " << mysql_error(&mysql) << endl;
      return false;
    }
    return true;
  }
  return false;
}

int Game::create_stat(int usr_id){
  char querry[300];
  int stat_id = -1;
  if (this->database_enabled){
    stat_id=generate_stat_id();
    sprintf(querry,"Insert into Statistic (id_stats, num_game, num_win, num_gold, num_round_saboteur, id_user) values (%d,0,0,0,0,%d)",stat_id,usr_id);
    cout << "querry : " << querry << endl;
    if(mysql_query(&this->mysql, querry)){
      cerr <<  "MYSQL Error : Game::create_stat : " << mysql_error(&mysql) << endl;
      return -1;
    }
    cout << "Identifiant de la statistique : " << stat_id << endl;
  }
  return stat_id; //gestion d'erreurs nécessaires
}

int Game::create_player(Player *p){
  cout << "create_player \n" ;
  int pl_id = -1;
  if (this->database_enabled){
    char querry[200];
    pl_id = generate_player_id();
    p->set_player_id(pl_id);
    sprintf(querry,"Insert into Player values (%d,'%s',%d,%d,%d,%d,%d)",
      p->get_player_id(),p->get_name().c_str(),p->get_gold(),p->get_position(),
      p->get_saboteur(),p->get_user_id(),p->get_team_id());
    cout << "querry : " << querry << endl;
    if(mysql_query(&this->mysql, querry)){
      cerr <<  "MYSQL Error : Player::create_player : " << mysql_error(&this->mysql) << endl;
    }
  }
  cout << "Identifiant de player : " << pl_id << endl;
  return pl_id;
}

int Game::generate_player_id(){
  MYSQL_RES *result = NULL;
  MYSQL_ROW row;
  int pl_id=-1;
  if(mysql_query(&this->mysql, "Select max(id_player) from Player")){
    cerr <<  "MYSQL Error : Game::generate_player_id : " << mysql_error(&mysql) << endl;
    return -1;
  }
  else{
    cout << "select MYSQL réussi !" << endl;
  }
  //On met le jeu de résultat dans le pointeur result
  result = mysql_use_result(&this->mysql);
  //Récupération du résultat
  if (!(row = mysql_fetch_row(result))){
    cout << "extraction MYSQL échouée !" << endl;
    return -1;
  }
  cout << "row[0] : " << row[0] << endl;
  pl_id=atoi(row[0])+1;
  mysql_free_result(result);
  return pl_id;
}

int Game::generate_user_id(){
  MYSQL_RES *result = NULL;
  MYSQL_ROW row;
  int val=0;
  if(mysql_query(&this->mysql, "Select max(id_user) from User")){
    cerr <<  "MYSQL Error : Game::generate_user_id : " << mysql_error(&mysql) << endl;
    return -1;
  }
  else{
    cout << "select MYSQL réussi !" << endl;
  }
  //On met le jeu de résultat dans le pointeur result
  result = mysql_use_result(&this->mysql);
  //Récupération du résultat
  if (!(row = mysql_fetch_row(result))){
    cout << "extraction MYSQL échouée !" << endl;
    return -1;
  }
  cout << "row[0] : " << row[0] << endl;
  val=atoi(row[0])+1;
  mysql_free_result(result);
  return val;
}

int Game::generate_stat_id(){
  MYSQL_RES *result = NULL;
  MYSQL_ROW row;
  int val=0;
  if(mysql_query(&this->mysql, "Select max(id_stats) from Statistic")){
    cout << "select MYSQL échouée" << endl;
    return -1;
  }
  else{
    cout << "select MYSQL réussi !" << endl;
  }
  //On met le jeu de résultat dans le pointeur result
  result = mysql_use_result(&this->mysql);
  //Récupération du résultat
  if (!(row = mysql_fetch_row(result))){
    cout << "extraction MYSQL échouée !" << endl;
    return -1;
  }
  cout << "row[0] : " << row[0] << endl;
  val=atoi(row[0])+1;
  mysql_free_result(result);
  return val;
}

bool Game::save_game_info(){
  if(database_enabled){
    char querry[300];
    sprintf(querry,"Update Game set token_position = %d, actual_round = %d, grid_width = %d, \
        grid_height = %d where id_game = %d",this->position_token,this->round,
        this->grid->get_width(), this->grid->get_height(), this->game_id);
    if(mysql_query(&this->mysql, querry)){
      cerr <<  "MYSQL Error : Game::save_game_info : " << mysql_error(&mysql) << endl;
      return false;
    }
    return true;
  }
  return false;
}

bool Game::save_position_token(){
  if(database_enabled){
    char querry[300];
    sprintf(querry,"Update Game set token_position = %d where id_game = %d",this->position_token,this->game_id);
    if(mysql_query(&this->mysql, querry)){
      cerr <<  "MYSQL Error : Game::save_position_token : " << mysql_error(&mysql) << endl;
      return false;
    }
    return true;
  }
  return false;
}

bool Game::save_stats_end_round(){
  if(database_enabled){
    for (int i = 0 ; i < this->players->size() ; i ++){
      Player *p = get_player(i);
      char querry[500];
      sprintf(querry,"Update Statistic set num_round_saboteur = num_round_saboteur + 1 where id_user = %d",p->get_user_id());
      if(mysql_query(&this->mysql, querry)){
        cerr <<  "MYSQL Error : Game::save_stats_end_round : " << mysql_error(&mysql) << endl;
        return false;
      }
    }
    return true;
  }
  return false;
}

bool Game::save_gold(){
  if(database_enabled){
    for (int i = 0 ; i < this->players->size() ; i ++){
      Player *p = get_player(i);
      char querry[500];
      sprintf(querry,"Update Player set num_gold = %d where id_player = %d",p->get_gold(),p->get_player_id());
      if(mysql_query(&this->mysql, querry)){
        cerr <<  "MYSQL Error : Game::save_gold : " << mysql_error(&mysql) << endl;
        return false;
      }
    }
    return true;
  }
  return false;
}

bool Game::save_round(){
  if(database_enabled){
    char querry[500];
    sprintf(querry,"Update Game set actual_round = %d where id_game = %d",this->round,this->game_id);
    if(mysql_query(&this->mysql, querry)){
      cerr <<  "MYSQL Error : Game::save_round : " << mysql_error(&mysql) << endl;
      return false;
    }
    return true;
  }
  return false;
}

int Game::create_game(){
  if (this->database_enabled){
    char querry[100];
    this->game_id=generate_game_id();
    sprintf(querry,"Insert into Game values (%d,%d,3,%d,%d,%d)",
      this->game_id,this->position_token,this->round,this->grid->get_width(),this->grid->get_height());
    cout << "querry : " << querry << endl;
    if(mysql_query(&this->mysql, querry)){
      cerr <<  "MYSQL Error : Game::create_game : " << mysql_error(&mysql) << endl;
      return -1;
    }
    cout << "Identifiant du jeu : " << this->game_id << endl;
    return this->game_id;
  }
  return -1;
}

bool Game::save_stats_end_game(){
  if(database_enabled){
    for (int i = 0 ; i < this->players->size() ; i ++){
      Player *p = get_player(i);
      char querry[500];
      sprintf(querry,"Update Statistic set num_game = num_game + 1, num_win = num_win + %d, num_gold = num_gold + %d where id_user = %d",p->get_winner(),p->get_gold(),p->get_user_id());
      if(mysql_query(&this->mysql, querry)){
        cerr <<  "MYSQL Error : Game::save_stats_game : " << mysql_error(&mysql) << endl;
        return false;
      }
    }
    return true;
  }
  return false;
}


bool Game::save_dimensions(){
  if(database_enabled){
    char querry[100];
    sprintf(querry,"Update Game set grid_width = %d, grid_height = %d where id_game = %d",
      this->grid->get_width(), this->grid->get_height(), this->game_id);
    if(mysql_query(&this->mysql, querry)){
      cerr <<  "MYSQL Error : Game::save_dimensions : " << mysql_error(&mysql) << endl;
      return false;
    }
    return true;
  }
  return false;
}
