#include "Path.h"

// Path::_bind_methods()
// {
//   ClassDB::bind_method(D_METHOD("get_path"), &Path::get_path);
//   ClassDB::bind_method(D_METHOD("is_blocked"), &Path::is_blocked);
// }

/**Crée une carte chemin en indiquant les directions et si c'est bloqué*/
Path::Path(bool Nord, bool Est, bool Sud, bool Ouest, bool is_blocked)
{
  this->path[0] = Nord;
  this->path[1] = Est;
  this->path[2] = Sud;
  this->path[3] = Ouest;

  this->blocked = is_blocked;

  this->object_type = 'P';
}

Path::~Path()
{
}

bool Path::get_path(int direction)
{
  if (direction >= 0 && direction < 4)
  {
    return path[direction];
  }
  else
  {
    return false; // A MODIFIER DANGER
  }
}

bool Path::is_blocked()
{
  return blocked;
}
