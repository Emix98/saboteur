#include "Player.h"
#include "Card.h"
#include "Action.h"
#include "Action_on_player.h"
#include "Action_on_grid.h"

using namespace std;

// void Player::_bind_methods()
// {
//   ClassDB::bind_method(D_METHOD("set_saboteur", "saboteur"), &Player::set_saboteur);
//   ClassDB::bind_method(D_METHOD("draw_card", "draw", "mode"), &Player::draw_card);
//   ClassDB::bind_method(D_METHOD("draw_hand", "draw", "size"), &Player::draw_hand);
//   ClassDB::bind_method(D_METHOD("use_path_card", "hand_position", "grid", "line", "column", "draw"), &Player::use_path_card);
//   ClassDB::bind_method(D_METHOD("use_action_on_player_card", "hand_position", "target", "draw"), &Player::use_action_on_player_card);
//   ClassDB::bind_method(D_METHOD("use_action_on_grid_card", "hand_position", "grid", "line", "column", "draw"), &Player::use_action_on_grid_card);
//   ClassDB::bind_method(D_METHOD("discard", "index"), &Player::discard);
//   ClassDB::bind_method(D_METHOD("give_gold", "gold"), &Player::give_gold);
//   ClassDB::bind_method(D_METHOD("get_position"), &Player::get_position);
//   ClassDB::bind_method(D_METHOD("get_saboteur"), &Player::get_saboteur);
//   ClassDB::bind_method(D_METHOD("get_name"), &Player::get_name);
//   ClassDB::bind_method(D_METHOD("get_handicap", "i"), &Player::get_handicap);
//   ClassDB::bind_method(D_METHOD("get_hand"), &Player::get_hand);
//   ClassDB::bind_method(D_METHOD("get_card", "position"), &Player::get_card);
//   ClassDB::bind_method(D_METHOD("get_gold"), &Player::get_gold);
//   ClassDB::bind_method(D_METHOD("get_player_id"), &Player::get_player_id);
//   ClassDB::bind_method(D_METHOD("get_team_id"), &Player::get_team_id);
//   ClassDB::bind_method(D_METHOD("set_handicap", "handi", "apply"), &Player::set_handicap);
//   ClassDB::bind_method(D_METHOD("set_position", "pos"), &Player::set_position);
// }

// Constructors/Destructors

Player::Player(int pos, bool saboteur, std::string player_name, bool handicaps[3], int gold, int id, int team) : position(pos), is_saboteur(saboteur), name(player_name), total_gold(gold), player_id(id), team_id(team)
{
  for (int i = 0; i < 3; i++)
  {
    handicap[i] = handicaps[i];
  }
  this->hand = new std::vector<Card *>();
  this->is_winner = false;
}

Player::Player(std::string player_name, int id) : name(player_name), player_id(id)
{
  this->name = player_name;
  this->position = 0;
  this->is_saboteur = false;
  this->user_id = -1;
  for (int i = 0; i < 3; i++)
  {
    this->handicap[i] = false;
  }
  this->total_gold = 0;
  this->hand = new std::vector<Card *>();
  this->is_winner = false;
}

Player::~Player()
{

  for (int i = 0; i < hand->size(); i++)
  {
    delete hand->at(i);
  }
  delete (this->hand);
}

std::string Player::get_name() const
{
  return name;
}

bool Player::get_saboteur() const
{
  return is_saboteur;
}

std::vector<Card *> *Player::get_hand() const
{
  return hand;
}

Card *Player::get_card(int position) const
{
  return this->hand->at(position);
}

int Player::get_position() const
{
  return this->position;
}

void Player::set_winner(bool winner)
{
  this->is_winner = winner;
}

bool Player::get_winner() const
{
  return this->is_winner;
}

bool Player::get_handicap(int i) const
{
  if (i >= 0 && i < 3)
  {
    return handicap[i];
  }
  else
  {
    std::cerr << "get_handicap() : Handicap inexistant !" << std::endl;
  }
  return false;
}

int Player::get_gold() const
{
  return total_gold;
}

int Player::get_player_id() const
{
  return player_id;
}

int Player::get_team_id() const
{
  return team_id;
}

void Player::set_team_id(int tm_id)
{
  team_id = tm_id;
}

void Player::set_handicap(int handi, bool apply)
{
  if (handi >= 0 && handi < 3)
  {
    handicap[handi] = apply;
    //std::cout << "set_handicap() : Handicap n°" << handi << (apply ? " appliqué" : " supprimé") << std::endl;
  }
  else
  {
    std::cerr << "set_handicap() : Handicap inexistant : aucune modif appliquée" << std::endl;
  }
}

void Player::set_saboteur(bool saboteur)
{
  is_saboteur = saboteur;
}

void Player::set_position(int pos)
{
  if (pos > 0)
    position = pos;
}

void Player::set_user_id(int ur_id)
{
  this->user_id = ur_id;
}

int Player::get_user_id() const
{
  return this->user_id;
}

void Player::set_player_id(int p_id)
{
  this->player_id = p_id;
}


//mode = false -> test mode ; mode = true-> mode normal
void Player::draw_card(std::vector<Card *> *draw, bool mode)
{

  if (draw->size() > 0)
  {
    if (!mode)
    {
      hand->push_back(draw->at(0));
      draw->erase(draw->begin());
    }
    else
    {
      std::srand(std::time(0));
      int drawn_card = rand() % draw->size();
      hand->push_back(draw->at(drawn_card));
      //std::cout << "Index carte piochée : " << drawn_card << std::endl;
      draw->erase(draw->begin() + drawn_card);
    }
  }
  else
  {
    std::cerr << "Pioche vide !" << std::endl;
  }
}

void Player::draw_hand(std::vector<Card *> *draw, int size)
{

  if (size >= 0)
  {
    for (int i = 0; i < size; i++)
    {
      draw_card(draw, true);
    }
    //std::cout << "Main constituée !" << std::endl;
    //std::cout << "Nouvelle taille main joueur " << get_name() << " : " << get_hand()->size() << std::endl;
  }
  else
  {
    std::cerr << "Taille passée négative !" << std::endl;
  }
}

bool Player::use_path_card(int hand_position, Grid *grid, int line, int column, std::vector<Card *> *draw)
{
  if (draw->size()<hand_position) {
    cerr << "Error use_path_card() : La position de carte n'existe pas.\n";
    return false;
  }
  bool test = grid->add_path(get_hand()->at(hand_position), line, column);
  if (test)
  {
    discard(hand_position);
    draw_card(draw, true);
  }
  return test;
}

bool Player::use_action_on_player_card(int hand_position, Player *target, std::vector<Card *> *draw)
{
  if (draw->size()<hand_position) {
    cerr << "Error use_action_on_player_card() : La position de carte n'existe pas.\n";
    return false;
  }
  Card *card = get_hand()->at(hand_position);
  if (get_hand()->at(hand_position)->get_object_type() != 'J')
  {
    std::cerr << "use_action_on_player_card() : La carte n'est pas un Action_on_player !" << std::endl;
    return false;
  }
  Action_on_player *aop_card = static_cast<Action_on_player *>(card);
  aop_card->apply(target);
  discard(hand_position);
  draw_card(draw, true);
  return true;
}

bool Player::use_action_on_grid_card(int hand_position, Grid *grid, int line, int column, std::vector<Card *> *draw)
{
  if (draw->size()<hand_position) {
    cerr << "Error use_action_on_grid_card() : La position de carte n'existe pas.\n";
    return false;
  }
  Card *card = get_hand()->at(hand_position);
  if (card->get_object_type() != 'G')
  {
    std::cerr << "use_action_on_player_card() : La carte n'est pas un Action_on_grid !" << std::endl;
    return false;
  }
  Action_on_grid *aog_card = static_cast<Action_on_grid *>(card);
  if (!aog_card->apply(grid, line, column))
    return false;
  discard(hand_position);
  draw_card(draw, true);
  return true;
}

void Player::discard(int index)
{
  if (index >= 0 && index < hand->size())
  {
    delete hand->at(index);
    hand->erase(hand->begin() + index);
    //std::cout << "Carte d'index " << index << " défaussée" << std::endl;
  }
  else
  {
    std::cerr << "Carte à défausser inexistante !" << std::endl;
  }
  //std::cout << "Nouvelle taille main joueur " << get_name() << " : " << hand->size() << std::endl;
}

void Player::give_gold(int gold)
{
  total_gold += gold;
  if (total_gold < 0){
    total_gold = 0;
  }
}

std::ostream &operator<<(std::ostream &stream, const Player &player)
{
  stream << "[" << player.get_name() << ", " << player.get_saboteur() << ", " << player.get_position() << ", {";
  for (int i = 0; i < 3; i++)
  {
    stream << player.get_handicap(i);
    if (i < 2)
      stream << ", ";
  }
  stream << "}, " << player.get_gold() << ", " << player.get_player_id() << ", " << player.get_team_id() << "]";
  return stream;
}
