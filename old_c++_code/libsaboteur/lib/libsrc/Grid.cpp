#include "Grid.h"
#include <iostream>
#include <vector>
#include <cmath>
#include "Action.h"
#include "Action_on_player.h"
#include "Action_on_grid.h"

using namespace std;

// void Grid::_bind_methods()
// {
//   ClassDB::bind_method(D_METHOD("set_width", "size"), &Grid::set_width);
//   ClassDB::bind_method(D_METHOD("set_height", "size"), &Grid::set_height);
//   ClassDB::bind_method(D_METHOD("get_width"), &Grid::get_width);
//   ClassDB::bind_method(D_METHOD("get_height"), &Grid::get_height);
//   ClassDB::bind_method(D_METHOD("add_path", "card", "line", "column"), &Grid::add_path);
//   ClassDB::bind_method(D_METHOD("reset_cell", "line", "column"), &Grid::reset_cell);
//   ClassDB::bind_method(D_METHOD("get_cell", "line", "column"), &Grid::get_cell);
//   ClassDB::bind_method(D_METHOD("set_starting_point", "line", "column_center", "space"), &Grid::set_starting_point);
//   ClassDB::bind_method(D_METHOD("set_arrival_points", "line", "column_center", "space", "position"), &Grid::set_arrival_points);
//   ClassDB::bind_method(D_METHOD("try_placement", "path", "line", "column"), &Grid::try_placement);
//   ClassDB::bind_method(D_METHOD("get_gold_position"), &Grid::get_gold_position);
//   ClassDB::bind_method(D_METHOD("void_cells"), &Grid::void_cells);
//   ClassDB::bind_method(D_METHOD("get_starting_point"), &Grid::get_starting_point);
//   ClassDB::bind_method(D_METHOD("get_ending_points"), &Grid::get_ending_points);
//   ClassDB::bind_method(D_METHOD("path_finding"), &Grid::path_finding);
//   ClassDB::bind_method(D_METHOD("path_finding_aux", "cell_vector", "line", "column"), &Grid::path_finding_aux);
// }

Grid::Grid()
{
  this->width = 7;
  this->height = 11;
  this->cells = new Cell **[this->height];
  for (int i = 0; i < this->height; ++i)
  {
    cells[i] = new Cell *[this->width];
    for (int j = 0; j < this->width; ++j)
    {
      cells[i][j] = new Cell();
    }
  }
  this->starting_point = new int[2];
  this->ending_points = new int *[3];
  for (int i = 0; i < 3; i++)
  {
    this->ending_points[i] = new int[2];
    this->ending_points[i][0] = 0;
    this->ending_points[i][1] = 0;
  }
}

Grid::Grid(int w, int h)
{
  if ((w % 2 != 0) || (w < 5) || (w < 5))
  {
    cout << "The grid's dimension are not respected : minimum 5 width, odd width, minimum 5 height." << endl;
    cout << "- Default numbers are being set : width = 7 ; height = 11.";
    Grid(); //mettre les valeurs par défaut;
    return;
  }
  this->width = w;
  this->height = h;
  this->cells = new Cell **[this->height];
  for (int i = 0; i < this->height; ++i)
  {
    cells[i] = new Cell *[this->width];
    for (int j = 0; j < this->width; ++j)
    {
      cells[i][j] = new Cell();
    }
  }
  this->starting_point = new int[2];
  this->ending_points = new int *[3];
  for (int i = 0; i < 3; i++)
  {
    this->ending_points[i] = new int[2];
    this->ending_points[i][0] = 0;
    this->ending_points[i][1] = 0;
  }
}

Grid::Grid(Cell ***c, int w, int h)
{
  this->width = w;
  this->height = h;
  this->cells = c;
}

Grid::~Grid()
{
}

void Grid::set_width(int size)
{
  if (size > 0)
    this->width = size;
}

void Grid::set_height(int size)
{
  if (size > 0)
    this->height = size;
}

int Grid::get_width()
{
  return width;
}

int Grid::get_height()
{
  return height;
}

//ajoute une carte sur la grille, renvoye faux si l'action a échouée
//précondition : la carte est une carte chemin (Path)
bool Grid::add_path(Card *card, int line, int column)
{
  if (card->get_object_type() != 'P')
  {
    cerr << "Error add_path() : la carte utilisée n'est pas un chemin\n";
    return false;
  }
  Path *path_tmp = static_cast<Path *>(card);
  if (!try_placement(path_tmp, line, column))
  {
    return false;
  }
  this->cells[line][column]->set_path(path_tmp->get_path(0), path_tmp->get_path(1), path_tmp->get_path(2), path_tmp->get_path(3),
                                      path_tmp->is_blocked());
  return true;
}

//retourne cell de coord. x y
Cell *Grid::get_cell(int line, int column)
{
  return cells[line][column];
}

//réinitialise les valeurs d'une cellule
void Grid::reset_cell(int line, int column)
{
  this->cells[line][column]->reset_all();
}

//Définit les cases de départ
void Grid::set_starting_point(int line, int column_center, int space)
{
  if ((this->width < column_center + space + 1) || (column_center - space - 1 < 0))
  {
    cout << "x coordinates for starting_point are invalid." << endl;
    return;
  }
  if ((this->height < line + 2) || (line < 0))
  {
    cout << "y coordinates for starting_point are invalid." << endl;
    return;
  }
  this->starting_point[0] = line;
  this->starting_point[1] = column_center;
  this->cells[line][column_center]->set_start();
}

//Définit les cases d'arrivée
void Grid::set_arrival_points(int line, int column_center, int space, int position)
{
  if ((this->width < column_center + space + 1) || (column_center - space - 1 < 0))
  {
    cout << "x coordinates for arrival_points are invalid." << endl;
    return;
  }
  if ((this->height < line + 2) || (line < 0))
  {
    cout << "y coordinates for arrival_points are invalid." << endl;
    return;
  }
  this->ending_points[0][0] = line;
  this->ending_points[0][1] = column_center - space - 1;
  this->ending_points[1][0] = line;
  this->ending_points[1][1] = column_center;
  this->ending_points[2][0] = line;
  this->ending_points[2][1] = column_center + space + 1;

  int arrival_number = 0;
  std::srand(std::time(0));
  if (!position)
    arrival_number = rand() % 3;
  else
    arrival_number = position - 1;
  if (arrival_number == 0)
  {
    this->cells[line][column_center - space - 1]->set_gold();
    this->cells[line][column_center]->set_rock();
    this->cells[line][column_center + space + 1]->set_rock();
    this->gold_position = 1;
  }
  else if (arrival_number == 1)
  {
    this->cells[line][column_center - space - 1]->set_rock();
    this->cells[line][column_center]->set_gold();
    this->cells[line][column_center + space + 1]->set_rock();
    this->gold_position = 2;
  }
  else
  {
    this->cells[line][column_center - space - 1]->set_rock();
    this->cells[line][column_center]->set_rock();
    this->cells[line][column_center + space + 1]->set_gold();
    this->gold_position = 3;
  }
}

int Grid::get_gold_position()
{
  return this->gold_position;
}

//Vérifie si le placement d'un chemin est possible aux coordonnées données de la grille
bool Grid::try_placement(Path *path, int line, int column)
{
  if ((line < 0) || (line >= this->height) || (column < 0) || (column >= this->width))
  {
    cerr << "try_placement() : invalid coordinates" << endl;
    return false;
  }
  if (!get_cell(line, column)->is_empty())
  { //si la case est occupée
    cerr << "try_placement() : cell is not empty" << endl;
    return false;
  }
  bool surrounding_path = false;
  //nord
  if (line - 1 >= 0)
  {
    if ((get_cell(line - 1, column)->get_type() == 'P') || (get_cell(line - 1, column)->get_type() == 'S'))
    {
      surrounding_path = true;
      if ((get_cell(line - 1, column)->get_path(2) != path->get_path(0)))
      {
        cerr << "try_placement() : up incompatible path" << endl;
        return false;
      }
    }
  }
  //est
  if (column + 1 < this->width)
  {
    if ((get_cell(line, column + 1)->get_type() == 'P') || (get_cell(line, column + 1)->get_type() == 'S'))
    {
      surrounding_path = true;
      if ((get_cell(line, column + 1)->get_path(3) != path->get_path(1)))
      {
        cerr << "try_placement() : right incompatible path" << endl;
        return false;
      }
    }
  }
  //sud
  if (line + 1 < this->height)
  {
    if ((get_cell(line + 1, column)->get_type() == 'P') || (get_cell(line + 1, column)->get_type() == 'S'))
    {
      surrounding_path = true;
      if ((get_cell(line + 1, column)->get_path(0) != path->get_path(2)))
      {
        cerr << "try_placement() : down incompatible path" << endl;
        return false;
      }
    }
  }
  //ouest
  if (column - 1 >= 0)
  {
    if ((get_cell(line, column - 1)->get_type() == 'P') || (get_cell(line, column - 1)->get_type() == 'S'))
    {
      surrounding_path = true;
      if ((get_cell(line, column - 1)->get_path(1) != path->get_path(3)))
      {
        cerr << "try_placement() : left incompatible path" << endl;
        return false;
      }
    }
  }
  if (!surrounding_path)
    cerr << "try_placement() : no surrounding path" << endl;
  return surrounding_path;
}

void Grid::void_cells()
{
  for (int i = 0; i < this->height; i++)
  {
    for (int j = 0; j < this->width; j++)
    {
      cells[i][j]->reset_all();
    }
  }
}

int *Grid::get_starting_point()
{
  return this->starting_point;
}

int **Grid::get_ending_points()
{
  return this->ending_points;
}

//recherche d'un chemin vers une pépite d'or
bool Grid::path_finding()
{
  int count_path = 0;
  //compte le nombre de cartes chemin sur la grille
  for (int i = 0; i < this->height; i++)
  {
    for (int j = 0; j < this->width; j++)
    {
      if (get_cell(i, j)->get_type() == 'P')
        count_path++;
    }
  }
  //si on a un nombre minimal de cartes requis à atteindre le chemin,
  //on test si un chemin existe entre le départ et la fin
  if (count_path >= (get_starting_point()[0] - get_ending_points()[1][0] - 1))
  {
    int line = get_starting_point()[0];
    int column = get_starting_point()[1];
    vector<Cell *> *cell_vector = new vector<Cell *>;
    if (path_finding_aux(cell_vector, line, column))
    {
      for (int i = 0; i < cell_vector->size(); i++)
      {
        cell_vector->at(i)->set_marked(false);
      }
      delete (cell_vector);
      return true;
    }
    for (int i = 0; i < cell_vector->size(); i++)
    {
      cell_vector->at(i)->set_marked(false);
    }
    delete (cell_vector);
  }
  return false;
}

//fonction auxiliaire de la recherche de chemin
bool Grid::path_finding_aux(vector<Cell *> *cell_vector, int line, int column)
{
  get_cell(line, column)->set_marked(true);
  cell_vector->push_back(get_cell(line, column));
  cout << "parcours en progfondeur : (" << line << "," << column << ")" << endl;
  if (line - 1 > 0)
  { //haut
    Cell *up_cell = get_cell(line - 1, column);
    if (up_cell->get_type() == 'G')
      return true;
    if ((up_cell->get_type() == 'P') && (!up_cell->is_blocked()) && (!up_cell->get_marked()))
      if (path_finding_aux(cell_vector, line - 1, column))
        return true;
  }
  if (column + 1 < this->width)
  { //droite
    Cell *right_cell = get_cell(line, column + 1);
    if (right_cell->get_type() == 'G')
      return true;
    if ((right_cell->get_type() == 'P') && (!right_cell->is_blocked()) && (!right_cell->get_marked()))
      if (path_finding_aux(cell_vector, line, column + 1))
        return true;
  }
  if (line + 1 < this->height)
  { //bas
    Cell *down_cell = get_cell(line + 1, column);
    if (down_cell->get_type() == 'G')
      return true;
    if ((down_cell->get_type() == 'P') && (!down_cell->is_blocked()) && (!down_cell->get_marked()))
      if (path_finding_aux(cell_vector, line + 1, column))
        return true;
  }
  if (column - 1 > 0)
  { //gauche
    Cell *left_cell = get_cell(line, column - 1);
    if (left_cell->get_type() == 'G')
      return true;
    if ((left_cell->get_type() == 'P') && (!left_cell->is_blocked()) && (!left_cell->get_marked()))
      if (path_finding_aux(cell_vector, line, column - 1))
        return true;
  }
  return false;
}
