Main: Main.o Game.o Card.o Grid.o Cell.o Player.o Team.o Path.o Action.o Action_on_grid.o Action_on_player.o
	g++ -g -o Main Game.o Main.o Card.o Grid.o Cell.o Player.o Team.o Path.o Action.o Action_on_grid.o Action_on_player.o

Game.o: Game.cpp Game.h
	g++ -g -c Game.cpp

Main.o: Main.cpp Card.h Grid.h Cell.h Player.h Team.h
	g++ -g -c Main.cpp

Card.o: Card.cpp Card.h
	g++ -g -c Card.cpp

Grid.o: Grid.cpp Grid.h
	g++ -g -c Grid.cpp

Cell.o: Cell.cpp Cell.h
	g++ -g -c Cell.cpp

Player.o: Player.cpp Player.h
	g++ -g -c Player.cpp

Team.o: Team.cpp Team.h
	g++ -g -c Team.cpp

Path.o: Path.cpp Path.h
	g++ -g -c Path.cpp

Action.o: Action.cpp Action.h
	g++ -g -c Action.cpp

Action_on_grid.o: Action_on_grid.cpp Action_on_grid.h
	g++ -g -c Action_on_grid.cpp

Action_on_player.o: Action_on_player.cpp Action_on_player.h
	g++ -g -c Action_on_player.cpp

clean:
	rm *.o
	rm Main

testgame:
	g++ -o testgame $(shell mysql_config --libs) tests/TestGame.cpp Game.cpp Card.cpp Grid.cpp Cell.cpp Player.cpp Team.cpp Path.cpp Action.cpp Action_on_grid.cpp Action_on_player.cpp $(shell mysql_config --libs)

testgameosx:
	g++ -I/usr/local/mysql/include -L/usr/local/mysql/lib -lmysqlclient -o testgame tests/TestGame.cpp Game.cpp Card.cpp Grid.cpp Cell.cpp Player.cpp Team.cpp Path.cpp Action.cpp Action_on_grid.cpp Action_on_player.cpp
