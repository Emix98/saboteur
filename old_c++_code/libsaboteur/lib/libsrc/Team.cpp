#include "Team.h"
#include <iostream>
#include <vector>

// void Team::_bind_methods() {
//
//   ClassDB::bind_method(D_METHOD("get_players"), &Team::get_players);
//   ClassDB::bind_method(D_METHOD("get_player", "position"), &Team::get_player);
//   ClassDB::bind_method(D_METHOD("set_players", "p"), &Team::set_players);
//   ClassDB::bind_method(D_METHOD("add_player", "p"), &Team::add_player);
//   ClassDB::bind_method(D_METHOD("delete_player", "p"), &Team::delete_player);
//   ClassDB::bind_method(D_METHOD("delete_player", "n"), &Team::delete_player);
//   ClassDB::bind_method(D_METHOD("get_name"), &Team::get_name);
//   ClassDB::bind_method(D_METHOD("set_name", "nm"), &Team::set_name);
//
// }

//constructeur
Team::Team(std::string nm){
	team_id = -1;
	total_gold = 0;
	game_id = -1;
	name = nm;
	this->players = new std::vector<Player*>();
	// is_winner = false;
}

//constructeur
Team::Team(int id, int gold, int game, std::string nm){
	team_id = id;
	total_gold = gold;
	game_id = game;
	name=nm;
	this->players = new std::vector<Player*>();
	// is_winner = false;
}

//destructeur
Team::~Team(){
	delete(this->players);
}

//retourne les joueurs
std::vector<Player*>* Team::get_players(){
	return players;
}

//retourne un joueur
Player* Team::get_player(int position){
	return this->players->at(position);
}

//modifie le vecteur de joueurs
void Team::set_players(std::vector<Player*>* p){
	this->players = p;
}

//retourne un joueur
void Team::set_team_id(int tm_id){
	this->team_id=tm_id;
}

//retourne un joueur
int Team::get_team_id(){
	return this->team_id;
}

//ajouter un joueur à l'équipe
void Team::add_player(Player *p){
	this->players->push_back(p);
}

//retire un joueur donné de l'équipe
void Team::delete_player(Player *p){
	for(int i = 0 ; i < this->players->size() ; i++){
		if (this->players->at(i)==p){
			this->players->erase(this->players->begin()+i);
		}
	}
}

//retire un joueur à la position n
void Team::delete_player(int n){
	this->players->erase(this->players->begin()+n);
}

//retourne le nom de l'équipe
std::string Team::get_name(){
	return this->name;
}

//modifie le nom de l'équipe
void Team::set_name(std::string nm){
	this->name = nm;
}
