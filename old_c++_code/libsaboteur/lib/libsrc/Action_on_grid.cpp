#include "Action_on_grid.h"

// void Action_on_grid::_bind_methods()
// {
//   ClassDB::bind_method(D_METHOD("is_remove"), &Action_on_grid::is_remove);
//   ClassDB::bind_method(D_METHOD("is_swap"), &Action_on_grid::is_swap);
//   ClassDB::bind_method(D_METHOD("is_reveal"), &Action_on_grid::is_reveal);
//   ClassDB::bind_method(D_METHOD("assign_card", "c"), &Action_on_grid::assign_card);
//   ClassDB::bind_method(D_METHOD("apply", "grid", "line", "column"), &Action_on_grid::apply);
// }

Action_on_grid::Action_on_grid(bool reveal, bool remove, bool swap)
{
  this->reveal = reveal;
  this->remove = remove;
  this->swap = swap; //En plus du jeu de base, swapper une pierre et un caillou
  this->object_type = 'G';
}

Action_on_grid::~Action_on_grid()
{
}

bool Action_on_grid::is_reveal()
{
  return this->reveal;
}

bool Action_on_grid::is_remove()
{
  return this->remove;
}

bool Action_on_grid::is_swap()
{
  return this->swap;
}

//Obsolète
void Action_on_grid::assign_card(char c)
{
  // R Reveal
  // M  reMove
  // S Swap

  if (c == 'r' || c == 'R')
  {
    this->reveal = true;
  }
  else if (c == 'm' || c == 'M')
  {
    this->remove = true;
  }
  else
  {
    this->swap = true;
  }
}

//applique l'action d'une carte sur une grille
bool Action_on_grid::apply(Grid *grid, int line, int column)
{
  Cell *current_cell = grid->get_cell(line, column);
  if (is_reveal())
  { //Révélation si la cellule est une pépite
    if (current_cell->is_gold())
    {
      return true;
    }
  }
  else if (is_remove())
  { //Destruction d'une cellule
    if (!(current_cell->is_indestructible()))
    {
      current_cell->reset_all();
      return true;
    }
  }
  else if (is_swap())
  { //Intervertir 2 cartes
    //TODO, maybe... hope not...player
    return false;
  }
  return false;
}
