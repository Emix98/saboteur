#include <iostream>

#include "../Player.h"
#include "../Card.h"
#include "../Action.h"
#include "../Action_on_player.h"
#include "../Action_on_grid.h"

#define MARGIN 0.00000000000000000001

using namespace std;

void assertEquals(int value, int expected, string errmsg) {
  if (value != expected) {
    cerr << errmsg << " (attendu : " << expected << " / obtenu : " << value << ")" << endl;
    exit(1);
  }
}

void assertEquals(double value, double expected, double margin, string errmsg) {
  if (abs(value-margin) >= margin) {
    cerr << errmsg << " (attendu : " << expected << " / obtenu : " << value << ")" << endl;
    exit(1);
  }
}

void assertEquals(bool value, bool expected, string errmsg) {
  if (value != expected) {
    cerr << errmsg << " (attendu : " << expected << " / obtenu : " << value << ")" << endl;
    exit(1);
  }
}

void assertEquals(string value, string expected, string errmsg) {
  if (value != expected) {
    cerr << errmsg << endl;
    cerr << " Attendu : " << expected << endl << "Obtenu : " << value << endl;
    exit(1);
  }
}

void separator() {
  cout << "-----------------------------------------------------------" << endl;
}

int main() {

  //Création joueurs
  bool handicaps[3] = {false, false, false};
  string p1name = "Joueur 1";
  string p2name = "Joueur 2";
  string p3name = "Joueur 2 épisode 1";
  Player *player1 = new Player(1, true, p1name, handicaps, 0, 9, 55555);
  Player *player2 = new Player(2, false, p2name, handicaps, 0, 2, 66666);
  Player *player3 = new Player(p3name, 4, 44444);
  //Tests sur joueur 1
  assertEquals(player1->get_name(), p1name, "Nom du joueur 1 incorrect !");
  for (int i = 0 ; i < 3 ; i++) {
    assertEquals(player1->get_handicap(i), handicaps[i], "Handicaps du joueur 1 incorrects !");
  }
  //Tests sur joueur 2
  assertEquals(player2->get_name(), p2name, "Nom du joueur 2 incorrect !");
  for (int i = 0 ; i < 3 ; i++) {
    assertEquals(player2->get_handicap(i), handicaps[i], "Handicaps du joueur 2 incorrects !");
  }
  //Tests sur joueur 3
  assertEquals(player3->get_name(), p3name, "Nom du joueur 3 incorrect !");
  for (int i = 0 ; i < 3 ; i++) {
    assertEquals(player3->get_handicap(i), false, "Handicaps du joueur 3 incorrects !");
  }
  
  //Ajout d'une position et d'un rôle au joueur 3
  player3->set_position(3);
  assertEquals(player3->get_position(), 3, "Position du joueur 3 incorrecte !");
  player3->set_saboteur(false);
  assertEquals(player3->get_saboteur(), false, "Le joueur 3 ne devrait pas être un saboteur !");
  
  cout << *player1 << endl << *player2 << endl << *player3 << endl;

  separator();

  //Initialisation pioche
  vector<Card*> draw;
  for (int i = 0 ; i < 12 ; i++) {
    draw.push_back(new Card());
  }
  assertEquals(draw.size(), 12, "Erreur initialisation pioche !");
  cout << "Pioche initialisée" << endl << "Taille pioche initiale : " << draw.size() << endl;

  separator();

  //Constitution de la main du joueur 1 (est censé marcher)
  player1->draw_hand(&draw, 5);
  assertEquals(player1->get_hand().size(), 5, "Le joueur 1 n'a pas cinq cartes !");
  cout << "Nouvelle taille pioche : " << draw.size() << endl;
  //Constitution d'une main de taille négative (est censé renvoyer un message d'erreur)
  player2->draw_hand(&draw, -3);
  assertEquals(player2->get_hand().size(), 0, "Le joueur 2 a des cartes !");
  cout << "Nouvelle taille pioche : " << draw.size() << endl;
  //Constitution de la main du joueur 2
  player2->draw_hand(&draw, 4);
  assertEquals(player2->get_hand().size(), 4, "Le joueur 2 n'a pas quatre cartes !");
  cout << "Nouvelle taille pioche : " << draw.size() << endl;

  separator();

  //Défausse d'une carte du joueur 1 (est censé marcher)
  player1->discard(1);
  assertEquals(player1->get_hand().size(), 4, "Joueur 1 devrait avoir perdu une carte !");
  //Défausse de six cartes du joueur 2 (est censé renvoyer un message pour les deux dernières)
  for (int i = 0 ; i < 6 ; i++) {
    player2->discard(0);
  }
  assertEquals(player2->get_hand().size(), 0, "Le joueur 2 devrait avoir 0 cartes !");
  //Défausse d'une carte inexistante du joueur 1 (est censé renvoyer un message d'erreur)
  player1->discard(-1);
  assertEquals(player1->get_hand().size(), 4, "Le joueur 1 ne devrait pas avoir perdu de carte !");

  separator();

  //Joueur 2 pioche 4 cartes (censé marcher pour les trois premières et renvoyer un message d'erreur pour la quatrième)
  for (int i = 0 ; i < 4 ; i++) {
    player1->draw_card(&draw);
    assertEquals(draw.size(), (3-(i+1) > 0 ? 3-(i+1) : 0), "La pioche ne devrait pas avoir autant de cartes");
  }

  separator();

  //Application d'un handicap 2 au joueur 1 et de handicaps 0,1 au joueur 2
  player1->set_handicap(2, true);
  assertEquals(player1->get_handicap(2), true, "Handicap 2 non appliqué au joueur 1 !");
  player2->set_handicap(0, true);
  assertEquals(player2->get_handicap(0), true, "Handicap 0 non appliqué au joueur 2 !");
  player2->set_handicap(1, true);
  assertEquals(player2->get_handicap(1), true, "Handicap 1 non appliqué au joueur 2 !");
  //Suppression du handicap 0 (inexistant) du joueur 1 (est censé ne rien changer)
  player1->set_handicap(0, false);
  assertEquals(player1->get_handicap(0), false, "Joueur 1 n'est pas censé avoir un handicap 0 !");
  //Suppression du handicap 1 du joueur 2
  player2->set_handicap(1, false);
  assertEquals(player2->get_handicap(1), false, "Joueur 2 n'est plus censé avoir un handicap 1 !");

  separator();

  //Ajout d'une carte handicap 0 dans la pioche et don de cette carte au joueur 2
  Action_on_player *handicap0 = new Action_on_player();
  handicap0->assign_card('c', false);
  assertEquals(handicap0->get_handicap_item(0), true, "La carte handicap0 n'a pas de handicap chariot !");
  assertEquals(handicap0->get_handicap_item(1), false, "La carte handicap0 ne devrait pas avoir de handicap lampe !");
  assertEquals(handicap0->get_handicap_item(2), false, "La carte handicap0 ne devrait pas avoir de handicap pioche !");
  draw.push_back(handicap0);

  player2->draw_card(&draw);
  cout << "Nouvelle taille main joueur " << player2->get_name() << " : " << player2->get_hand().size() << endl;


  //Application de la carte handicap au joueur 1
  for (int i = 0 ; i < player2->get_hand().size() ; i++) {
    player2->use_action_on_player_card(player2->get_hand()[i], player1);
  }
  assertEquals(player1->get_handicap(0), true, "Joueur 1 devrait avoir un handicap 0 !");
  assertEquals(player1->get_handicap(1), false, "Joueur 1 ne devrait pas avoir de handicap 0 !");
  assertEquals(player1->get_handicap(2), true, "Joueur 1 devrait avoir un handicap 2 !");

  separator();

  //Ajout de quatre d'or au joueur 1 et 2 d'or au joueur 2
  player1->give_gold(4);
  player2->give_gold(2);
  assertEquals(player1->get_gold(), 4, "Joueur 1 devrait avoir 4 d'or !");
  assertEquals(player2->get_gold(), 2, "Joueur 2 devrait avoir 2 d'or !");
  //Retrait de six d'or au joueur 1 (est censé en avoir 0)
  player1->give_gold(-6);
  assertEquals(player1->get_gold(), 0, "Joueur 1 devrait avoir 0 d'or !");
  //Don de trois d'or au joueur 1 (est censé en avoir 3)
  player1->give_gold(3);
  assertEquals(player1->get_gold(), 3, "Joueur 1 devrait avoir 3 d'or !");
  
  separator();
  
  cout << *player1 << endl << *player2 << endl << *player3 << endl;

  separator();

  delete player1; cout << "Joueur 1 supprimé" << endl;
  delete player2; cout << "Joueur 2 supprimé" << endl;
  delete player3; cout << "Joueur 3 supprimé" << endl;
  //delete handicap0; cout << "Handicap0 supprimé" << endl;
  for (int i = 0 ; i < draw.size() ; i++) {
    delete draw[i]; cout << "Carte de la pioche supprimée" << endl;
  }

  separator();

  //Si tous les tests passent, félicitations : vous êtes un champion !
  cout << "Tout s'est bien passé mon capitaine !" << endl;
  return 0;

}
