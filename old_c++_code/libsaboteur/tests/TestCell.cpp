#include "../Cell.h"
#include "TestFunc.h"
#include <stdlib.h>
#include <iostream>

#define TAILLE 4
#define NORD 0
#define EST 1
#define SUD 2
#define OUEST 3
#define DEFAULT_COLOUR -1

//COMPILATION :  g++ -o main TestCell.cpp ../Cell.cpp
using namespace std;

int main()
{
    Cell *c1,*c2;
    c1 = new Cell();
    c2 = new Cell();

    //c1 pépite d'or
    c1->set_gold();

    //test is_gold
    assertEquals( c1->is_gold(), true, "c1 devrait être la pépite d'or" );
    assertEquals( c2->is_gold(), false, "c2 ne devrait pas être la pépite d'or" );


    //test is_empty
    assertEquals(  c2->is_empty(), true, "c2 devrait être vide" );

    //c2 n'est pas vide
    c2->set_empty();

    assertEquals(  c2->is_empty(), false, "c2 ne devrait pas être vide" );

    //c2 est à nouveau vide
    c2->set_empty();

    assertEquals(  c2->is_empty(), true, "c2 devrait être à nouveau vide" );


    //test is_indestructible
    assertEquals(  c2->is_indestructible(), false, "c2 ne devrait pas être indestructible" );

    //c2 indestructible
    c2->set_indestructible();
    assertEquals(  c2->is_indestructible(), true, "c2 devrait être indestructible" );

    //c2 pas indestructible
    c2->reset_indestructible();
    assertEquals(  c2->is_indestructible(), false, "après reset, c2 ne devrait pas être indestructible" );


    //test is_blocked
    assertEquals(  c2->is_blocked(), false, "c2 ne devrait pas être bloqué" );

    //c2 est bloqué
    c2->set_blocked();
    assertEquals(  c2->is_blocked(), true, "c2 devrait être bloqué" );

    //c2 plus bloqué
    c2->reset_blocked();
    assertEquals(  c2->is_blocked(), false, "apès reset, c2 ne devrait pas être bloqué" );

    //test get_color

    int colour=DEFAULT_COLOUR;

    assertEquals(  c2->get_color(), DEFAULT_COLOUR, "couleur de c2 incorrect" );

    colour=77;
    c2->set_color(colour);
    assertEquals(  c2->get_color(), colour, "couleur de c2 incorrect" );

    colour=DEFAULT_COLOUR;
    c2->reset_color();
    assertEquals(  c2->get_color(), colour, "après reset, couleur de c2 incorrect" );

    //test get_path

    // par défaut toutes les directions sont à false
    assertEquals(  c2->get_path(NORD), false, "direction Nord de c2 est incorrect" );
    assertEquals(  c2->get_path(EST), false, "direction Est de c2 est incorrect" );
    assertEquals(  c2->get_path(SUD), false, "direction Sud de c2 est incorrect" );
    assertEquals(  c2->get_path(OUEST), false, "direction Ouest de c2 est incorrect" );


    c2->set_path(NORD, true);
    assertEquals(  c2->get_path(NORD), true, "après set_path, direction Nord de c2 est incorrect" );

    c2->set_path(EST, true);
    assertEquals(  c2->get_path(EST), true, "après set_path, direction Est de c2 est incorrect" );

    c2->set_path(SUD, true);
    assertEquals(  c2->get_path(SUD), true, "après set_path, direction Sud de c2 est incorrect" );

    c2->set_path(OUEST, true);
    assertEquals(  c2->get_path(OUEST), true, "après set_path, direction Ouest de c2 est incorrect" );

    //reset met toutes les directions à false
    c2->reset_path();
    assertEquals(  c2->get_path(NORD), false, "après reset, direction Nord de c2 est incorrect" );
    assertEquals(  c2->get_path(EST), false, "après reset, direction Est de c2 est incorrect" );
    assertEquals(  c2->get_path(SUD), false, "après reset, direction Sud de c2 est incorrect" );
    assertEquals(  c2->get_path(OUEST), false, "après reset, direction Ouest de c2 est incorrect" );


    delete c1;
    delete c2;

    cout << "Tout va bien !" << endl;

    return 0;
}
