#ifndef TESTFUNC_H
#define TESTFUNC_H

#include <iostream>
#include <stdlib.h>

void assertEquals(int value, int expected, std::string errmsg);
void assertEquals(double value, double expected, double margin, std::string errmsg);
void assertEquals(bool value, bool expected, std::string errmsg);
void assertEquals(std::string value, std::string expected, std::string errmsg);

#endif
