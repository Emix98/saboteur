#include "../Action_on_grid.h"
#include <stdlib.h>

using namespace std;

//COMPILATION : g++ -o main Tests_Action_on_grid.cpp Action_on_grid.cpp Action.cpp Card.cpp

int main()
{
    //REVEAL,REMOVE,SWAP
    Action_on_grid *carte1 = new Action_on_grid(true, false, false);
    Action_on_grid *carte2 = new Action_on_grid(false, true, false);
    Action_on_grid *carte3 = new Action_on_grid(false, false, true);
    //Action_on_grid *carte4 = new Action_on_grid(false, false, true);

    if (carte1->is_reveal())
    {
        cout << "La carte1 permet bien de révéler une carte !\n";
    }
    else
    {
        cout << "Erreur is_reveal() !\n";
        exit(1);
    }

    if (carte2->is_remove())
    {
        cout << "La carte2 permet bien de retirer une carte !\n";
    }
    else
    {
        cout << "Erreur is_remove() !\n";
        exit(1);
    }

    if (carte2->is_reveal() == false)
    {
        cout << "La carte2 ne permet effectivement pas de retirer une carte !\n";
    }
    else
    {
        cout << "Erreur is_remove() !\n";
        exit(1);
    }

    if (carte3->is_swap())
    {
        cout << "La carte3 permet bien de swap une carte !\n";
    }
    else
    {
        cout << "Erreur is_swap() !\n";
        exit(1);
    }

    cout << "Tout va bien !\n";

    return 0;
}
