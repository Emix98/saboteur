//Installer MySQL avec mot de passe "saboteur"
//https://dev.mysql.com/downloads/mysql/
//Installer Connectors
//https://dev.mysql.com/downloads/connector/cpp/
//Compiler
//g++ -I/usr/local/mysql/include -L/usr/local/mysql/lib -lmysqlclient -o testgame tests/TestGame.cpp Game.cpp Card.cpp Grid.cpp Cell.cpp Player.cpp Team.cpp Path.cpp Action.cpp Action_on_grid.cpp Action_on_player.cpp


#include <iostream>

#include "../Player.h"
#include "../Card.h"
#include "../Action.h"
#include "../Action_on_player.h"
#include "../Action_on_grid.h"
#include "../Game.h"
#include "../Team.h"
#include "../Cell.h"
#include "../Grid.h"


using namespace std;

void print_check(string s,bool b){
  cout << s << " réussi ? : ";
  if(b) cout << "ok" << endl;
  else cout << "non" << endl;
}

void print_game(Game *g){
  cout << "game_id : " << g->get_game_id() << endl
  << "round : " << g->get_round() << endl
  << "position_token : " << g->get_position_token() << endl
  << "hand_length : " << g->get_hand_length() << endl
  << "play_time : " << g->get_play_time() << endl
  << "round_started : " << g->get_round_started() << endl
  << "gold_found : " << g->get_gold_found() << endl
  << "draw size : " << g->get_draw()->size() << endl;
}

void print_grid(Game *game){
  Grid *g = game->get_grid();
  int value = 0;
  //cout << "--------------------------------------" << endl;
  for(int i = 0 ; i < g->get_height() ; i++){
    for(int j = 0 ; j < g->get_width() ; j++){
      value = 0;
      if (g->get_cell(i,j)->get_path(0))
        value += 1;
      if (g->get_cell(i,j)->get_path(1))
        value += 3;
      if (g->get_cell(i,j)->get_path(2))
        value += 5;
      if (g->get_cell(i,j)->get_path(3))
        value += 7;
      cout << " " << g->get_cell(i,j)->get_type() << value;
    }
    cout << endl;
  }
    cout << "starting_point : "
    << "(" << g->get_starting_point()[0] << "," << g->get_starting_point()[1] << ")" << endl;
    cout << "ending_points : "
    << "(" << g->get_ending_points()[0][0] << "," << g->get_ending_points()[0][1] << ") ; ("
    << g->get_ending_points()[1][0] << "," << g->get_ending_points()[1][1] << ") ; ("
    << g->get_ending_points()[2][0] << "," << g->get_ending_points()[2][1] << ") " << endl;
  //cout << "--------------------------------------" << endl;
}

void print_teams(Game *game){
  std::vector<Team*> teams = *game->get_teams();
  for(int i = 0 ; i < teams.size() ; i++){
    cout << "team : " << teams[i]->get_name() << endl;
    for(int j = 0 ; j < teams[i]->get_players()->size() ; j++){
      cout << "   -  player : " << teams[i]->get_player(j)->get_name() << " ; "
      << "role : " << teams[i]->get_player(j)->get_saboteur() << endl;
    }
  }
}

void print_players(Game *game){
  std::vector<Player*> players = *game->get_players();
  for(int i = 0 ; i < players.size() ; i++){
    cout << *players[i] << endl;
  }
}

void print_draw(Game *game){
  std::vector<Card*> draw = *game->get_draw();
  for(int i = 0 ; i < draw.size() ; i++){
    //si c'est une carte chemin
    if(draw[i]->get_object_type()=='P'){
      cout << "Path : [";
      for(int j = 0; j < 4; j++){
        cout << static_cast<Path*>(draw[i])->get_path(j);
        if (j!=3) cout << ",";
      }
      cout << "]" << endl;
    }
    //si c'est une carte action sur joueur
    else if(draw[i]->get_object_type()=='J'){
    cout << "Action_on_player : [";
    for(int j = 0; j < 3; j++){
      cout << static_cast<Action_on_player*>(draw[i])->get_handicap_item(j);
      if (j!=2) cout << ",";
    }
    cout << "]" << " ; bonus ? : " << static_cast<Action_on_player*>(draw[i])->is_positive() << endl;
    }
    //si c'est une carte action sur grille
    else if(draw[i]->get_object_type()=='G'){
      cout << "Action_on_grid : ["
      << static_cast<Action_on_grid*>(draw[i])->is_reveal() << ","
      << static_cast<Action_on_grid*>(draw[i])->is_remove() << ","
      << static_cast<Action_on_grid*>(draw[i])->is_swap() << "]" << endl;
    }
  }
}

void print_hands(Game *game){
  for (int n = 0 ; n < game->get_nb_players() ; n++ ){
    std::vector<Card*>* hand = game->get_player(n)->get_hand();
    cout << "Player : " << game->get_player(n)->get_name() << endl;
    for(int i = 0 ; i < hand->size() ; i++){
      //si c'est une carte chemin
      if(hand->at(i)->get_object_type()=='P'){
        cout << "   -  Path : [";
        for(int j = 0; j < 4; j++){
          cout << static_cast<Path*>(hand->at(i))->get_path(j);
          if (j!=3) cout << ",";
        }
        cout << "]" << endl;
      }
      //si c'est une carte action sur joueur
      else if(hand->at(i)->get_object_type()=='J'){
      cout << "   -  Action_on_player : [";
      for(int j = 0; j < 3; j++){
        cout << static_cast<Action_on_player*>(hand->at(i))->get_handicap_item(j);
        if (j!=2) cout << ",";
      }
      cout << "]" << " ; bonus ? : " << static_cast<Action_on_player*>(hand->at(i))->is_positive() << endl;
      }
      //si c'est une carte action sur grille
      else if(hand->at(i)->get_object_type()=='G'){
        cout << "   -  Action_on_grid : ["
        << static_cast<Action_on_grid*>(hand->at(i))->is_reveal() << ","
        << static_cast<Action_on_grid*>(hand->at(i))->is_remove() << ","
        << static_cast<Action_on_grid*>(hand->at(i))->is_swap() << "]" << endl;
      }
    }
  }
}


void separator() {
  cout << "-------------------------------------------------------" << endl;
}

void title(string s) {
  cout << "----------- " << s << " -----------" << endl;
}

int main() {
  //Initialisation du jeu
  Game *game = new Game();

  //Création joueurs
  Player *player1 = new Player("Joueur 1", 0);
  Player *player2 = new Player("Joueur 2", 1);
  Player *player3 = new Player("Joueur 3", 2);
  cout << "Joueur de nom " << player1->get_name() << " créé" << endl;
  cout << "Joueur de nom " << player2->get_name() << " créé" << endl;
  cout << "Joueur de nom " << player3->get_name() << " créé" << endl;

  if(!game->add_user(1)){
    game->add_player(player1);
  }
  if(!game->add_user(2)){
    game->add_player(player2);
  }
  if(!game->add_user(3)){
    game->add_player(player3);
  }

  cout << "Joueurs : " << game->get_player(0)->get_name() << ", "
  << game->get_player(1)->get_name() << " et "
  << game->get_player(2)->get_name() << " présents" << endl;

  title("PRINT GAME");
  print_game(game);
  separator();
  //print_grid(game);
  title("PLAYERS");
  print_players(game);
  separator();

  title("Grid");
  print_grid(game);
  separator();

  title("INIT ROUND");
  game->init_round();
  separator();
  separator();

  title("Variables de la classe Game");
  print_game(game);
  separator();
  title("Grille");
  print_grid(game);
  separator();
  title("Joueurs");
  print_players(game);
  separator();
  title("Equipes");
  print_teams(game);
  separator();
  title("Pioche après distribution des mains");
  print_draw(game);
  separator();
  title("Mains");
  print_hands(game);
  separator();
  //Cas de jeu

  title("6 cartes chemin");
  bool b1 = game->get_player(0)->use_path_card(1,game->get_grid(),8,3,game->get_draw());
  bool b2 = game->get_player(0)->use_path_card(1,game->get_grid(),8,4,game->get_draw());
  bool b3 = game->get_player(0)->use_path_card(1,game->get_grid(),3,5,game->get_draw());
  bool b4 = game->get_player(0)->use_path_card(1,game->get_grid(),7,3,game->get_draw());
  bool b5 = game->get_player(0)->use_path_card(1,game->get_grid(),7,4,game->get_draw());
  bool b6 = game->get_player(0)->use_path_card(1,game->get_grid(),7,0,game->get_draw());
  print_check ("1. poser une carte au dessus du départ",b1);
  print_check ("2. poser une carte à droite de 1.",b2);
  print_check ("3. poser une carte dans le vide (faux)",b3);
  print_check ("4. poser une carte au dessus de 1.",b4);
  print_check ("5. poser une carte non joignable à droite de 4. (faux)",b5);
  print_check ("6. poser une carte dans le vide (faux)",b6);

  title("Grille");
  print_grid(game);
  separator();

  title("Mains");
  print_hands(game);
  separator();


  cout << "path_finding() : " << game->get_grid()->path_finding() << endl;
  game->get_grid()->get_cell(8,3)->reset_all();
  game->get_grid()->get_cell(8,4)->reset_all();
  game->get_grid()->get_cell(7,3)->reset_all();

  game->get_grid()->get_cell(8,3)->set_path(1,1,1,1,0);
  game->get_grid()->get_cell(7,3)->set_path(1,1,1,1,0);
  game->get_grid()->get_cell(6,3)->set_path(1,1,1,1,0);
  game->get_grid()->get_cell(5,3)->set_path(1,1,1,1,0);
  game->get_grid()->get_cell(4,3)->set_path(1,1,1,1,0);
  game->get_grid()->get_cell(3,3)->set_path(1,1,1,1,0);
  game->get_grid()->get_cell(2,3)->set_path(1,1,1,1,0);
  game->get_grid()->get_cell(8,4)->set_path(1,1,1,1,0);
  game->get_grid()->get_cell(8,5)->set_path(1,1,1,1,0);
  game->get_grid()->get_cell(7,5)->set_path(1,1,1,1,0);

  title("Recherche d'un chemin");
  print_grid(game);

  cout << "- path_finding() : " << endl;
  b1=game->get_grid()->path_finding();
  print_check("parcours",b1);

  //cout << "get_cell(7,5)->get_marked() : " << game->get_grid()->get_cell(7,5)->get_marked() <<endl;
  separator();

  //tester les cartes action
  title("Action sur un joueur");
  cout << "avant malus Joueur 2 -> Joueur 1 : " << endl << *game->get_player(0) << endl;
  b1 = game->get_player(2)->use_action_on_player_card(5,game->get_player(0),game->get_draw());
  print_check("malus",b1);
  cout << "après malus Joueur 3 -> Joueur 1: " << endl << *game->get_player(0) << endl;
  b1 = game->get_player(1)->use_action_on_player_card(5,game->get_player(0),game->get_draw());
  print_check("bonus",b1);
  separator();
  //cout << game->get_player(1)->get_card(4)->get_object_type() << endl;

  title("Action sur la grille");

  b1=game->is_over();
  print_check("Partie finie (vrai)",b1);
  game->end_round();
  game->end_game();
  title("PLAYERS");
  print_players(game);
  separator();
  game->next_player();
  //Test SQL
  game->create_user("testing_user","admin","first_name","name");
}
