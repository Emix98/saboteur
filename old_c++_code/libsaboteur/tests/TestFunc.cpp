#include "TestFunc.h"

using namespace std;

void assertEquals(int value, int expected, string errmsg) {
  if (value != expected) {
    cerr << errmsg << " (attendu : " << expected << " / obtenu : " << value << ")" << endl;
    exit(1);
  }
}

void assertEquals(double value, double expected, double margin, string errmsg) {
  if (abs(value-margin) >= margin) {
    cerr << errmsg << " (attendu : " << expected << " / obtenu : " << value << ")" << endl;
    exit(1);
  }
}

void assertEquals(bool value, bool expected, string errmsg) {
  if (value != expected) {
    cerr << errmsg << " (attendu : " << expected << " / obtenu : " << value << ")" << endl;
    exit(1);
  }
}

void assertEquals(string value, string expected, string errmsg) {
  if (value != expected) {
    cerr << errmsg << endl;
    cerr << " Attendu : " << expected << endl << "Obtenu : " << value << endl;
    exit(1);
  }
}
