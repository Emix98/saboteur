#include "../Card.h"
#include "../Action_on_grid.h"
#include "../Action_on_player.h"
#include "../Path.h"
#include <stdlib.h>
#include <iostream>

using namespace std;

//Fonctions à tester : http://www.cplusplus.com/reference/vector/vector/

int main()
{
    vector<Card*> jeu ;

    Action_on_grid *carte1 = new Action_on_grid(true, false, false);
    Action_on_grid *carte2 = new Action_on_grid(false, true, false);
    Action_on_grid *carte3 = new Action_on_grid(false, false, true);
    Action_on_grid *carte4 = new Action_on_grid(false, false, true);

    Action_on_player *carte5 = new Action_on_player();
    Action_on_player *carte6 = new Action_on_player();
    //Carte1 répare chariot et lanterne
    carte5->assign_card('c', true);
    carte5->assign_card('l', true);

    //Carte2 casse la pioche
    carte6->assign_card('p', false);

    Path *carte7 = new Path(true, false, false, true, false);
    Path *carte8 = new Path(false, true, false, false, true);
    Path *carte9 = new Path(true, true, true, true, false);

    jeu.push_back(carte1);
    cout << "Carte 1 ajoutée dans la pioche !\n" ;
    jeu.push_back(carte2);
    cout << "Carte 2 ajoutée dans la pioche !\n" ;
    jeu.push_back(carte3);
    cout << "Carte 3 ajoutée dans la pioche !\n" ;
    jeu.push_back(carte4);
    cout << "Carte 4 ajoutée dans la pioche !\n" ;
    jeu.push_back(carte5);
    cout << "Carte 5 ajoutée dans la pioche !\n" ;
    jeu.push_back(carte6);
    cout << "Carte 6 ajoutée dans la pioche !\n" ;
    jeu.push_back(carte7);
    cout << "Carte 7 ajoutée dans la pioche !\n" ;
    jeu.push_back(carte8);
    cout << "Carte 8 ajoutée dans la pioche !\n" ;
    jeu.push_back(carte9);
    cout << "Carte 9 ajoutée dans la pioche !\n" ;

    for (auto it = jeu.begin(); it != jeu.end(); ++it)
        //cout << *it << " ";
        cout << "Test boucle de parcours ! \n" ;

    cout << "Nombre de cartes ajoutées : " << jeu.size() << " !\n";

    //jeu.begin().is_reveal();

    return 0 ;
}
