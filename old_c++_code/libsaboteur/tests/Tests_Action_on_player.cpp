#include "../Action_on_player.h"
#include <stdlib.h>
#include <iostream>

using namespace std;

//COMPILATION : g++ -o main Tests_Action_on_player.cpp Action_on_player.cpp Action.cpp Card.cpp

int main()
{
    Action_on_player *carte1 = new Action_on_player();
    Action_on_player *carte2 = new Action_on_player();
    //Action_on_player *carte3 = new Action_on_player();
    
    //Carte1 répare chariot et lanterne
    carte1->assign_card('c', true);
    carte1->assign_card('l', true);

    //Carte2 casse la pioche
    carte2->assign_card('p', false);

    if (carte1->is_positive() == true)
    {
        cout << "Carte1 est réparatrice, OK\n";
    }
    else
    {
        cout << "Erreur is_positive() !\n";
        exit(1);
    }

    if (carte1->get_handicap_item(0) && carte1->get_handicap_item(1))
    {
        cout << "Carte1 affecte bien le chariot et la lanterne, OK\n";
    }
    else
    {
        cout << "Erreur get_handicap_item(x) !\n";
        exit(1);
    }

    if (carte1->is_positive() == false)
    {
        cout << "Carte2 est destructive, OK\n";
    }
    else
    {
        cout << "Erreur is_positive() !\n";
        exit(1);
    }

    if (carte2->get_handicap_item(2) == true)
    {
        cout << "Carte2 affecte bien la lanterne, OK\n";
    }
    else
    {
        cout << "Erreur get_handicap_item(2) !\n";
        exit(1);
    }

    cout << "Tout va bien :)\n";

    return 0;
}
