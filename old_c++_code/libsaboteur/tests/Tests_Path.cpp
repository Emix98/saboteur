#include "../Path.h"
#include <stdlib.h>

using namespace std;

//COMPILATION :  g++ -o main Tests_Path.cpp Path.cpp Card.cpp
int main()
{
    Path *carte1 = new Path(true, false, false, true, false);
    Path *carte2 = new Path(false, true, false, false, true);
    Path *carte3 = new Path(true, true, true, true, false);

    if ((carte1->get_path_item(0) == true) && (carte1->get_path_item(1) == false) && (carte1->get_path_item(2) == false) && (carte1->get_path_item(3) == true))
    {
        cout << "Vérification get_path_item(x) sur Carte1 OK !\n";
    }
    else
    {
        cout << "Erreur get_path_item(x) ou constructeur sur carte1 !\n";
        exit(1);
    }

    if ((carte2->get_path_item(0) == false) && (carte2->get_path_item(1) == true) && (carte2->get_path_item(2) == false) && (carte2->get_path_item(3) == false))
    {
        cout << "Vérification get_path_item(x) sur Carte2 OK !\n";
    }
    else
    {
        cout << "Erreur get_path_item(x) ou constructeur sur carte2 !\n";
        exit(1);
    }

    if ((carte3->is_blocked() == false) && carte2->is_blocked() == true)
    {
        cout << "Vérification is_blocked() OK !\n";
    }
    else
    {
        cout << "Erreur is_blocked() !\n";
        exit(1);
    }

    cout << "Tout va bien :)\n";
    return 0;
}
