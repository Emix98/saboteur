#ifndef GODOT_SABOTEUR_H
#define GODOT_SABOTEUR_H

#include "core/reference.h"
// #include "libsaboteur/include.h"
#include "libsaboteur/Game.h"

class Saboteur : public Reference {

  GDCLASS(Saboteur, Reference);

  /**Partie en cours*/
  Game current_game;

  protected:
    static void _bind_methods();

  public:
    Saboteur();

    void start_new_game();

};

#endif // GODOT_SABOTEUR_H
