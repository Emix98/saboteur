#include "Saboteur.h"

void Saboteur::_bind_methods() {
  ClassDB::bind_method(D_METHOD("start_new_game"), &Saboteur::start_new_game);
}

void Saboteur::start_new_game() {
  current_game = Game();
}

Saboteur::Saboteur() {}
