#include "register_types.h"
#include "core/class_db.h"
#include "Saboteur.h"

void register_saboteur_types() {
  ClassDB::register_class<Saboteur>();
}

void unregister_saboteur_types() {}
