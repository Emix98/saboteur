# PROJET INTEGRATEUR


## Petite regles

- On COMMENTE son code le moment venu
- Les noms des variables ne sont PAS ABREGER
- Tout est ecrit en ANGLAIS dans les fichiers de codes
- On fait la doc approximative AU FUR ET A MESURE du projet
- On ne taff pas que le week-end sinon on finira jamais !
- On previent si on galere !!!


## Taches des Groupes

### POO

1. UML (en groupe)
2. Algo pour les méthodes (en groupe)
3. Recherche de comment fonctionne C#
4. On code (en binome)


### Reseau

1. Recherche pour le C#
2. Adaptation du code client/serveur en C#
3. L'ameliorer pour pouvoir envoyer une grille


### GUI

1. Design + Wireframe
2. Recherche pour le faire avec Unity
3. Implementation


### BDD

1. Merise
2. Implementation (oracle/ ou mySQL)


### APK

1. Recherche si C# et Unity passe bien sur Android
2. Comprendre comment est fait un apk
3. Essayer d'en faire un
4. En faire un avec un code en C#

